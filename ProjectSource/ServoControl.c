/****************************************************************************
 Module
   ServoControl.c

 Revision
   1.0.1

 Description
   This is the lower-level module to control the servo through Timer 2 PWM

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 05/10/20 12:00  kbj     creation
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "ServoControl.h"

// debugging printf()
#include "dbprintf.h"

// Hardware
#include <xc.h>

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

//from eqn 28-1: 20ms = [PR2+1]*4*(1/31.25kHz)*1 -> PR2 = 155
#define SERVO_PERIOD 155

//the values to load into duty cycle register for push to talk (PTT)
//from eqn 28-3: 5% = CCPR2H/((155+1)) -> CCPR2H = 31
//              10% = CCPR2H/((155+1)) -> CCPR2H = 62
#define PTT_OFF 8//5% DC
#define PTT_ON 16//10% DC

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t DeferralQueue[3 + 1];


/*------------------------------ Module Code ------------------------------*/

void Start_PushToTalk (void){
    //set DC using macro in CCPR2H register
    CCPR2H = PTT_ON;
}

void Stop_PushToTalk (void){
    //call SetServo with correct #define period
    CCPR2H = PTT_OFF;
}

void InitPWM(void){
    //Config and start Timer2
        //Clear TMR2IF in PIR4
    PIR4bits.TMR2IF = 0;
        //Select clock source (31.25kHz mode, much slower than Fosc!)
    T2CLKCON = 0b00000110;
        //Select prescaler (1:4) [needed for PWM it seems]
    T2CON = 0b00100000;
    //Load PR2 w/ PWM period value
    T2PR = SERVO_PERIOD;
        //Enable timer in T2CON
    T2CONbits.ON = 1;
    //Use output pin RxyPPS control to select CCP2 as source
    //  and disable pin output in TRIS
    RC1PPS = 0x0A; //set CCP2 out to be on RC1
    TRISCbits.TRISC1 = 1; //leaving default RC1 as CCP2
    ANSELCbits.ANSC1 = 0; //clear analog
    //Configure CCP module for PWM mode by loading CCP2CON w/ appropriate values
    CCP2CONbits.CCP2MODE0 = 1;
    CCP2CONbits.CCP2MODE1 = 1;
    CCP2CONbits.CCP2MODE2 = 1;
    CCP2CONbits.CCP2MODE3 = 1;
    CCP2CONbits.CCP2EN = 1;
    //Load CCPR2L and CCPR2H with duty cycle and config CCPxFMT bit in CON for alignment
    CCPR2H = PTT_OFF;
    CCP2CONbits.CCP2FMT = 1; //only use CCPR2H 8-bits
    //Enable PWM output pin
        //wait for TMR2IF to be set (optional, won't do but keep in mind)
    T2TMR = 0; //just gonna clear Timer 2 instead
        //Enable CCPx pin output driver by clearing TRIS
    TRISCbits.TRISC1 = 0;
    printf("Init PWM\r\n");
}