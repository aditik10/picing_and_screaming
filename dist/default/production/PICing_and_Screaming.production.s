opt subtitle "Microchip MPLAB XC8 C Compiler v2.10 (Free license) build 20190730164152 Og1 "

opt pagewidth 120

	opt flic

	processor	16F15356
opt include "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\16f15356.cgen.inc"
getbyte	macro	val,pos
	(((val) >> (8 * pos)) and 0xff)
endm
byte0	macro	val
	(getbyte(val,0))
endm
byte1	macro	val
	(getbyte(val,1))
endm
byte2	macro	val
	(getbyte(val,2))
endm
byte3	macro	val
	(getbyte(val,3))
endm
byte4	macro	val
	(getbyte(val,4))
endm
byte5	macro	val
	(getbyte(val,5))
endm
byte6	macro	val
	(getbyte(val,6))
endm
byte7	macro	val
	(getbyte(val,7))
endm
getword	macro	val,pos
	(((val) >> (8 * pos)) and 0xffff)
endm
word0	macro	val
	(getword(val,0))
endm
word1	macro	val
	(getword(val,2))
endm
word2	macro	val
	(getword(val,4))
endm
word3	macro	val
	(getword(val,6))
endm
gettword	macro	val,pos
	(((val) >> (8 * pos)) and 0xffffff)
endm
tword0	macro	val
	(gettword(val,0))
endm
tword1	macro	val
	(gettword(val,3))
endm
tword2	macro	val
	(gettword(val,6))
endm
getdword	macro	val,pos
	(((val) >> (8 * pos)) and 0xffffffff)
endm
dword0	macro	val
	(getdword(val,0))
endm
dword1	macro	val
	(getdword(val,4))
endm
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
pushw	macro
	movwi fsr1++
	endm
popw	macro
	moviw --fsr1
	endm
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
# 110 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF0 equ 00h ;# 
# 130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INDF1 equ 01h ;# 
# 150 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCL equ 02h ;# 
# 170 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS equ 03h ;# 
# 233 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L equ 04h ;# 
# 253 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H equ 05h ;# 
# 277 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L equ 06h ;# 
# 297 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H equ 07h ;# 
# 317 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR equ 08h ;# 
# 375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG equ 09h ;# 
# 395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH equ 0Ah ;# 
# 415 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTCON equ 0Bh ;# 
# 448 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTA equ 0Ch ;# 
# 510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTB equ 0Dh ;# 
# 572 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTC equ 0Eh ;# 
# 634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PORTE equ 010h ;# 
# 655 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISA equ 012h ;# 
# 717 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISB equ 013h ;# 
# 779 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISC equ 014h ;# 
# 841 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TRISE equ 016h ;# 
# 862 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATA equ 018h ;# 
# 924 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATB equ 019h ;# 
# 986 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
LATC equ 01Ah ;# 
# 1048 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRES equ 09Bh ;# 
# 1055 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESL equ 09Bh ;# 
# 1125 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADRESH equ 09Ch ;# 
# 1195 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON0 equ 09Dh ;# 
# 1272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADCON1 equ 09Eh ;# 
# 1338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACT equ 09Fh ;# 
# 1390 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1REG equ 0119h ;# 
# 1395 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG equ 0119h ;# 
# 1399 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG1 equ 0119h ;# 
# 1444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1REG equ 011Ah ;# 
# 1449 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG1 equ 011Ah ;# 
# 1453 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG equ 011Ah ;# 
# 1498 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRG equ 011Bh ;# 
# 1505 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGL equ 011Bh ;# 
# 1510 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG equ 011Bh ;# 
# 1514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG1 equ 011Bh ;# 
# 1518 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGL equ 011Bh ;# 
# 1575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP1BRGH equ 011Ch ;# 
# 1580 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH equ 011Ch ;# 
# 1584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH1 equ 011Ch ;# 
# 1629 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1STA equ 011Dh ;# 
# 1634 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA1 equ 011Dh ;# 
# 1638 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA equ 011Dh ;# 
# 1809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1STA equ 011Eh ;# 
# 1814 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA1 equ 011Eh ;# 
# 1818 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA equ 011Eh ;# 
# 1989 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD1CON equ 011Fh ;# 
# 1994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON1 equ 011Fh ;# 
# 1998 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL1 equ 011Fh ;# 
# 2002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON equ 011Fh ;# 
# 2006 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL equ 011Fh ;# 
# 2235 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1BUF equ 018Ch ;# 
# 2255 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1ADD equ 018Dh ;# 
# 2375 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1MSK equ 018Eh ;# 
# 2445 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1STAT equ 018Fh ;# 
# 2809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON1 equ 0190h ;# 
# 2929 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON2 equ 0191h ;# 
# 3116 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CON3 equ 0192h ;# 
# 3178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2BUF equ 0196h ;# 
# 3198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2ADD equ 0197h ;# 
# 3318 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2MSK equ 0198h ;# 
# 3388 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2STAT equ 0199h ;# 
# 3752 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON1 equ 019Ah ;# 
# 3872 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON2 equ 019Bh ;# 
# 4059 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CON3 equ 019Ch ;# 
# 4121 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1 equ 020Ch ;# 
# 4128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1L equ 020Ch ;# 
# 4298 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1H equ 020Dh ;# 
# 4418 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CON equ 020Eh ;# 
# 4514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GCON equ 020Fh ;# 
# 4519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR1 equ 020Fh ;# 
# 4710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GATE equ 0210h ;# 
# 4715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1GATE equ 0210h ;# 
# 4876 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CLK equ 0211h ;# 
# 4881 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR1CLK equ 0211h ;# 
# 5018 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2TMR equ 028Ch ;# 
# 5023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR2 equ 028Ch ;# 
# 5072 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2PR equ 028Dh ;# 
# 5077 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR2 equ 028Dh ;# 
# 5126 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CON equ 028Eh ;# 
# 5272 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2HLT equ 028Fh ;# 
# 5400 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2CLKCON equ 0290h ;# 
# 5480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2RST equ 0291h ;# 
# 5560 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1 equ 030Ch ;# 
# 5567 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1L equ 030Ch ;# 
# 5587 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR1H equ 030Dh ;# 
# 5607 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CON equ 030Eh ;# 
# 5734 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1CAP equ 030Fh ;# 
# 5802 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2 equ 0310h ;# 
# 5809 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2L equ 0310h ;# 
# 5829 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCPR2H equ 0311h ;# 
# 5849 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CON equ 0312h ;# 
# 5976 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2CAP equ 0313h ;# 
# 6044 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DC equ 0314h ;# 
# 6051 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCL equ 0314h ;# 
# 6117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3DCH equ 0315h ;# 
# 6287 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM3CON equ 0316h ;# 
# 6343 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DC equ 0318h ;# 
# 6350 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCL equ 0318h ;# 
# 6416 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4DCH equ 0319h ;# 
# 6586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM4CON equ 031Ah ;# 
# 6642 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DC equ 031Ch ;# 
# 6649 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCL equ 031Ch ;# 
# 6715 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5DCH equ 031Dh ;# 
# 6885 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM5CON equ 031Eh ;# 
# 6941 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DC equ 038Ch ;# 
# 6948 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCL equ 038Ch ;# 
# 7014 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6DCH equ 038Dh ;# 
# 7184 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PWM6CON equ 038Eh ;# 
# 7242 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACC equ 058Ch ;# 
# 7249 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCL equ 058Ch ;# 
# 7319 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCH equ 058Dh ;# 
# 7389 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1ACCU equ 058Eh ;# 
# 7437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INC equ 058Fh ;# 
# 7444 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCL equ 058Fh ;# 
# 7514 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCH equ 0590h ;# 
# 7584 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1INCU equ 0591h ;# 
# 7630 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CON equ 0592h ;# 
# 7670 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NCO1CLK equ 0593h ;# 
# 7736 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0L equ 059Ch ;# 
# 7741 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0 equ 059Ch ;# 
# 7874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TMR0H equ 059Dh ;# 
# 7879 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PR0 equ 059Dh ;# 
# 8128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON0 equ 059Eh ;# 
# 8198 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CON1 equ 059Fh ;# 
# 8309 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CLKCON equ 060Ch ;# 
# 8337 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DAT equ 060Dh ;# 
# 8383 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBR equ 060Eh ;# 
# 8487 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1DBF equ 060Fh ;# 
# 8591 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON0 equ 0610h ;# 
# 8692 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1CON1 equ 0611h ;# 
# 8770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS0 equ 0612h ;# 
# 8890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1AS1 equ 0613h ;# 
# 8934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1STR equ 0614h ;# 
# 9046 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR0 equ 070Ch ;# 
# 9079 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR1 equ 070Dh ;# 
# 9118 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR2 equ 070Eh ;# 
# 9151 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR3 equ 070Fh ;# 
# 9213 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR4 equ 0710h ;# 
# 9239 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR5 equ 0711h ;# 
# 9284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR6 equ 0712h ;# 
# 9310 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIR7 equ 0713h ;# 
# 9352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE0 equ 0716h ;# 
# 9385 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE1 equ 0717h ;# 
# 9424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE2 equ 0718h ;# 
# 9457 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE3 equ 0719h ;# 
# 9519 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE4 equ 071Ah ;# 
# 9545 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE5 equ 071Bh ;# 
# 9590 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE6 equ 071Ch ;# 
# 9616 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PIE7 equ 071Dh ;# 
# 9658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD0 equ 0796h ;# 
# 9703 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD1 equ 0797h ;# 
# 9751 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD2 equ 0798h ;# 
# 9796 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD3 equ 0799h ;# 
# 9846 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD4 equ 079Ah ;# 
# 9891 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PMD5 equ 079Bh ;# 
# 9930 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON0 equ 080Ch ;# 
# 10005 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTCON1 equ 080Dh ;# 
# 10099 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSL equ 080Eh ;# 
# 10227 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTPSH equ 080Fh ;# 
# 10355 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WDTTMR equ 0810h ;# 
# 10437 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BORCON equ 0811h ;# 
# 10464 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
VREGCON equ 0812h ;# 
# 10485 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON0 equ 0813h ;# 
# 10547 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCON1 equ 0814h ;# 
# 10568 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADR equ 081Ah ;# 
# 10575 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRL equ 081Ah ;# 
# 10637 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMADRH equ 081Bh ;# 
# 10693 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDAT equ 081Ch ;# 
# 10700 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATL equ 081Ch ;# 
# 10762 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMDATH equ 081Dh ;# 
# 10812 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON1 equ 081Eh ;# 
# 10868 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
NVMCON2 equ 081Fh ;# 
# 10888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CPUDOZE equ 088Ch ;# 
# 10953 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON1 equ 088Dh ;# 
# 11023 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON2 equ 088Eh ;# 
# 11093 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCCON3 equ 088Fh ;# 
# 11133 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCSTAT equ 0890h ;# 
# 11190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCEN equ 0891h ;# 
# 11241 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCTUNE equ 0892h ;# 
# 11299 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
OSCFRQ equ 0893h ;# 
# 11339 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCON equ 0895h ;# 
# 11404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLKRCLK equ 0896h ;# 
# 11450 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FVRCON equ 090Ch ;# 
# 11526 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON0 equ 090Eh ;# 
# 11627 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
DAC1CON1 equ 090Fh ;# 
# 11679 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ZCDCON equ 091Fh ;# 
# 11725 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMOUT equ 098Fh ;# 
# 11730 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CMSTAT equ 098Fh ;# 
# 11803 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON0 equ 0990h ;# 
# 11883 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1CON1 equ 0991h ;# 
# 11923 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1NCH equ 0992h ;# 
# 11983 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM1PCH equ 0993h ;# 
# 12043 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON0 equ 0994h ;# 
# 12123 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2CON1 equ 0995h ;# 
# 12163 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2NCH equ 0996h ;# 
# 12223 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CM2PCH equ 0997h ;# 
# 12283 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2REG equ 0A19h ;# 
# 12288 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCREG2 equ 0A19h ;# 
# 12321 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2REG equ 0A1Ah ;# 
# 12326 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXREG2 equ 0A1Ah ;# 
# 12359 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRG equ 0A1Bh ;# 
# 12366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGL equ 0A1Bh ;# 
# 12371 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRG2 equ 0A1Bh ;# 
# 12404 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SP2BRGH equ 0A1Ch ;# 
# 12409 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SPBRGH2 equ 0A1Ch ;# 
# 12442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2STA equ 0A1Dh ;# 
# 12447 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RCSTA2 equ 0A1Dh ;# 
# 12564 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2STA equ 0A1Eh ;# 
# 12569 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TXSTA2 equ 0A1Eh ;# 
# 12686 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUD2CON equ 0A1Fh ;# 
# 12691 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCON2 equ 0A1Fh ;# 
# 12695 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BAUDCTL2 equ 0A1Fh ;# 
# 12836 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCDATA equ 01E0Fh ;# 
# 12874 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1CON equ 01E10h ;# 
# 12992 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1POL equ 01E11h ;# 
# 13070 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL0 equ 01E12h ;# 
# 13174 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL1 equ 01E13h ;# 
# 13278 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL2 equ 01E14h ;# 
# 13382 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1SEL3 equ 01E15h ;# 
# 13486 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS0 equ 01E16h ;# 
# 13598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS1 equ 01E17h ;# 
# 13710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS2 equ 01E18h ;# 
# 13822 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC1GLS3 equ 01E19h ;# 
# 13934 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2CON equ 01E1Ah ;# 
# 14052 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2POL equ 01E1Bh ;# 
# 14130 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL0 equ 01E1Ch ;# 
# 14234 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL1 equ 01E1Dh ;# 
# 14338 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL2 equ 01E1Eh ;# 
# 14442 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2SEL3 equ 01E1Fh ;# 
# 14546 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS0 equ 01E20h ;# 
# 14658 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS1 equ 01E21h ;# 
# 14770 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS2 equ 01E22h ;# 
# 14882 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC2GLS3 equ 01E23h ;# 
# 14994 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3CON equ 01E24h ;# 
# 15112 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3POL equ 01E25h ;# 
# 15190 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL0 equ 01E26h ;# 
# 15294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL1 equ 01E27h ;# 
# 15398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL2 equ 01E28h ;# 
# 15502 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3SEL3 equ 01E29h ;# 
# 15606 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS0 equ 01E2Ah ;# 
# 15718 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS1 equ 01E2Bh ;# 
# 15830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS2 equ 01E2Ch ;# 
# 15942 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC3GLS3 equ 01E2Dh ;# 
# 16054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4CON equ 01E2Eh ;# 
# 16172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4POL equ 01E2Fh ;# 
# 16250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL0 equ 01E30h ;# 
# 16354 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL1 equ 01E31h ;# 
# 16458 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL2 equ 01E32h ;# 
# 16562 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4SEL3 equ 01E33h ;# 
# 16666 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS0 equ 01E34h ;# 
# 16778 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS1 equ 01E35h ;# 
# 16890 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS2 equ 01E36h ;# 
# 17002 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLC4GLS3 equ 01E37h ;# 
# 17114 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PPSLOCK equ 01E8Fh ;# 
# 17134 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INTPPS equ 01E90h ;# 
# 17192 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T0CKIPPS equ 01E91h ;# 
# 17250 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1CKIPPS equ 01E92h ;# 
# 17308 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T1GPPS equ 01E93h ;# 
# 17366 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
T2INPPS equ 01E9Ch ;# 
# 17424 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP1PPS equ 01EA1h ;# 
# 17482 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CCP2PPS equ 01EA2h ;# 
# 17540 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CWG1PPS equ 01EB1h ;# 
# 17598 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN0PPS equ 01EBBh ;# 
# 17656 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN1PPS equ 01EBCh ;# 
# 17714 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN2PPS equ 01EBDh ;# 
# 17772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
CLCIN3PPS equ 01EBEh ;# 
# 17830 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ADACTPPS equ 01EC3h ;# 
# 17888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1CLKPPS equ 01EC5h ;# 
# 17946 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1DATPPS equ 01EC6h ;# 
# 18004 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP1SSPPS equ 01EC7h ;# 
# 18062 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2CLKPPS equ 01EC8h ;# 
# 18120 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2DATPPS equ 01EC9h ;# 
# 18178 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SSP2SSPPS equ 01ECAh ;# 
# 18236 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX1DTPPS equ 01ECBh ;# 
# 18294 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX1CKPPS equ 01ECCh ;# 
# 18352 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RX2DTPPS equ 01ECDh ;# 
# 18410 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TX2CKPPS equ 01ECEh ;# 
# 18468 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA0PPS equ 01F10h ;# 
# 18512 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA1PPS equ 01F11h ;# 
# 18556 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA2PPS equ 01F12h ;# 
# 18600 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA3PPS equ 01F13h ;# 
# 18644 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA4PPS equ 01F14h ;# 
# 18688 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA5PPS equ 01F15h ;# 
# 18732 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA6PPS equ 01F16h ;# 
# 18776 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RA7PPS equ 01F17h ;# 
# 18820 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB0PPS equ 01F18h ;# 
# 18864 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB1PPS equ 01F19h ;# 
# 18908 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB2PPS equ 01F1Ah ;# 
# 18952 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB3PPS equ 01F1Bh ;# 
# 18996 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB4PPS equ 01F1Ch ;# 
# 19040 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB5PPS equ 01F1Dh ;# 
# 19084 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB6PPS equ 01F1Eh ;# 
# 19128 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RB7PPS equ 01F1Fh ;# 
# 19172 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC0PPS equ 01F20h ;# 
# 19216 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC1PPS equ 01F21h ;# 
# 19260 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC2PPS equ 01F22h ;# 
# 19304 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC3PPS equ 01F23h ;# 
# 19348 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC4PPS equ 01F24h ;# 
# 19392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC5PPS equ 01F25h ;# 
# 19436 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC6PPS equ 01F26h ;# 
# 19480 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
RC7PPS equ 01F27h ;# 
# 19524 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELA equ 01F38h ;# 
# 19586 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUA equ 01F39h ;# 
# 19648 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONA equ 01F3Ah ;# 
# 19710 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONA equ 01F3Bh ;# 
# 19772 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLA equ 01F3Ch ;# 
# 19834 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAP equ 01F3Dh ;# 
# 19896 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAN equ 01F3Eh ;# 
# 19958 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCAF equ 01F3Fh ;# 
# 20020 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELB equ 01F43h ;# 
# 20082 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUB equ 01F44h ;# 
# 20144 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONB equ 01F45h ;# 
# 20206 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONB equ 01F46h ;# 
# 20268 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLB equ 01F47h ;# 
# 20330 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBP equ 01F48h ;# 
# 20392 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBN equ 01F49h ;# 
# 20454 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCBF equ 01F4Ah ;# 
# 20516 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ANSELC equ 01F4Eh ;# 
# 20578 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUC equ 01F4Fh ;# 
# 20640 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
ODCONC equ 01F50h ;# 
# 20702 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
SLRCONC equ 01F51h ;# 
# 20764 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLC equ 01F52h ;# 
# 20826 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCP equ 01F53h ;# 
# 20888 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCN equ 01F54h ;# 
# 20950 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCCF equ 01F55h ;# 
# 21012 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WPUE equ 01F65h ;# 
# 21033 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
INLVLE equ 01F68h ;# 
# 21054 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEP equ 01F69h ;# 
# 21075 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEN equ 01F6Ah ;# 
# 21096 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
IOCEF equ 01F6Bh ;# 
# 21117 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STATUS_SHAD equ 01FE4h ;# 
# 21137 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
WREG_SHAD equ 01FE5h ;# 
# 21157 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
BSR_SHAD equ 01FE6h ;# 
# 21177 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
PCLATH_SHAD equ 01FE7h ;# 
# 21197 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0_SHAD equ 01FE8h ;# 
# 21204 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0L_SHAD equ 01FE8h ;# 
# 21224 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR0H_SHAD equ 01FE9h ;# 
# 21244 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1L_SHAD equ 01FEAh ;# 
# 21264 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
FSR1H_SHAD equ 01FEBh ;# 
# 21284 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
STKPTR equ 01FEDh ;# 
# 21328 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSL equ 01FEEh ;# 
# 21398 "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\include\pic16f15356.h"
TOSH equ 01FEFh ;# 
;;_ThisEvent	offset=-3 (param)
;;_WhichService	offset	0
;;_TheEvent	offset=-3 (param)
;;_pBlock	offset	0
;;_pThisQueue	offset	1
;;_Event2Add	offset=-3 (param)
	FNCALL	_main,_DB_printf
	FNCALL	_main,_ES_Initialize
	FNCALL	_main,_ES_Run
	FNCALL	_main,_InitPWM
	FNCALL	_main,__HW_PIC15356Init
	FNCALL	_main,_puts
	FNCALL	__HW_PIC15356Init,__HW_ConsoleInit
	FNCALL	_InitPWM,_DB_printf
	FNCALL	_ES_Run,_ES_CheckUserEvents
	FNCALL	_ES_Run,_ES_DeQueue
	FNCALL	_ES_Run,_ES_GetMSBitSet
	FNCALL	_ES_Run,_RunTestHarnessService0
	FNCALL	_ES_Run,__HW_Process_Pending_Ints
	FNCALL	__HW_Process_Pending_Ints,_ES_Timer_Tick_Resp
	FNCALL	_ES_Timer_Tick_Resp,_ES_GetMSBitSet
	FNCALL	_ES_Timer_Tick_Resp,_PostTestHarnessService0
	FNCALL	_PostTestHarnessService0,_ES_PostToService
	FNCALL	_RunTestHarnessService0,_BlinkLED
	FNCALL	_RunTestHarnessService0,_DB_printf
	FNCALL	_RunTestHarnessService0,_ES_EnQueueLIFO
	FNCALL	_RunTestHarnessService0,_ES_PostToService
	FNCALL	_RunTestHarnessService0,_ES_RecallEvents
	FNCALL	_RunTestHarnessService0,_ES_Timer_InitTimer
	FNCALL	_RunTestHarnessService0,_Start_PushToTalk
	FNCALL	_RunTestHarnessService0,_Stop_PushToTalk
	FNCALL	_RunTestHarnessService0,_puts
	FNCALL	_puts,_fputc
	FNCALL	_puts,_fputs
	FNCALL	_fputs,_fputc
	FNCALL	_fputc,_putch
	FNCALL	_ES_RecallEvents,_ES_DeQueue
	FNCALL	_ES_RecallEvents,_ES_PostToServiceLIFO
	FNCALL	_ES_PostToServiceLIFO,_ES_EnQueueLIFO
	FNCALL	_ES_EnQueueLIFO,___wmul
	FNCALL	_ES_DeQueue,___lbmod
	FNCALL	_ES_DeQueue,___wmul
	FNCALL	_DB_printf,_putch
	FNCALL	_DB_printf,_uitoa
	FNCALL	_uitoa,___lwdiv
	FNCALL	_uitoa,___lwmod
	FNCALL	_ES_CheckUserEvents,_Check4Keystroke
	FNCALL	_Check4Keystroke,_ES_PostAll
	FNCALL	_Check4Keystroke,_getchar
	FNCALL	_ES_PostAll,_ES_EnQueueFIFO
	FNCALL	_ES_Initialize,_ES_InitQueue
	FNCALL	_ES_Initialize,_ES_Timer_Init
	FNCALL	_ES_Initialize,_InitTestHarnessService0
	FNCALL	_InitTestHarnessService0,_ES_InitQueue
	FNCALL	_InitTestHarnessService0,_ES_PostToService
	FNCALL	_InitTestHarnessService0,_InitLED
	FNCALL	_ES_PostToService,_ES_EnQueueFIFO
	FNCALL	_ES_EnQueueFIFO,___awmod
	FNCALL	_ES_Timer_Init,__HW_Timer_Init
	FNROOT	_main
	FNCALL	_myIsr,__HW_SysTickIntHandler
	FNCALL	intlevel1,_myIsr
	global	intlevel1
	FNROOT	intlevel1
	global	RunTestHarnessService0@DeferredChar
psect	idataBANK0,class=CODE,space=0,delta=2,noexec
global __pidataBANK0
__pidataBANK0:
	file	"ProjectSource/TestHarnessService0.c"
	line	161

;initializer for RunTestHarnessService0@DeferredChar
	retlw	031h
	global	_Timer2PostFunc
psect	stringtext1,class=STRCODE,delta=2,noexec
global __pstringtext1
__pstringtext1:
	file	"FrameworkSource/ES_Timers.c"
	line	94
_Timer2PostFunc:
	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(0)
	retlw	high(0)

	retlw	low(_PostTestHarnessService0)
	retlw	high(_PostTestHarnessService0)

	global __end_of_Timer2PostFunc
__end_of_Timer2PostFunc:
	global	_BitNum2SetMask
psect	stringtext2,class=STRCODE,delta=2,noexec
global __pstringtext2
__pstringtext2:
	file	"FrameworkSource/ES_LookupTables.c"
	line	62
_BitNum2SetMask:
	retlw	01h
	retlw	0

	retlw	02h
	retlw	0

	retlw	04h
	retlw	0

	retlw	08h
	retlw	0

	retlw	010h
	retlw	0

	retlw	020h
	retlw	0

	retlw	040h
	retlw	0

	retlw	080h
	retlw	0

	retlw	0
	retlw	01h

	retlw	0
	retlw	02h

	retlw	0
	retlw	04h

	retlw	0
	retlw	08h

	retlw	0
	retlw	010h

	retlw	0
	retlw	020h

	retlw	0
	retlw	040h

	retlw	0
	retlw	080h

	global __end_of_BitNum2SetMask
__end_of_BitNum2SetMask:
	global	_Nybble2MSBitNum
psect	stringtext3,class=STRCODE,delta=2,noexec
global __pstringtext3
__pstringtext3:
	file	"FrameworkSource/ES_LookupTables.c"
	line	74
_Nybble2MSBitNum:
	retlw	low(0)
	retlw	01h
	retlw	01h
	retlw	02h
	retlw	02h
	retlw	02h
	retlw	02h
	retlw	03h
	retlw	03h
	retlw	03h
	retlw	03h
	retlw	03h
	retlw	03h
	retlw	03h
	retlw	03h
	global __end_of_Nybble2MSBitNum
__end_of_Nybble2MSBitNum:
	global	_ServDescList
psect	stringtext4,class=STRCODE,delta=2,noexec
global __pstringtext4
__pstringtext4:
	file	"FrameworkSource/ES_Framework.c"
	line	84
_ServDescList:
	retlw	low(_InitTestHarnessService0)
	retlw	high(_InitTestHarnessService0)

	retlw	low(_RunTestHarnessService0)
	retlw	high(_RunTestHarnessService0)

	global __end_of_ServDescList
__end_of_ServDescList:
	global	_EventQueues
psect	stringtext5,class=STRCODE,delta=2,noexec
global __pstringtext5
__pstringtext5:
	file	"FrameworkSource/ES_Framework.c"
	line	186
_EventQueues:
	retlw	low(_Queue0|((0x1)<<8))
	retlw	06h
	global __end_of_EventQueues
__end_of_EventQueues:
	global	_ES_EventList
psect	stringtext6,class=STRCODE,delta=2,noexec
global __pstringtext6
__pstringtext6:
	file	"FrameworkSource/ES_CheckEvents.c"
	line	27
_ES_EventList:
	retlw	low(_Check4Keystroke)
	retlw	high(_Check4Keystroke)

	global __end_of_ES_EventList
__end_of_ES_EventList:
	global	_Timer2PostFunc
	global	_BitNum2SetMask
	global	_Nybble2MSBitNum
	global	_ServDescList
	global	_EventQueues
	global	_ES_EventList
	global	ES_Timer_Tick_Resp@NextTimer2Process
	global	ES_Timer_Tick_Resp@NewEvent
	global	_Ready
	global	ES_Timer_Tick_Resp@NeedsProcessing
	global	_TMR_ActiveFlags
	global	_SysTickCounter
	global	_MyPriority
	global	__INTCON_temp
	global	_TickCount
	global	_DeferralQueue
	global	_FieldBuf
	global	ES_Run@ThisEvent
	global	_TMR_TimerArray
	global	_Queue0
	global	_TRISCbits
_TRISCbits	set	0x14
	global	_INTCON
_INTCON	set	0xB
	global	_GIE
_GIE	set	0x5F
	global	_PEIE
_PEIE	set	0x5E
	global	_RC1REG
_RC1REG	set	0x119
	global	_TX1REG
_TX1REG	set	0x11A
	global	_RC1STAbits
_RC1STAbits	set	0x11D
	global	_TX1STAbits
_TX1STAbits	set	0x11E
	global	_SP1BRGL
_SP1BRGL	set	0x11B
	global	_T2TMR
_T2TMR	set	0x28C
	global	_T2CONbits
_T2CONbits	set	0x28E
	global	_T2PR
_T2PR	set	0x28D
	global	_T2CON
_T2CON	set	0x28E
	global	_T2CLKCON
_T2CLKCON	set	0x290
	global	_CCP2CONbits
_CCP2CONbits	set	0x312
	global	_CCPR2H
_CCPR2H	set	0x311
	global	_NCO1INCL
_NCO1INCL	set	0x58F
	global	_NCO1INCH
_NCO1INCH	set	0x590
	global	_NCO1INCU
_NCO1INCU	set	0x591
	global	_NCO1CLK
_NCO1CLK	set	0x593
	global	_N1EN
_N1EN	set	0x2C97
	global	_PIR4bits
_PIR4bits	set	0x710
	global	_NCO1IF
_NCO1IF	set	0x389C
	global	_RC1IF
_RC1IF	set	0x387D
	global	_TX1IF
_TX1IF	set	0x387C
	global	_NCOIF
_NCOIF	set	0x389C
	global	_NCO1IE
_NCO1IE	set	0x38EC
	global	_ANSELCbits
_ANSELCbits	set	0x1F4E
	global	_RC1PPS
_RC1PPS	set	0x1F21
	global	_RC6PPS
_RC6PPS	set	0x1F26
	global	_ANSELC
_ANSELC	set	0x1F4E
	
STR_8:	
	retlw	80	;'P'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	97	;'a'
	retlw	110	;'n'
	retlw	121	;'y'
	retlw	32	;' '
	retlw	107	;'k'
	retlw	101	;'e'
	retlw	121	;'y'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	32	;' '
	retlw	112	;'p'
	retlw	111	;'o'
	retlw	115	;'s'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	107	;'k'
	retlw	101	;'e'
	retlw	121	;'y'
	retlw	45	;'-'
	retlw	115	;'s'
	retlw	116	;'t'
	retlw	114	;'r'
	retlw	111	;'o'
	retlw	107	;'k'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	101	;'e'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	48	;'0'
	retlw	10
	retlw	13
	retlw	0
psect	stringtext7,class=STRCODE,delta=2,noexec
global __pstringtext7
__pstringtext7:
	
STR_3:	
	retlw	116	;'t'
	retlw	104	;'h'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	50	;'2'
	retlw	110	;'n'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	71	;'G'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	97	;'a'
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	69	;'E'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	38	;'&'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	70	;'F'
	retlw	114	;'r'
	retlw	97	;'a'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	119	;'w'
	retlw	111	;'o'
	retlw	114	;'r'
	retlw	107	;'k'
	retlw	32	;' '
	retlw	86	;'V'
	retlw	50	;'2'
	retlw	46	;'.'
	retlw	52	;'4'
	retlw	13
	retlw	10
	retlw	0
psect	stringtext8,class=STRCODE,delta=2,noexec
global __pstringtext8
__pstringtext8:
	
STR_17:	
	retlw	69	;'E'
	retlw	83	;'S'
	retlw	95	;'_'
	retlw	84	;'T'
	retlw	73	;'I'
	retlw	77	;'M'
	retlw	69	;'E'
	retlw	79	;'O'
	retlw	85	;'U'
	retlw	84	;'T'
	retlw	32	;' '
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	105	;'i'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	102	;'f'
	retlw	114	;'r'
	retlw	111	;'o'
	retlw	109	;'m'
	retlw	32	;' '
	retlw	84	;'T'
	retlw	105	;'i'
	retlw	109	;'m'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	13
	retlw	10
	retlw	0
psect	stringtext9,class=STRCODE,delta=2,noexec
global __pstringtext9
__pstringtext9:
	
STR_19:	
	retlw	69	;'E'
	retlw	83	;'S'
	retlw	95	;'_'
	retlw	78	;'N'
	retlw	69	;'E'
	retlw	87	;'W'
	retlw	95	;'_'
	retlw	75	;'K'
	retlw	69	;'E'
	retlw	89	;'Y'
	retlw	32	;' '
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	105	;'i'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	119	;'w'
	retlw	105	;'i'
	retlw	116	;'t'
	retlw	104	;'h'
	retlw	32	;' '
	retlw	45	;'-'
	retlw	62	;'>'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	99	;'c'
	retlw	32	;' '
	retlw	60	;'<'
	retlw	45	;'-'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	48	;'0'
	retlw	13
	retlw	10
	retlw	0
psect	stringtext10,class=STRCODE,delta=2,noexec
global __pstringtext10
__pstringtext10:
	
STR_21:	
	retlw	69	;'E'
	retlw	83	;'S'
	retlw	95	;'_'
	retlw	78	;'N'
	retlw	69	;'E'
	retlw	87	;'W'
	retlw	95	;'_'
	retlw	75	;'K'
	retlw	69	;'E'
	retlw	89	;'Y'
	retlw	40	;'('
	retlw	115	;'s'
	retlw	41	;')'
	retlw	32	;' '
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	97	;'a'
	retlw	108	;'l'
	retlw	108	;'l'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	48	;'0'
	retlw	13
	retlw	0
psect	stringtext11,class=STRCODE,delta=2,noexec
global __pstringtext11
__pstringtext11:
	
STR_9:	
	retlw	80	;'P'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	39	;'''
	retlw	100	;'d'
	retlw	39	;'''
	retlw	32	;' '
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	101	;'e'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	100	;'d'
	retlw	101	;'e'
	retlw	102	;'f'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	114	;'r'
	retlw	97	;'a'
	retlw	108	;'l'
	retlw	32	;' '
	retlw	10
	retlw	13
	retlw	0
psect	stringtext12,class=STRCODE,delta=2,noexec
global __pstringtext12
__pstringtext12:
	
STR_16:	
	retlw	13
	retlw	69	;'E'
	retlw	83	;'S'
	retlw	95	;'_'
	retlw	73	;'I'
	retlw	78	;'N'
	retlw	73	;'I'
	retlw	84	;'T'
	retlw	32	;' '
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	105	;'i'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	100	;'d'
	retlw	13
	retlw	10
	retlw	0
psect	stringtext13,class=STRCODE,delta=2,noexec
global __pstringtext13
__pstringtext13:
	
STR_20:	
	retlw	69	;'E'
	retlw	83	;'S'
	retlw	95	;'_'
	retlw	78	;'N'
	retlw	69	;'E'
	retlw	87	;'W'
	retlw	95	;'_'
	retlw	75	;'K'
	retlw	69	;'E'
	retlw	89	;'Y'
	retlw	32	;' '
	retlw	100	;'d'
	retlw	101	;'e'
	retlw	102	;'f'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	48	;'0'
	retlw	13
	retlw	0
psect	stringtext14,class=STRCODE,delta=2,noexec
global __pstringtext14
__pstringtext14:
	
STR_10:	
	retlw	80	;'P'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	39	;'''
	retlw	114	;'r'
	retlw	39	;'''
	retlw	32	;' '
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	101	;'e'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	97	;'a'
	retlw	108	;'l'
	retlw	108	;'l'
	retlw	32	;' '
	retlw	10
	retlw	13
	retlw	0
psect	stringtext15,class=STRCODE,delta=2,noexec
global __pstringtext15
__pstringtext15:
	
STR_2:	
	retlw	13
	retlw	83	;'S'
	retlw	116	;'t'
	retlw	97	;'a'
	retlw	114	;'r'
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	103	;'g'
	retlw	32	;' '
	retlw	84	;'T'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	72	;'H'
	retlw	97	;'a'
	retlw	114	;'r'
	retlw	110	;'n'
	retlw	101	;'e'
	retlw	115	;'s'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	102	;'f'
	retlw	111	;'o'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	13
	retlw	0
psect	stringtext16,class=STRCODE,delta=2,noexec
global __pstringtext16
__pstringtext16:
	
STR_11:	
	retlw	70	;'F'
	retlw	97	;'a'
	retlw	105	;'i'
	retlw	108	;'l'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	97	;'a'
	retlw	116	;'t'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	109	;'m'
	retlw	112	;'p'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	116	;'t'
	retlw	111	;'o'
	retlw	32	;' '
	retlw	80	;'P'
	retlw	111	;'o'
	retlw	115	;'s'
	retlw	116	;'t'
	retlw	10
	retlw	0
psect	stringtext17,class=STRCODE,delta=2,noexec
global __pstringtext17
__pstringtext17:
	
STR_18:	
	retlw	69	;'E'
	retlw	83	;'S'
	retlw	95	;'_'
	retlw	83	;'S'
	retlw	72	;'H'
	retlw	79	;'O'
	retlw	82	;'R'
	retlw	84	;'T'
	retlw	95	;'_'
	retlw	84	;'T'
	retlw	73	;'I'
	retlw	77	;'M'
	retlw	69	;'E'
	retlw	79	;'O'
	retlw	85	;'U'
	retlw	84	;'T'
	retlw	32	;' '
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	105	;'i'
	retlw	118	;'v'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	0
psect	stringtext18,class=STRCODE,delta=2,noexec
global __pstringtext18
__pstringtext18:
	
STR_12:	
	retlw	70	;'F'
	retlw	97	;'a'
	retlw	105	;'i'
	retlw	108	;'l'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	32	;' '
	retlw	78	;'N'
	retlw	85	;'U'
	retlw	76	;'L'
	retlw	76	;'L'
	retlw	32	;' '
	retlw	112	;'p'
	retlw	111	;'o'
	retlw	105	;'i'
	retlw	110	;'n'
	retlw	116	;'t'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	10
	retlw	0
psect	stringtext19,class=STRCODE,delta=2,noexec
global __pstringtext19
__pstringtext19:
	
STR_13:	
	retlw	70	;'F'
	retlw	97	;'a'
	retlw	105	;'i'
	retlw	108	;'l'
	retlw	101	;'e'
	retlw	100	;'d'
	retlw	32	;' '
	retlw	73	;'I'
	retlw	110	;'n'
	retlw	105	;'i'
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	97	;'a'
	retlw	108	;'l'
	retlw	105	;'i'
	retlw	122	;'z'
	retlw	97	;'a'
	retlw	116	;'t'
	retlw	105	;'i'
	retlw	111	;'o'
	retlw	110	;'n'
	retlw	10
	retlw	0
psect	stringtext20,class=STRCODE,delta=2,noexec
global __pstringtext20
__pstringtext20:
	
STR_14:	
	retlw	79	;'O'
	retlw	116	;'t'
	retlw	104	;'h'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	32	;' '
	retlw	70	;'F'
	retlw	97	;'a'
	retlw	105	;'i'
	retlw	108	;'l'
	retlw	117	;'u'
	retlw	114	;'r'
	retlw	101	;'e'
	retlw	10
	retlw	0
psect	stringtext21,class=STRCODE,delta=2,noexec
global __pstringtext21
__pstringtext21:
	
STR_15:	
	retlw	83	;'S'
	retlw	101	;'e'
	retlw	114	;'r'
	retlw	118	;'v'
	retlw	105	;'i'
	retlw	99	;'c'
	retlw	101	;'e'
	retlw	32	;' '
	retlw	48	;'0'
	retlw	48	;'0'
	retlw	58	;':'
	retlw	0
psect	stringtext22,class=STRCODE,delta=2,noexec
global __pstringtext22
__pstringtext22:
	
STR_6:	
	retlw	77	;'M'
	retlw	97	;'a'
	retlw	121	;'y'
	retlw	32	;' '
	retlw	49	;'1'
	retlw	49	;'1'
	retlw	32	;' '
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	50	;'2'
	retlw	48	;'0'
	retlw	0
psect	stringtext23,class=STRCODE,delta=2,noexec
global __pstringtext23
__pstringtext23:
	
STR_24:	
	retlw	73	;'I'
	retlw	110	;'n'
	retlw	105	;'i'
	retlw	116	;'t'
	retlw	32	;' '
	retlw	80	;'P'
	retlw	87	;'W'
	retlw	77	;'M'
	retlw	13
	retlw	10
	retlw	0
psect	stringtext24,class=STRCODE,delta=2,noexec
global __pstringtext24
__pstringtext24:
	
STR_5:	
	retlw	48	;'0'
	retlw	48	;'0'
	retlw	58	;':'
	retlw	50	;'2'
	retlw	57	;'9'
	retlw	58	;':'
	retlw	49	;'1'
	retlw	49	;'1'
	retlw	0
psect	stringtext25,class=STRCODE,delta=2,noexec
global __pstringtext25
__pstringtext25:
	
STR_22:	
	retlw	40	;'('
	retlw	110	;'n'
	retlw	117	;'u'
	retlw	108	;'l'
	retlw	108	;'l'
	retlw	41	;')'
	retlw	0
psect	stringtext26,class=STRCODE,delta=2,noexec
global __pstringtext26
__pstringtext26:
	
STR_23:	
	retlw	97	;'a'
	retlw	98	;'b'
	retlw	99	;'c'
	retlw	100	;'d'
	retlw	101	;'e'
	retlw	102	;'f'
	retlw	0
psect	stringtext27,class=STRCODE,delta=2,noexec
global __pstringtext27
__pstringtext27:
	
STR_4:	
	retlw	37	;'%'
	retlw	115	;'s'
	retlw	32	;' '
	retlw	37	;'%'
	retlw	115	;'s'
	retlw	10
	retlw	0
psect	stringtext28,class=STRCODE,delta=2,noexec
global __pstringtext28
__pstringtext28:
	
STR_1:	
	retlw	27
	retlw	91	;'['
	retlw	50	;'2'
	retlw	74	;'J'
	retlw	0
psect	stringtext29,class=STRCODE,delta=2,noexec
global __pstringtext29
__pstringtext29:
	
STR_7:	
	retlw	10
	retlw	13
	retlw	10
	retlw	0
psect	stringtext30,class=STRCODE,delta=2,noexec
global __pstringtext30
__pstringtext30:
; #config settings
global __CFG_FEXTOSC$OFF
__CFG_FEXTOSC$OFF equ 0x0
global __CFG_RSTOSC$HFINT32
__CFG_RSTOSC$HFINT32 equ 0x0
global __CFG_CLKOUTEN$OFF
__CFG_CLKOUTEN$OFF equ 0x0
global __CFG_CSWEN$OFF
__CFG_CSWEN$OFF equ 0x0
global __CFG_FCMEN$ON
__CFG_FCMEN$ON equ 0x0
global __CFG_MCLRE$ON
__CFG_MCLRE$ON equ 0x0
global __CFG_PWRTE$ON
__CFG_PWRTE$ON equ 0x0
global __CFG_LPBOREN$OFF
__CFG_LPBOREN$OFF equ 0x0
global __CFG_BOREN$ON
__CFG_BOREN$ON equ 0x0
global __CFG_BORV$LO
__CFG_BORV$LO equ 0x0
global __CFG_ZCD$OFF
__CFG_ZCD$OFF equ 0x0
global __CFG_PPS1WAY$OFF
__CFG_PPS1WAY$OFF equ 0x0
global __CFG_STVREN$ON
__CFG_STVREN$ON equ 0x0
global __CFG_WDTCPS$WDTCPS_31
__CFG_WDTCPS$WDTCPS_31 equ 0x0
global __CFG_WDTE$OFF
__CFG_WDTE$OFF equ 0x0
global __CFG_WDTCWS$WDTCWS_7
__CFG_WDTCWS$WDTCWS_7 equ 0x0
global __CFG_WDTCCS$SC
__CFG_WDTCCS$SC equ 0x0
global __CFG_BBSIZE$BB512
__CFG_BBSIZE$BB512 equ 0x0
global __CFG_BBEN$OFF
__CFG_BBEN$OFF equ 0x0
global __CFG_SAFEN$OFF
__CFG_SAFEN$OFF equ 0x0
global __CFG_WRTAPP$OFF
__CFG_WRTAPP$OFF equ 0x0
global __CFG_WRTB$OFF
__CFG_WRTB$OFF equ 0x0
global __CFG_WRTC$OFF
__CFG_WRTC$OFF equ 0x0
global __CFG_WRTSAF$OFF
__CFG_WRTSAF$OFF equ 0x0
global __CFG_LVP$ON
__CFG_LVP$ON equ 0x0
global __CFG_CP$OFF
__CFG_CP$OFF equ 0x0
	file	"dist/default/production\PICing_and_Screaming.production.s"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

global __initialization
__initialization:
psect	bssCOMMON,class=COMMON,space=1,noexec
global __pbssCOMMON
__pbssCOMMON:
ES_Timer_Tick_Resp@NextTimer2Process:
       ds      1

psect	bssBANK0,class=BANK0,space=1,noexec
global __pbssBANK0
__pbssBANK0:
ServoControl@DeferralQueue:
       ds      12

ES_Timer_Tick_Resp@NewEvent:
       ds      3

_Ready:
       ds      2

ES_Timer_Tick_Resp@NeedsProcessing:
       ds      2

_TMR_ActiveFlags:
       ds      2

_SysTickCounter:
       ds      2

ServoControl@MyPriority:
       ds      1

_MyPriority:
       ds      1

__INTCON_temp:
       ds      1

_TickCount:
       ds      1

_DeferralQueue:
       ds      12

_FieldBuf:
       ds      7

ES_Run@ThisEvent:
       ds      3

psect	dataBANK0,class=BANK0,space=1,noexec
global __pdataBANK0
__pdataBANK0:
	file	"ProjectSource/TestHarnessService0.c"
	line	161
RunTestHarnessService0@DeferredChar:
       ds      1

psect	bssBANK2,class=BANK2,space=1,noexec
global __pbssBANK2
__pbssBANK2:
_TMR_TimerArray:
       ds      32

_Queue0:
       ds      18

	file	"dist/default/production\PICing_and_Screaming.production.s"
	line	#
; Initialize objects allocated to BANK0
	global __pidataBANK0,__pdataBANK0
psect cinit,class=CODE,delta=2,merge=1
	fcall	__pidataBANK0+0		;fetch initializer
	movwf	__pdataBANK0+0&07fh		
	line	#
psect clrtext,class=CODE,delta=2
global clear_ram0
;	Called with FSR0 containing the base address, and
;	WREG with the size to clear
clear_ram0:
	clrwdt			;clear the watchdog before getting into this loop
clrloop0:
	clrf	indf0		;clear RAM location pointed to by FSR
	addfsr	0,1
	decfsz wreg		;Have we reached the end of clearing yet?
	goto clrloop0	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2,merge=1
	global __pbssCOMMON
	clrf	((__pbssCOMMON)+0)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2,merge=1
	global __pbssBANK0
	movlw	low(__pbssBANK0)
	movwf	fsr0l
	movlw	high(__pbssBANK0)
	movwf	fsr0h
	movlw	031h
	fcall	clear_ram0
; Clear objects allocated to BANK2
psect cinit,class=CODE,delta=2,merge=1
	global __pbssBANK2
	movlw	low(__pbssBANK2)
	movwf	fsr0l
	movlw	high(__pbssBANK2)
	movwf	fsr0h
	movlw	032h
	fcall	clear_ram0
psect cinit,class=CODE,delta=2,merge=1
global end_of_initialization,__end_of__initialization

;End of C runtime variable initialization code

end_of_initialization:
__end_of__initialization:
movlb 0

;Initialize the stack pointer (FSR1)
	global ___sp
	movlw low(___sp)
	movwf fsr1l
	movlw high(___sp)
	movwf fsr1h
ljmp _main	;jump to C main() function
psect	cstackBANK1,class=BANK1,space=1,noexec
global __pcstackBANK1
__pcstackBANK1:
	global	DB_printf@LineBuffer
DB_printf@LineBuffer:	; 61 bytes @ 0x0
	ds	61
	global	DB_printf@u
DB_printf@u:	; 2 bytes @ 0x3D
	ds	2
	global	DB_printf@i
DB_printf@i:	; 2 bytes @ 0x3F
	ds	2
	global	DB_printf@pString
DB_printf@pString:	; 2 bytes @ 0x41
	ds	2
	global	DB_printf@Arguments
DB_printf@Arguments:	; 1 bytes @ 0x43
	ds	1
	global	DB_printf@pBuffer
DB_printf@pBuffer:	; 1 bytes @ 0x44
	ds	1
	global	RunTestHarnessService0@ReturnEvent
RunTestHarnessService0@ReturnEvent:	; 3 bytes @ 0x45
	ds	3
	global	main@ErrorType
main@ErrorType:	; 1 bytes @ 0x48
	ds	1
psect	cstackCOMMON,class=COMMON,space=1,noexec
global __pcstackCOMMON
__pcstackCOMMON:
?__HW_ConsoleInit:	; 1 bytes @ 0x0
?_ES_Timer_Tick_Resp:	; 1 bytes @ 0x0
?_Check4Keystroke:	; 1 bytes @ 0x0
?_InitPWM:	; 1 bytes @ 0x0
?_InitLED:	; 1 bytes @ 0x0
?_BlinkLED:	; 1 bytes @ 0x0
?_Start_PushToTalk:	; 1 bytes @ 0x0
?_Stop_PushToTalk:	; 1 bytes @ 0x0
?__HW_PIC15356Init:	; 1 bytes @ 0x0
?__HW_SysTickIntHandler:	; 1 bytes @ 0x0
??__HW_SysTickIntHandler:	; 1 bytes @ 0x0
?__HW_Process_Pending_Ints:	; 1 bytes @ 0x0
?_putch:	; 1 bytes @ 0x0
?_ES_CheckUserEvents:	; 1 bytes @ 0x0
?_ES_Run:	; 1 bytes @ 0x0
?_main:	; 1 bytes @ 0x0
?_myIsr:	; 1 bytes @ 0x0
??_myIsr:	; 1 bytes @ 0x0
	ds	1
??__HW_ConsoleInit:	; 1 bytes @ 0x1
??_ES_GetMSBitSet:	; 1 bytes @ 0x1
??_ES_PostToServiceLIFO:	; 1 bytes @ 0x1
??_InitTestHarnessService0:	; 1 bytes @ 0x1
??_ES_InitQueue:	; 1 bytes @ 0x1
??_InitLED:	; 1 bytes @ 0x1
??_BlinkLED:	; 1 bytes @ 0x1
??_Start_PushToTalk:	; 1 bytes @ 0x1
??_Stop_PushToTalk:	; 1 bytes @ 0x1
??__HW_PIC15356Init:	; 1 bytes @ 0x1
??__HW_Timer_Init:	; 1 bytes @ 0x1
??_putch:	; 1 bytes @ 0x1
??_getchar:	; 1 bytes @ 0x1
??_ES_Timer_Init:	; 1 bytes @ 0x1
??_ES_PostAll:	; 1 bytes @ 0x1
??___wmul:	; 1 bytes @ 0x1
??___awmod:	; 1 bytes @ 0x1
?___lbmod:	; 1 bytes @ 0x1
??___lwdiv:	; 1 bytes @ 0x1
??___lwmod:	; 1 bytes @ 0x1
	global	putch@data
putch@data:	; 1 bytes @ 0x1
	global	ES_Timer_InitTimer@Num
ES_Timer_InitTimer@Num:	; 1 bytes @ 0x1
	global	ES_Initialize@i
ES_Initialize@i:	; 1 bytes @ 0x1
	global	ES_PostAll@i
ES_PostAll@i:	; 1 bytes @ 0x1
	global	___lbmod@divisor
___lbmod@divisor:	; 1 bytes @ 0x1
	global	___lwmod@counter
___lwmod@counter:	; 1 bytes @ 0x1
	ds	1
??_Check4Keystroke:	; 1 bytes @ 0x2
??_RunTestHarnessService0:	; 1 bytes @ 0x2
??_puts:	; 1 bytes @ 0x2
??_InitPWM:	; 1 bytes @ 0x2
??_uitoa:	; 1 bytes @ 0x2
??_fputs:	; 1 bytes @ 0x2
??__HW_Process_Pending_Ints:	; 1 bytes @ 0x2
??_ES_CheckUserEvents:	; 1 bytes @ 0x2
??_ES_RecallEvents:	; 1 bytes @ 0x2
??_main:	; 1 bytes @ 0x2
psect	cstackBANK0,class=BANK0,space=1,noexec
global __pcstackBANK0
__pcstackBANK0:
?_ES_GetMSBitSet:	; 1 bytes @ 0x0
?_ES_InitQueue:	; 1 bytes @ 0x0
?__HW_Timer_Init:	; 1 bytes @ 0x0
?_ES_Timer_InitTimer:	; 1 bytes @ 0x0
??___lbmod:	; 1 bytes @ 0x0
	global	?_fputc
?_fputc:	; 2 bytes @ 0x0
	global	?_getchar
?_getchar:	; 2 bytes @ 0x0
	global	?___wmul
?___wmul:	; 2 bytes @ 0x0
	global	?___awmod
?___awmod:	; 2 bytes @ 0x0
	global	?___lwdiv
?___lwdiv:	; 2 bytes @ 0x0
	global	?___lwmod
?___lwmod:	; 2 bytes @ 0x0
	global	__HW_Timer_Init@Rate
__HW_Timer_Init@Rate:	; 2 bytes @ 0x0
	global	ES_Timer_InitTimer@NewTime
ES_Timer_InitTimer@NewTime:	; 2 bytes @ 0x0
	global	ES_GetMSBitSet@Val2Check
ES_GetMSBitSet@Val2Check:	; 2 bytes @ 0x0
	global	ES_InitQueue@pBlock
ES_InitQueue@pBlock:	; 2 bytes @ 0x0
	global	___wmul@multiplier
___wmul@multiplier:	; 2 bytes @ 0x0
	global	___awmod@divisor
___awmod@divisor:	; 2 bytes @ 0x0
	global	___lwdiv@divisor
___lwdiv@divisor:	; 2 bytes @ 0x0
	global	___lwmod@divisor
___lwmod@divisor:	; 2 bytes @ 0x0
	global	fputc@c
fputc@c:	; 2 bytes @ 0x0
	ds	1
	global	___lbmod@dividend
___lbmod@dividend:	; 1 bytes @ 0x1
	ds	1
?_ES_Timer_Init:	; 1 bytes @ 0x2
??_ES_Timer_InitTimer:	; 1 bytes @ 0x2
	global	ES_GetMSBitSet@ReturnVal
ES_GetMSBitSet@ReturnVal:	; 1 bytes @ 0x2
	global	ES_InitQueue@BlockSize
ES_InitQueue@BlockSize:	; 1 bytes @ 0x2
	global	___lbmod@counter
___lbmod@counter:	; 1 bytes @ 0x2
	global	fputc@fp
fputc@fp:	; 1 bytes @ 0x2
	global	ES_Timer_Init@Rate
ES_Timer_Init@Rate:	; 2 bytes @ 0x2
	global	___wmul@multiplicand
___wmul@multiplicand:	; 2 bytes @ 0x2
	global	___awmod@dividend
___awmod@dividend:	; 2 bytes @ 0x2
	global	___lwdiv@dividend
___lwdiv@dividend:	; 2 bytes @ 0x2
	global	___lwmod@dividend
___lwmod@dividend:	; 2 bytes @ 0x2
	ds	1
??_fputc:	; 1 bytes @ 0x3
	global	ES_GetMSBitSet@Nybble2Test
ES_GetMSBitSet@Nybble2Test:	; 1 bytes @ 0x3
	global	___lbmod@rem
___lbmod@rem:	; 1 bytes @ 0x3
	global	ES_InitQueue@pThisQueue
ES_InitQueue@pThisQueue:	; 2 bytes @ 0x3
	ds	1
	global	ES_GetMSBitSet@LoopCntr
ES_GetMSBitSet@LoopCntr:	; 1 bytes @ 0x4
	global	___awmod@counter
___awmod@counter:	; 1 bytes @ 0x4
	global	___lwdiv@counter
___lwdiv@counter:	; 1 bytes @ 0x4
	global	___wmul@product
___wmul@product:	; 2 bytes @ 0x4
	ds	1
	global	?_fputs
?_fputs:	; 2 bytes @ 0x5
	global	___awmod@sign
___awmod@sign:	; 1 bytes @ 0x5
	global	___lwdiv@quotient
___lwdiv@quotient:	; 2 bytes @ 0x5
	global	fputs@s
fputs@s:	; 2 bytes @ 0x5
	ds	1
??_ES_Timer_Tick_Resp:	; 1 bytes @ 0x6
?_ES_DeQueue:	; 1 bytes @ 0x6
?_InitTestHarnessService0:	; 1 bytes @ 0x6
?_ES_EnQueueLIFO:	; 1 bytes @ 0x6
?_ES_PostAll:	; 1 bytes @ 0x6
	global	InitTestHarnessService0@Priority
InitTestHarnessService0@Priority:	; 1 bytes @ 0x6
	global	ES_EnQueueLIFO@pBlock
ES_EnQueueLIFO@pBlock:	; 2 bytes @ 0x6
	global	ES_DeQueue@pBlock
ES_DeQueue@pBlock:	; 2 bytes @ 0x6
	global	ES_PostAll@ThisEvent
ES_PostAll@ThisEvent:	; 3 bytes @ 0x6
	ds	1
?_uitoa:	; 1 bytes @ 0x7
	global	fputs@fp
fputs@fp:	; 1 bytes @ 0x7
	global	uitoa@i
uitoa@i:	; 2 bytes @ 0x7
	global	InitTestHarnessService0@ThisEvent
InitTestHarnessService0@ThisEvent:	; 3 bytes @ 0x7
	ds	1
	global	ES_DeQueue@pReturnEvent
ES_DeQueue@pReturnEvent:	; 1 bytes @ 0x8
	global	fputs@c
fputs@c:	; 1 bytes @ 0x8
	global	ES_EnQueueLIFO@Event2Add
ES_EnQueueLIFO@Event2Add:	; 3 bytes @ 0x8
	ds	1
??_ES_DeQueue:	; 1 bytes @ 0x9
	global	uitoa@base
uitoa@base:	; 2 bytes @ 0x9
	global	fputs@i
fputs@i:	; 2 bytes @ 0x9
	global	Check4Keystroke@ThisEvent
Check4Keystroke@ThisEvent:	; 3 bytes @ 0x9
	ds	1
?_ES_Initialize:	; 1 bytes @ 0xA
	global	ES_Initialize@NewRate
ES_Initialize@NewRate:	; 2 bytes @ 0xA
	ds	1
??_ES_EnQueueLIFO:	; 1 bytes @ 0xB
?_puts:	; 2 bytes @ 0xB
	global	uitoa@rem
uitoa@rem:	; 2 bytes @ 0xB
	global	puts@s
puts@s:	; 2 bytes @ 0xB
	ds	1
??_ES_Initialize:	; 1 bytes @ 0xC
	global	ES_CheckUserEvents@i
ES_CheckUserEvents@i:	; 1 bytes @ 0xC
	global	ES_DeQueue@NumLeft
ES_DeQueue@NumLeft:	; 1 bytes @ 0xC
	ds	1
	global	uitoa@s
uitoa@s:	; 1 bytes @ 0xD
	global	ES_EnQueueLIFO@pThisQueue
ES_EnQueueLIFO@pThisQueue:	; 2 bytes @ 0xD
	global	ES_DeQueue@pThisQueue
ES_DeQueue@pThisQueue:	; 2 bytes @ 0xD
	ds	1
	global	uitoa@LineBuffer
uitoa@LineBuffer:	; 1 bytes @ 0xE
	ds	1
?_ES_PostToServiceLIFO:	; 1 bytes @ 0xF
	global	?_DB_printf
?_DB_printf:	; 1 bytes @ 0xF
	global	_puts$1226
_puts$1226:	; 1 bytes @ 0xF
	global	DB_printf@Format
DB_printf@Format:	; 2 bytes @ 0xF
	global	ES_PostToServiceLIFO@TheEvent
ES_PostToServiceLIFO@TheEvent:	; 3 bytes @ 0xF
	ds	3
	global	ES_PostToServiceLIFO@WhichService
ES_PostToServiceLIFO@WhichService:	; 1 bytes @ 0x12
	ds	1
?_ES_RecallEvents:	; 1 bytes @ 0x13
	global	ES_RecallEvents@pBlock
ES_RecallEvents@pBlock:	; 1 bytes @ 0x13
	ds	1
	global	ES_RecallEvents@WhichService
ES_RecallEvents@WhichService:	; 1 bytes @ 0x14
	ds	1
??_DB_printf:	; 1 bytes @ 0x15
	global	ES_RecallEvents@WereEventsPulled
ES_RecallEvents@WereEventsPulled:	; 1 bytes @ 0x15
	ds	1
	global	ES_RecallEvents@RecalledEvent
ES_RecallEvents@RecalledEvent:	; 3 bytes @ 0x16
	ds	3
	global	?_RunTestHarnessService0
?_RunTestHarnessService0:	; 3 bytes @ 0x19
	global	RunTestHarnessService0@ThisEvent
RunTestHarnessService0@ThisEvent:	; 3 bytes @ 0x19
	ds	3
??_ES_Run:	; 1 bytes @ 0x1C
	ds	1
	global	ES_Run@HighestPrior
ES_Run@HighestPrior:	; 1 bytes @ 0x1D
	ds	1
;!
;!Data Sizes:
;!    Strings     601
;!    Constant    87
;!    Data        1
;!    BSS         100
;!    Persistent  32
;!    Stack       0
;!
;!Auto Spaces:
;!    Space          Size  Autos    Used
;!    COMMON            4      2       3
;!    BANK0            80     30      80
;!    BANK1            80     73      73
;!    BANK2            80      0      50
;!    BANK3            80      0       0
;!    BANK4            80      0       0
;!    BANK5            80      0       0
;!    BANK6            80      0       0
;!    BANK7            80      0       0
;!    BANK8            80      0       0
;!    BANK9            80      0       0
;!    BANK10           80      0       0
;!    BANK11           80      0       0
;!    BANK12           80      0       0
;!    BANK13           80      0       0
;!    BANK14           80      0       0
;!    BANK15           80      0       0
;!    BANK16           80      0       0
;!    BANK17           80      0       0
;!    BANK18           80      0       0
;!    BANK19           80      0       0
;!    BANK20           80      0       0
;!    BANK21           80      0       0
;!    BANK22           80      0       0
;!    BANK23           80      0       0
;!    BANK24           80      0       0
;!    BANK25           32      0       0

;!
;!Pointer List with Targets:
;!
;!    ES_CheckUserEvents$1432	const PTR FTN()_Bool  size(2) Largest target is 1
;!		 -> Check4Keystroke(), 
;!
;!    ES_Timer_Tick_Resp$1427	const PTR FTN(struct ES_Event,)_Bool  size(2) Largest target is 1
;!		 -> PostTestHarnessService0(), Absolute function(), 
;!
;!    ES_PostToServiceLIFO$1422	const PTR struct ES_Event size(1) Largest target is 18
;!		 -> Queue0(BANK2[18]), 
;!
;!    ES_Run$1419	const PTR struct ES_Event size(1) Largest target is 18
;!		 -> Queue0(BANK2[18]), 
;!
;!    fputs@s	PTR const unsigned char  size(2) Largest target is 37
;!		 -> STR_21(CODE[37]), STR_20(CODE[34]), STR_18(CODE[26]), STR_15(CODE[12]), 
;!		 -> STR_2(CODE[29]), 
;!
;!    fputs@fp	PTR struct _IO_FILE size(1) Largest target is 0
;!		 -> NULL(NULL[0]), 
;!
;!    S1207_IO_FILE$buffer	PTR unsigned char  size(1) Largest target is 0
;!
;!    fputc@fp.buffer	PTR unsigned char  size(1) Largest target is 0
;!
;!    fputc@fp	PTR struct _IO_FILE size(1) Largest target is 0
;!		 -> NULL(NULL[0]), 
;!
;!    puts@s	PTR const unsigned char  size(2) Largest target is 37
;!		 -> STR_21(CODE[37]), STR_20(CODE[34]), STR_18(CODE[26]), STR_15(CODE[12]), 
;!		 -> STR_2(CODE[29]), 
;!
;!    uitoa@s	PTR unsigned char  size(1) Largest target is 7
;!		 -> FieldBuf(BANK0[7]), 
;!
;!    uitoa@LineBuffer	PTR PTR unsigned char  size(1) Largest target is 1
;!		 -> DB_printf@pBuffer(BANK1[1]), 
;!
;!    DB_printf@pString	PTR unsigned char  size(2) Largest target is 12
;!		 -> STR_22(CODE[7]), ?_DB_printf(BANK0[1]), STR_6(CODE[12]), STR_5(CODE[9]), 
;!
;!    DB_printf@Format	PTR const unsigned char  size(2) Largest target is 55
;!		 -> STR_24(CODE[11]), STR_19(CODE[49]), STR_17(CODE[50]), STR_16(CODE[34]), 
;!		 -> STR_14(CODE[15]), STR_13(CODE[23]), STR_12(CODE[24]), STR_11(CODE[27]), 
;!		 -> STR_10(CODE[34]), STR_9(CODE[36]), STR_8(CODE[55]), STR_7(CODE[4]), 
;!		 -> STR_4(CODE[7]), STR_3(CODE[54]), STR_1(CODE[5]), 
;!
;!    DB_printf@pBuffer	PTR unsigned char  size(1) Largest target is 61
;!		 -> DB_printf@LineBuffer(BANK1[61]), 
;!
;!    DB_printf@Arguments	PTR void [1] size(1) Largest target is 1
;!		 -> ?_DB_printf(BANK0[1]), 
;!
;!    ES_DeQueue@pReturnEvent	PTR struct ES_Event size(1) Largest target is 3
;!		 -> ES_Run@ThisEvent(BANK0[3]), ES_RecallEvents@RecalledEvent(BANK0[3]), 
;!
;!    ES_DeQueue@pBlock	PTR struct ES_Event size(2) Largest target is 18
;!		 -> DeferralQueue(BANK0[12]), Queue0(BANK2[18]), 
;!
;!    ES_DeQueue@pThisQueue	PTR struct . size(2) Largest target is 18
;!		 -> DeferralQueue(BANK0[12]), Queue0(BANK2[18]), 
;!
;!    ES_EnQueueLIFO@pBlock	PTR struct ES_Event size(2) Largest target is 18
;!		 -> DeferralQueue(BANK0[12]), Queue0(BANK2[18]), 
;!
;!    ES_EnQueueLIFO@pThisQueue	PTR struct . size(2) Largest target is 18
;!		 -> DeferralQueue(BANK0[12]), Queue0(BANK2[18]), 
;!
;!    pBlock	PTR struct ES_Event size(1) Largest target is 18
;!		 -> Queue0(BANK2[18]), 
;!
;!    pThisQueue	PTR struct . size(1) Largest target is 18
;!		 -> Queue0(BANK2[18]), 
;!
;!    ES_InitQueue@pBlock	PTR struct ES_Event size(2) Largest target is 18
;!		 -> DeferralQueue(BANK0[12]), Queue0(BANK2[18]), 
;!
;!    ES_InitQueue@pThisQueue	PTR struct . size(2) Largest target is 18
;!		 -> DeferralQueue(BANK0[12]), Queue0(BANK2[18]), 
;!
;!    S303$pMem	PTR struct ES_Event size(1) Largest target is 18
;!		 -> Queue0(BANK2[18]), 
;!
;!    EventQueues.pMem	PTR struct ES_Event size(1) Largest target is 18
;!		 -> Queue0(BANK2[18]), 
;!
;!    S282$RunFunc	PTR FTN(struct ES_Event,)struct ES_Event size(2) Largest target is 3
;!		 -> RunTestHarnessService0(), 
;!
;!    ServDescList.RunFunc	PTR FTN(struct ES_Event,)struct ES_Event size(2) Largest target is 3
;!		 -> RunTestHarnessService0(), 
;!
;!    S282$InitFunc	PTR FTN(unsigned char ,)_Bool  size(2) Largest target is 1
;!		 -> InitTestHarnessService0(), 
;!
;!    ServDescList.InitFunc	PTR FTN(unsigned char ,)_Bool  size(2) Largest target is 1
;!		 -> InitTestHarnessService0(), 
;!
;!    ES_RecallEvents@pBlock	PTR struct ES_Event size(1) Largest target is 12
;!		 -> DeferralQueue(BANK0[12]), 
;!
;!    ES_EventList	const PTR FTN()_Bool [1] size(2) Largest target is 1
;!		 -> Check4Keystroke(), 
;!
;!    Timer2PostFunc	const PTR FTN(struct ES_Event,)_Bool [16] size(2) Largest target is 1
;!		 -> PostTestHarnessService0(), Absolute function(), 
;!


;!
;!Critical Paths under _main in COMMON
;!
;!    _main->_ES_Initialize
;!    _ES_Run->_ES_GetMSBitSet
;!    _ES_Timer_Tick_Resp->_ES_GetMSBitSet
;!    _RunTestHarnessService0->_ES_Timer_InitTimer
;!    _fputc->_putch
;!    _ES_RecallEvents->_ES_PostToServiceLIFO
;!    _ES_DeQueue->___lbmod
;!    _DB_printf->_putch
;!    _uitoa->___lwmod
;!    _Check4Keystroke->_ES_PostAll
;!
;!Critical Paths under _myIsr in COMMON
;!
;!    None.
;!
;!Critical Paths under _main in BANK0
;!
;!    _main->_ES_Run
;!    _InitPWM->_DB_printf
;!    _ES_Run->_RunTestHarnessService0
;!    __HW_Process_Pending_Ints->_ES_Timer_Tick_Resp
;!    _RunTestHarnessService0->_ES_RecallEvents
;!    _puts->_fputs
;!    _fputs->_fputc
;!    _ES_RecallEvents->_ES_PostToServiceLIFO
;!    _ES_PostToServiceLIFO->_ES_EnQueueLIFO
;!    _ES_EnQueueLIFO->___wmul
;!    _ES_DeQueue->___wmul
;!    _DB_printf->_uitoa
;!    _uitoa->___lwdiv
;!    _ES_CheckUserEvents->_Check4Keystroke
;!    _Check4Keystroke->_ES_PostAll
;!    _ES_Initialize->_InitTestHarnessService0
;!    _ES_EnQueueFIFO->___awmod
;!    _ES_Timer_Init->__HW_Timer_Init
;!
;!Critical Paths under _myIsr in BANK0
;!
;!    None.
;!
;!Critical Paths under _main in BANK1
;!
;!    _InitPWM->_DB_printf
;!    _ES_Run->_RunTestHarnessService0
;!    _RunTestHarnessService0->_DB_printf
;!
;!Critical Paths under _myIsr in BANK1
;!
;!    None.
;!
;!Critical Paths under _main in BANK2
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK2
;!
;!    None.
;!
;!Critical Paths under _main in BANK3
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK3
;!
;!    None.
;!
;!Critical Paths under _main in BANK4
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK4
;!
;!    None.
;!
;!Critical Paths under _main in BANK5
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK5
;!
;!    None.
;!
;!Critical Paths under _main in BANK6
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK6
;!
;!    None.
;!
;!Critical Paths under _main in BANK7
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK7
;!
;!    None.
;!
;!Critical Paths under _main in BANK8
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK8
;!
;!    None.
;!
;!Critical Paths under _main in BANK9
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK9
;!
;!    None.
;!
;!Critical Paths under _main in BANK10
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK10
;!
;!    None.
;!
;!Critical Paths under _main in BANK11
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK11
;!
;!    None.
;!
;!Critical Paths under _main in BANK12
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK12
;!
;!    None.
;!
;!Critical Paths under _main in BANK13
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK13
;!
;!    None.
;!
;!Critical Paths under _main in BANK14
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK14
;!
;!    None.
;!
;!Critical Paths under _main in BANK15
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK15
;!
;!    None.
;!
;!Critical Paths under _main in BANK16
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK16
;!
;!    None.
;!
;!Critical Paths under _main in BANK17
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK17
;!
;!    None.
;!
;!Critical Paths under _main in BANK18
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK18
;!
;!    None.
;!
;!Critical Paths under _main in BANK19
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK19
;!
;!    None.
;!
;!Critical Paths under _main in BANK20
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK20
;!
;!    None.
;!
;!Critical Paths under _main in BANK21
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK21
;!
;!    None.
;!
;!Critical Paths under _main in BANK22
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK22
;!
;!    None.
;!
;!Critical Paths under _main in BANK23
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK23
;!
;!    None.
;!
;!Critical Paths under _main in BANK24
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK24
;!
;!    None.
;!
;!Critical Paths under _main in BANK25
;!
;!    None.
;!
;!Critical Paths under _myIsr in BANK25
;!
;!    None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;!
;!Call Graph Tables:
;!
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (0) _main                                                 1     1      0   38107
;!                                             72 BANK1      1     1      0
;!                          _DB_printf
;!                      _ES_Initialize
;!                             _ES_Run
;!                            _InitPWM
;!                   __HW_PIC15356Init
;!                               _puts
;! ---------------------------------------------------------------------------------
;! (1) __HW_PIC15356Init                                     0     0      0       0
;!                    __HW_ConsoleInit
;! ---------------------------------------------------------------------------------
;! (2) __HW_ConsoleInit                                      0     0      0       0
;! ---------------------------------------------------------------------------------
;! (1) _InitPWM                                              0     0      0    5024
;!                          _DB_printf
;! ---------------------------------------------------------------------------------
;! (1) _ES_Run                                               2     2      0   23194
;!                                             28 BANK0      2     2      0
;!                 _ES_CheckUserEvents
;!                         _ES_DeQueue
;!                     _ES_GetMSBitSet
;!             _RunTestHarnessService0 *
;!           __HW_Process_Pending_Ints
;! ---------------------------------------------------------------------------------
;! (2) __HW_Process_Pending_Ints                             0     0      0    1493
;!                 _ES_Timer_Tick_Resp
;! ---------------------------------------------------------------------------------
;! (3) _ES_Timer_Tick_Resp                                   2     2      0    1493
;!                                              6 BANK0      2     2      0
;!                   Absolute function *
;!                     _ES_GetMSBitSet
;!            _PostTestHarnessService0 *
;! ---------------------------------------------------------------------------------
;! (4) _PostTestHarnessService0                              3     0      3    1050
;!                   _ES_PostToService
;! ---------------------------------------------------------------------------------
;! (4) _ES_GetMSBitSet                                       6     4      2     443
;!                                              1 COMMON     1     1      0
;!                                              0 BANK0      5     3      2
;! ---------------------------------------------------------------------------------
;! (4) Absolute function(Fake)                               3     0      3       0
;! ---------------------------------------------------------------------------------
;! (2) _RunTestHarnessService0                               6     3      3   17014
;!                                             25 BANK0      3     0      3
;!                                             69 BANK1      3     3      0
;!                           _BlinkLED
;!                          _DB_printf
;!                     _ES_EnQueueLIFO
;!                   _ES_PostToService
;!                    _ES_RecallEvents
;!                 _ES_Timer_InitTimer
;!                   _Start_PushToTalk
;!                    _Stop_PushToTalk
;!                               _puts
;! ---------------------------------------------------------------------------------
;! (1) _puts                                                 5     3      2    2150
;!                                             11 BANK0      5     3      2
;!                              _fputc
;!                              _fputs
;! ---------------------------------------------------------------------------------
;! (2) _fputs                                                6     3      3     983
;!                                              5 BANK0      6     3      3
;!                              _fputc
;! ---------------------------------------------------------------------------------
;! (3) _fputc                                                5     2      3     639
;!                                              0 BANK0      5     2      3
;!                              _putch
;! ---------------------------------------------------------------------------------
;! (3) _Stop_PushToTalk                                      0     0      0       0
;! ---------------------------------------------------------------------------------
;! (3) _Start_PushToTalk                                     0     0      0       0
;! ---------------------------------------------------------------------------------
;! (3) _ES_Timer_InitTimer                                   5     3      2     366
;!                                              1 COMMON     1     1      0
;!                                              0 BANK0      4     2      2
;! ---------------------------------------------------------------------------------
;! (3) _ES_RecallEvents                                      6     5      1    5633
;!                                             19 BANK0      6     5      1
;!                         _ES_DeQueue
;!               _ES_PostToServiceLIFO
;! ---------------------------------------------------------------------------------
;! (4) _ES_PostToServiceLIFO                                 5     2      3    2532
;!                                              1 COMMON     1     1      0
;!                                             15 BANK0      4     1      3
;!                     _ES_EnQueueLIFO
;! ---------------------------------------------------------------------------------
;! (5) _ES_EnQueueLIFO                                       9     4      5    2318
;!                                              6 BANK0      9     4      5
;!                             ___wmul
;! ---------------------------------------------------------------------------------
;! (4) _ES_DeQueue                                           9     6      3    2770
;!                                              6 BANK0      9     6      3
;!                            ___lbmod
;!                             ___wmul
;! ---------------------------------------------------------------------------------
;! (6) ___wmul                                               6     2      4    1714
;!                                              0 BANK0      6     2      4
;! ---------------------------------------------------------------------------------
;! (5) ___lbmod                                              5     4      1     322
;!                                              1 COMMON     1     0      1
;!                                              0 BANK0      4     4      0
;! ---------------------------------------------------------------------------------
;! (2) _DB_printf                                           76    70      6    5024
;!                                             15 BANK0      7     1      6
;!                                              0 BANK1     69    69      0
;!                              _putch
;!                              _uitoa
;! ---------------------------------------------------------------------------------
;! (3) _uitoa                                                8     4      4    2381
;!                                              7 BANK0      8     4      4
;!                            ___lwdiv
;!                            ___lwmod
;! ---------------------------------------------------------------------------------
;! (4) ___lwmod                                              5     1      4     412
;!                                              1 COMMON     1     1      0
;!                                              0 BANK0      4     0      4
;! ---------------------------------------------------------------------------------
;! (4) ___lwdiv                                              7     3      4     751
;!                                              0 BANK0      7     3      4
;! ---------------------------------------------------------------------------------
;! (4) _putch                                                1     1      0      31
;!                                              1 COMMON     1     1      0
;! ---------------------------------------------------------------------------------
;! (3) _BlinkLED                                             0     0      0       0
;! ---------------------------------------------------------------------------------
;! (2) _ES_CheckUserEvents                                   1     1      0    1378
;!                                             12 BANK0      1     1      0
;!                    _Check4Keystroke *
;! ---------------------------------------------------------------------------------
;! (3) _Check4Keystroke                                      3     3      0    1248
;!                                              9 BANK0      3     3      0
;!                         _ES_PostAll
;!                            _getchar
;! ---------------------------------------------------------------------------------
;! (4) _getchar                                              2     0      2       0
;!                                              0 BANK0      2     0      2
;! ---------------------------------------------------------------------------------
;! (4) _ES_PostAll                                           4     1      3    1177
;!                                              1 COMMON     1     1      0
;!                                              6 BANK0      3     0      3
;!                     _ES_EnQueueFIFO
;! ---------------------------------------------------------------------------------
;! (1) _ES_Initialize                                        5     3      2    2647
;!                                              1 COMMON     1     1      0
;!                                             10 BANK0      4     2      2
;!                       _ES_InitQueue
;!                      _ES_Timer_Init
;!            _InitTestHarnessService0 *
;! ---------------------------------------------------------------------------------
;! (2) _InitTestHarnessService0                              4     3      1    1574
;!                                              6 BANK0      4     3      1
;!                       _ES_InitQueue
;!                   _ES_PostToService
;!                            _InitLED
;! ---------------------------------------------------------------------------------
;! (3) _InitLED                                              0     0      0       0
;! ---------------------------------------------------------------------------------
;! (5) _ES_PostToService                                     4     1      3    1019
;!                     _ES_EnQueueFIFO
;! ---------------------------------------------------------------------------------
;! (6) _ES_EnQueueFIFO                                       5     2      3     895
;!                            ___awmod
;! ---------------------------------------------------------------------------------
;! (7) ___awmod                                              6     2      4     613
;!                                              0 BANK0      6     2      4
;! ---------------------------------------------------------------------------------
;! (3) _ES_InitQueue                                         5     2      3     425
;!                                              0 BANK0      5     2      3
;! ---------------------------------------------------------------------------------
;! (2) _ES_Timer_Init                                        2     0      2     273
;!                                              2 BANK0      2     0      2
;!                     __HW_Timer_Init
;! ---------------------------------------------------------------------------------
;! (3) __HW_Timer_Init                                       2     0      2     152
;!                                              0 BANK0      2     0      2
;! ---------------------------------------------------------------------------------
;! (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;! ---------------------------------------------------------------------------------
;! (8) _myIsr                                                1     1      0       0
;!                                              0 COMMON     1     1      0
;!              __HW_SysTickIntHandler
;! ---------------------------------------------------------------------------------
;! (9) __HW_SysTickIntHandler                                0     0      0       0
;! ---------------------------------------------------------------------------------
;! Estimated maximum stack depth 9
;! ---------------------------------------------------------------------------------
;!
;! Call Graph Graphs:
;!
;! _main (ROOT)
;!   _DB_printf
;!     _putch
;!     _uitoa
;!       ___lwdiv
;!       ___lwmod
;!   _ES_Initialize
;!     _ES_InitQueue
;!     _ES_Timer_Init
;!       __HW_Timer_Init
;!     _InitTestHarnessService0 *
;!       _ES_InitQueue
;!       _ES_PostToService
;!         _ES_EnQueueFIFO
;!           ___awmod
;!       _InitLED
;!   _ES_Run
;!     _ES_CheckUserEvents
;!       _Check4Keystroke *
;!         _ES_PostAll
;!           _ES_EnQueueFIFO
;!             ___awmod
;!         _getchar
;!     _ES_DeQueue
;!       ___lbmod
;!       ___wmul
;!     _ES_GetMSBitSet
;!     _RunTestHarnessService0 *
;!       _BlinkLED
;!       _DB_printf
;!         _putch
;!         _uitoa
;!           ___lwdiv
;!           ___lwmod
;!       _ES_EnQueueLIFO
;!         ___wmul
;!       _ES_PostToService
;!         _ES_EnQueueFIFO
;!           ___awmod
;!       _ES_RecallEvents
;!         _ES_DeQueue
;!           ___lbmod
;!           ___wmul
;!         _ES_PostToServiceLIFO
;!           _ES_EnQueueLIFO
;!             ___wmul
;!       _ES_Timer_InitTimer
;!       _Start_PushToTalk
;!       _Stop_PushToTalk
;!       _puts
;!         _fputc
;!           _putch
;!         _fputs
;!           _fputc
;!             _putch
;!     __HW_Process_Pending_Ints *
;!       _ES_Timer_Tick_Resp
;!         Absolute function(Fake) *
;!         _ES_GetMSBitSet *
;!         _PostTestHarnessService0 *
;!           _ES_PostToService
;!             _ES_EnQueueFIFO
;!               ___awmod
;!   _InitPWM
;!     _DB_printf
;!       _putch
;!       _uitoa
;!         ___lwdiv
;!         ___lwmod
;!   __HW_PIC15356Init
;!     __HW_ConsoleInit
;!   _puts
;!     _fputc
;!       _putch
;!     _fputs
;!       _fputc
;!         _putch
;!
;! _myIsr (ROOT)
;!   __HW_SysTickIntHandler
;!

;! Address spaces:

;!Name               Size   Autos  Total    Cost      Usage
;!BIGRAM             7F0      0       0       0        0.0%
;!NULL                 0      0       0       0        0.0%
;!CODE                 0      0       0       0        0.0%
;!BITCOMMON            4      0       0       1        0.0%
;!BITSFR0              0      0       0       1        0.0%
;!SFR0                 0      0       0       1        0.0%
;!COMMON               4      2       3       2       75.0%
;!BITSFR1              0      0       0       2        0.0%
;!SFR1                 0      0       0       2        0.0%
;!BITSFR2              0      0       0       3        0.0%
;!SFR2                 0      0       0       3        0.0%
;!STACK                0      0       3       3        0.0%
;!BITBANK0            50      0       0       4        0.0%
;!BITSFR3              0      0       0       4        0.0%
;!SFR3                 0      0       0       4        0.0%
;!BANK0               50     1E      50       5      100.0%
;!BITSFR4              0      0       0       5        0.0%
;!SFR4                 0      0       0       5        0.0%
;!BITBANK1            50      0       0       6        0.0%
;!BITSFR5              0      0       0       6        0.0%
;!SFR5                 0      0       0       6        0.0%
;!BANK1               50     49      49       7       91.3%
;!BITSFR6              0      0       0       7        0.0%
;!SFR6                 0      0       0       7        0.0%
;!BITSFR7              0      0       0       8        0.0%
;!SFR7                 0      0       0       8        0.0%
;!ABS                  0      0      CE       8        0.0%
;!BITBANK2            50      0       0       9        0.0%
;!BITSFR8              0      0       0       9        0.0%
;!SFR8                 0      0       0       9        0.0%
;!BANK2               50      0      32      10       62.5%
;!BITSFR9              0      0       0      10        0.0%
;!SFR9                 0      0       0      10        0.0%
;!BITBANK3            50      0       0      11        0.0%
;!BITSFR10             0      0       0      11        0.0%
;!SFR10                0      0       0      11        0.0%
;!BANK3               50      0       0      12        0.0%
;!BITSFR11             0      0       0      12        0.0%
;!SFR11                0      0       0      12        0.0%
;!BITBANK4            50      0       0      13        0.0%
;!BITSFR12             0      0       0      13        0.0%
;!SFR12                0      0       0      13        0.0%
;!BANK4               50      0       0      14        0.0%
;!BITSFR13             0      0       0      14        0.0%
;!SFR13                0      0       0      14        0.0%
;!BITBANK5            50      0       0      15        0.0%
;!BITSFR14             0      0       0      15        0.0%
;!SFR14                0      0       0      15        0.0%
;!BANK5               50      0       0      16        0.0%
;!BITSFR15             0      0       0      16        0.0%
;!SFR15                0      0       0      16        0.0%
;!BITBANK6            50      0       0      17        0.0%
;!BITSFR16             0      0       0      17        0.0%
;!SFR16                0      0       0      17        0.0%
;!BANK6               50      0       0      18        0.0%
;!BITSFR17             0      0       0      18        0.0%
;!SFR17                0      0       0      18        0.0%
;!BITBANK7            50      0       0      19        0.0%
;!BITSFR18             0      0       0      19        0.0%
;!SFR18                0      0       0      19        0.0%
;!BANK7               50      0       0      20        0.0%
;!BITSFR19             0      0       0      20        0.0%
;!SFR19                0      0       0      20        0.0%
;!BITBANK8            50      0       0      21        0.0%
;!BITSFR20             0      0       0      21        0.0%
;!SFR20                0      0       0      21        0.0%
;!BANK8               50      0       0      22        0.0%
;!BITSFR21             0      0       0      22        0.0%
;!SFR21                0      0       0      22        0.0%
;!BITBANK9            50      0       0      23        0.0%
;!BITSFR22             0      0       0      23        0.0%
;!SFR22                0      0       0      23        0.0%
;!BANK9               50      0       0      24        0.0%
;!BITSFR23             0      0       0      24        0.0%
;!SFR23                0      0       0      24        0.0%
;!BITBANK10           50      0       0      25        0.0%
;!BITSFR24             0      0       0      25        0.0%
;!SFR24                0      0       0      25        0.0%
;!BANK10              50      0       0      26        0.0%
;!BITSFR25             0      0       0      26        0.0%
;!SFR25                0      0       0      26        0.0%
;!BITBANK11           50      0       0      27        0.0%
;!BITSFR26             0      0       0      27        0.0%
;!SFR26                0      0       0      27        0.0%
;!BANK11              50      0       0      28        0.0%
;!BITSFR27             0      0       0      28        0.0%
;!SFR27                0      0       0      28        0.0%
;!BITBANK12           50      0       0      29        0.0%
;!BITSFR28             0      0       0      29        0.0%
;!SFR28                0      0       0      29        0.0%
;!BANK12              50      0       0      30        0.0%
;!BITSFR29             0      0       0      30        0.0%
;!SFR29                0      0       0      30        0.0%
;!BITBANK13           50      0       0      31        0.0%
;!BITSFR30             0      0       0      31        0.0%
;!SFR30                0      0       0      31        0.0%
;!BANK13              50      0       0      32        0.0%
;!BITSFR31             0      0       0      32        0.0%
;!SFR31                0      0       0      32        0.0%
;!BITBANK14           50      0       0      33        0.0%
;!BITSFR32             0      0       0      33        0.0%
;!SFR32                0      0       0      33        0.0%
;!BANK14              50      0       0      34        0.0%
;!BITSFR33             0      0       0      34        0.0%
;!SFR33                0      0       0      34        0.0%
;!BITBANK15           50      0       0      35        0.0%
;!BITSFR34             0      0       0      35        0.0%
;!SFR34                0      0       0      35        0.0%
;!BANK15              50      0       0      36        0.0%
;!BITSFR35             0      0       0      36        0.0%
;!SFR35                0      0       0      36        0.0%
;!BITBANK16           50      0       0      37        0.0%
;!BITSFR36             0      0       0      37        0.0%
;!SFR36                0      0       0      37        0.0%
;!BANK16              50      0       0      38        0.0%
;!BITSFR37             0      0       0      38        0.0%
;!SFR37                0      0       0      38        0.0%
;!BITBANK17           50      0       0      39        0.0%
;!BITSFR38             0      0       0      39        0.0%
;!SFR38                0      0       0      39        0.0%
;!BANK17              50      0       0      40        0.0%
;!BITSFR39             0      0       0      40        0.0%
;!SFR39                0      0       0      40        0.0%
;!BITBANK18           50      0       0      41        0.0%
;!BITSFR40             0      0       0      41        0.0%
;!SFR40                0      0       0      41        0.0%
;!BANK18              50      0       0      42        0.0%
;!BITSFR41             0      0       0      42        0.0%
;!SFR41                0      0       0      42        0.0%
;!BITBANK19           50      0       0      43        0.0%
;!BITSFR42             0      0       0      43        0.0%
;!SFR42                0      0       0      43        0.0%
;!BANK19              50      0       0      44        0.0%
;!BITSFR43             0      0       0      44        0.0%
;!SFR43                0      0       0      44        0.0%
;!BITBANK20           50      0       0      45        0.0%
;!BITSFR44             0      0       0      45        0.0%
;!SFR44                0      0       0      45        0.0%
;!BANK20              50      0       0      46        0.0%
;!BITSFR45             0      0       0      46        0.0%
;!SFR45                0      0       0      46        0.0%
;!BITBANK21           50      0       0      47        0.0%
;!BITSFR46             0      0       0      47        0.0%
;!SFR46                0      0       0      47        0.0%
;!BANK21              50      0       0      48        0.0%
;!BITSFR47             0      0       0      48        0.0%
;!SFR47                0      0       0      48        0.0%
;!BITBANK22           50      0       0      49        0.0%
;!BITSFR48             0      0       0      49        0.0%
;!SFR48                0      0       0      49        0.0%
;!BANK22              50      0       0      50        0.0%
;!BITSFR49             0      0       0      50        0.0%
;!SFR49                0      0       0      50        0.0%
;!BITBANK23           50      0       0      51        0.0%
;!BITSFR50             0      0       0      51        0.0%
;!SFR50                0      0       0      51        0.0%
;!BANK23              50      0       0      52        0.0%
;!BITSFR51             0      0       0      52        0.0%
;!SFR51                0      0       0      52        0.0%
;!BITBANK24           50      0       0      53        0.0%
;!BITSFR52             0      0       0      53        0.0%
;!SFR52                0      0       0      53        0.0%
;!BANK24              50      0       0      54        0.0%
;!BITSFR53             0      0       0      54        0.0%
;!SFR53                0      0       0      54        0.0%
;!BITBANK25           20      0       0      55        0.0%
;!BITSFR54             0      0       0      55        0.0%
;!SFR54                0      0       0      55        0.0%
;!BANK25              20      0       0      56        0.0%
;!BITSFR55             0      0       0      56        0.0%
;!SFR55                0      0       0      56        0.0%
;!BITSFR56             0      0       0      57        0.0%
;!SFR56                0      0       0      57        0.0%
;!DATA                 0      0      D1      57        0.0%
;!BITSFR57             0      0       0      58        0.0%
;!SFR57                0      0       0      58        0.0%
;!BITSFR58             0      0       0      59        0.0%
;!SFR58                0      0       0      59        0.0%
;!BITSFR59             0      0       0      60        0.0%
;!SFR59                0      0       0      60        0.0%
;!BITSFR60             0      0       0      61        0.0%
;!SFR60                0      0       0      61        0.0%
;!BITSFR61             0      0       0      62        0.0%
;!SFR61                0      0       0      62        0.0%
;!BITSFR62             0      0       0      63        0.0%
;!SFR62                0      0       0      63        0.0%
;!BITSFR63             0      0       0      64        0.0%
;!SFR63                0      0       0      64        0.0%

	global	_main

;; *************** function _main *****************
;; Defined at:
;;		line 27 in file "ProjectSource/main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ErrorType       1   72[BANK1 ] enum E7707
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : B3F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels required when called:    9
;; This function calls:
;;		_DB_printf
;;		_ES_Initialize
;;		_ES_Run
;;		_InitPWM
;;		__HW_PIC15356Init
;;		_puts
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext,global,class=CODE,delta=2,merge=1,split=1,group=0
	file	"ProjectSource/main.c"
	line	27
global __pmaintext
__pmaintext:	;psect for function _main
psect	maintext
	file	"ProjectSource/main.c"
	line	27
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
;incstack = 0
	opt	callstack 7
; Regs used in _main: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+8-btemp+11+pclath+cstack]
	line	31
	
l3687:	
;ProjectSource/main.c: 29:   ES_Return_t ErrorType;;ProjectSource/main.c: 31:   _HW_PIC15356Init();
	fcall	__HW_PIC15356Init
	line	32
	
l3689:	
;ProjectSource/main.c: 32:   DB_printf("\x1b[2J");
	movlw	low(((STR_1)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_1)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	36
	
l3691:	
;ProjectSource/main.c: 36:   puts("\rStarting Test Harness for \r");
	movlw	low(((STR_2)|8000h))
	movlb 0	; select bank0
	movwf	(puts@s)
	movlw	high(((STR_2)|8000h))
	movwf	((puts@s))+1
	fcall	_puts
	line	37
	
l3693:	
;ProjectSource/main.c: 37:   DB_printf( "the 2nd Generation Events & Services Framework V2.4\r\n");
	movlw	low(((STR_3)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_3)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	38
	
l3695:	
;ProjectSource/main.c: 38:   DB_printf( "%s %s\n", "00:29:11", "May 11 2020");
	movlw	low(((STR_4)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_4)|8000h))
	movwf	((DB_printf@Format))+1
	movlw	low(((STR_5)|8000h))
	movwf	(0+low(?_DB_printf|((0x0)<<8)+02h))
	movlw	high(((STR_5)|8000h))
	movwf	((0+low(?_DB_printf|((0x0)<<8)+02h)))+1
	movlw	low(((STR_6)|8000h))
	movwf	(0+low(?_DB_printf|((0x0)<<8)+04h))
	movlw	high(((STR_6)|8000h))
	movwf	((0+low(?_DB_printf|((0x0)<<8)+04h)))+1
	fcall	_DB_printf
	line	39
	
l3697:	
;ProjectSource/main.c: 39:   DB_printf( "\n\r\n");
	movlw	low(((STR_7)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_7)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	40
	
l3699:	
;ProjectSource/main.c: 40:   DB_printf( "Press any key to post key-stroke events to Service 0\n\r");
	movlw	low(((STR_8)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_8)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	41
	
l3701:	
;ProjectSource/main.c: 41:   DB_printf( "Press 'd' to test event deferral \n\r");
	movlw	low(((STR_9)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_9)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	42
	
l3703:	
;ProjectSource/main.c: 42:   DB_printf( "Press 'r' to test event recall \n\r");
	movlw	low(((STR_10)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_10)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	45
	
l3705:	
;ProjectSource/main.c: 45:   InitPWM();
	fcall	_InitPWM
	line	48
	
l3707:	
;ProjectSource/main.c: 48:   ErrorType = ES_Initialize(ES_Timer_RATE_10mS);
	movlw	01Bh
	movlb 0	; select bank0
	movwf	(ES_Initialize@NewRate)
	movlw	0Dh
	movwf	((ES_Initialize@NewRate))+1
	fcall	_ES_Initialize
	movwf	btemp+11
	movf	btemp+11,w
	movlb 1	; select bank1
	movwf	(main@ErrorType)^080h
	line	49
	
l3709:	
;ProjectSource/main.c: 49:   if (ErrorType == Success)
	movf	((main@ErrorType)^080h),w
	btfss	status,2
	goto	u3001
	goto	u3000
u3001:
	goto	l3723
u3000:
	line	51
	
l3711:	
;ProjectSource/main.c: 50:   {;ProjectSource/main.c: 51:     ErrorType = ES_Run();
	fcall	_ES_Run
	movwf	btemp+11
	movf	btemp+11,w
	movlb 1	; select bank1
	movwf	(main@ErrorType)^080h
	goto	l3723
	line	58
	
l3713:	
;ProjectSource/main.c: 57:     {;ProjectSource/main.c: 58:       DB_printf("Failed on attempt to Post\n");
	movlw	low(((STR_11)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_11)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	60
;ProjectSource/main.c: 59:     };ProjectSource/main.c: 60:     break;
	goto	l296
	line	63
	
l3715:	
;ProjectSource/main.c: 62:     {;ProjectSource/main.c: 63:       DB_printf("Failed on NULL pointer\n");
	movlw	low(((STR_12)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_12)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	65
;ProjectSource/main.c: 64:     };ProjectSource/main.c: 65:     break;
	goto	l296
	line	68
	
l3717:	
;ProjectSource/main.c: 67:     {;ProjectSource/main.c: 68:       DB_printf("Failed Initialization\n");
	movlw	low(((STR_13)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_13)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	70
;ProjectSource/main.c: 69:     };ProjectSource/main.c: 70:     break;
	goto	l296
	line	73
	
l3719:	
;ProjectSource/main.c: 72:     {;ProjectSource/main.c: 73:       DB_printf("Other Failure\n");
	movlw	low(((STR_14)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_14)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	75
;ProjectSource/main.c: 74:     };ProjectSource/main.c: 75:     break;
	goto	l296
	line	76
	
l3723:	
	movf	(main@ErrorType)^080h,w
	movwf	btemp+10
	clrf	btemp+11
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "simple"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte            8     6 (fixed)
; jumptable            260     6 (fixed)
;	Chosen strategy is simple_byte

	movf 1+wtemp5,w
	opt asmopt_push
	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	l3817
	goto	l3719
	opt asmopt_pop
	
l3817:	
; Switch size 1, requested type "simple"
; Number of cases is 3, Range of values is 1 to 5
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           10     6 (average)
; direct_byte           19     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf 0+wtemp5,w
	opt asmopt_push
	opt asmopt_off
	xorlw	1^0	; case 1
	skipnz
	goto	l3713
	xorlw	3^1	; case 3
	skipnz
	goto	l3715
	xorlw	5^3	; case 5
	skipnz
	goto	l3717
	goto	l3719
	opt asmopt_pop

	
l296:	
	goto	l296
	global	start
	ljmp	start
	opt callstack 0
	line	81
GLOBAL	__end_of_main
	__end_of_main:
	signat	_main,89
	global	__HW_PIC15356Init

;; *************** function __HW_PIC15356Init *****************
;; Defined at:
;;		line 121 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		__HW_ConsoleInit
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text1,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Port.c"
	line	121
global __ptext1
__ptext1:	;psect for function __HW_PIC15356Init
psect	text1
	file	"FrameworkSource/ES_Port.c"
	line	121
	global	__size_of__HW_PIC15356Init
	__size_of__HW_PIC15356Init	equ	__end_of__HW_PIC15356Init-__HW_PIC15356Init
	
__HW_PIC15356Init:	
;incstack = 0
	opt	callstack 12
; Regs used in __HW_PIC15356Init: [wreg+status,2+status,0+btemp+11+pclath+cstack]
	line	123
	
l3607:	
;FrameworkSource/ES_Port.c: 123:   _HW_ConsoleInit();
	fcall	__HW_ConsoleInit
	line	131
	
l49:	
	return
	opt callstack 0
GLOBAL	__end_of__HW_PIC15356Init
	__end_of__HW_PIC15356Init:
	signat	__HW_PIC15356Init,89
	global	__HW_ConsoleInit

;; *************** function __HW_ConsoleInit *****************
;; Defined at:
;;		line 262 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		__HW_PIC15356Init
;; This function uses a non-reentrant model
;;
psect	text2,local,class=CODE,delta=2,merge=1,group=0
	line	262
global __ptext2
__ptext2:	;psect for function __HW_ConsoleInit
psect	text2
	file	"FrameworkSource/ES_Port.c"
	line	262
	global	__size_of__HW_ConsoleInit
	__size_of__HW_ConsoleInit	equ	__end_of__HW_ConsoleInit-__HW_ConsoleInit
	
__HW_ConsoleInit:	
;incstack = 0
	opt	callstack 12
; Regs used in __HW_ConsoleInit: [wreg+status,2+status,0+btemp+11]
	line	265
	
l3479:	
;FrameworkSource/ES_Port.c: 265:   ANSELC &= (~0x00000040 & ~0x00000080);
	movlw	low(03Fh)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 62	; select bank62
	andwf	(8014)^01F00h,f	;volatile
	line	266
	
l3481:	
;FrameworkSource/ES_Port.c: 266:   RC6PPS = 0x0f;
	movlw	low(0Fh)
	movwf	(7974)^01F00h	;volatile
	line	272
	
l3483:	
;FrameworkSource/ES_Port.c: 272:   SP1BRGL = 51;
	movlw	low(033h)
	movlb 2	; select bank2
	movwf	(283)^0100h	;volatile
	line	274
	
l3485:	
;FrameworkSource/ES_Port.c: 274:   TX1STAbits.SYNC = 0;
	bcf	(286)^0100h,4	;volatile
	line	275
	
l3487:	
;FrameworkSource/ES_Port.c: 275:   RC1STAbits.SPEN = 1;
	bsf	(285)^0100h,7	;volatile
	line	281
	
l3489:	
;FrameworkSource/ES_Port.c: 281:   TX1STAbits.TXEN = 1;
	bsf	(286)^0100h,5	;volatile
	line	283
	
l3491:	
;FrameworkSource/ES_Port.c: 283:   RC1STAbits.CREN = 1;
	bsf	(285)^0100h,4	;volatile
	line	289
	
l67:	
	return
	opt callstack 0
GLOBAL	__end_of__HW_ConsoleInit
	__end_of__HW_ConsoleInit:
	signat	__HW_ConsoleInit,89
	global	_InitPWM

;; *************** function _InitPWM *****************
;; Defined at:
;;		line 69 in file "ProjectSource/ServoControl.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_DB_printf
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text3,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/ServoControl.c"
	line	69
global __ptext3
__ptext3:	;psect for function _InitPWM
psect	text3
	file	"ProjectSource/ServoControl.c"
	line	69
	global	__size_of_InitPWM
	__size_of_InitPWM	equ	__end_of_InitPWM-_InitPWM
	
_InitPWM:	
;incstack = 0
	opt	callstack 10
; Regs used in _InitPWM: [wreg-fsr0h+status,2+status,0+btemp+10+btemp+11+pclath+cstack]
	line	72
	
l3659:	
;ProjectSource/ServoControl.c: 72:     PIR4bits.TMR2IF = 0;
	movlb 14	; select bank14
	bcf	(1808)^0700h,1	;volatile
	line	74
	
l3661:	
;ProjectSource/ServoControl.c: 74:     T2CLKCON = 0b00000110;
	movlw	low(06h)
	movlb 5	; select bank5
	movwf	(656)^0280h	;volatile
	line	76
;ProjectSource/ServoControl.c: 76:     T2CON = 0b00100000;
	movlw	low(020h)
	movwf	(654)^0280h	;volatile
	line	78
;ProjectSource/ServoControl.c: 78:     T2PR = 155;
	movlw	low(09Bh)
	movwf	(653)^0280h	;volatile
	line	80
	
l3663:	
;ProjectSource/ServoControl.c: 80:     T2CONbits.ON = 1;
	bsf	(654)^0280h,7	;volatile
	line	83
;ProjectSource/ServoControl.c: 83:     RC1PPS = 0x0A;
	movlw	low(0Ah)
	movlb 62	; select bank62
	movwf	(7969)^01F00h	;volatile
	line	84
	
l3665:	
;ProjectSource/ServoControl.c: 84:     TRISCbits.TRISC1 = 1;
	movlb 0	; select bank0
	bsf	(20),1	;volatile
	line	85
	
l3667:	
;ProjectSource/ServoControl.c: 85:     ANSELCbits.ANSC1 = 0;
	movlb 62	; select bank62
	bcf	(8014)^01F00h,1	;volatile
	line	87
	
l3669:	
;ProjectSource/ServoControl.c: 87:     CCP2CONbits.CCP2MODE0 = 1;
	movlb 6	; select bank6
	bsf	(786)^0300h,0	;volatile
	line	88
	
l3671:	
;ProjectSource/ServoControl.c: 88:     CCP2CONbits.CCP2MODE1 = 1;
	bsf	(786)^0300h,1	;volatile
	line	89
	
l3673:	
;ProjectSource/ServoControl.c: 89:     CCP2CONbits.CCP2MODE2 = 1;
	bsf	(786)^0300h,2	;volatile
	line	90
	
l3675:	
;ProjectSource/ServoControl.c: 90:     CCP2CONbits.CCP2MODE3 = 1;
	bsf	(786)^0300h,3	;volatile
	line	91
	
l3677:	
;ProjectSource/ServoControl.c: 91:     CCP2CONbits.CCP2EN = 1;
	bsf	(786)^0300h,7	;volatile
	line	93
;ProjectSource/ServoControl.c: 93:     CCPR2H = 8;
	movlw	low(08h)
	movwf	(785)^0300h	;volatile
	line	94
	
l3679:	
;ProjectSource/ServoControl.c: 94:     CCP2CONbits.CCP2FMT = 1;
	bsf	(786)^0300h,4	;volatile
	line	97
	
l3681:	
;ProjectSource/ServoControl.c: 97:     T2TMR = 0;
	movlb 5	; select bank5
	clrf	(652)^0280h	;volatile
	line	99
	
l3683:	
;ProjectSource/ServoControl.c: 99:     TRISCbits.TRISC1 = 0;
	movlb 0	; select bank0
	bcf	(20),1	;volatile
	line	100
	
l3685:	
;ProjectSource/ServoControl.c: 100:     DB_printf("Init PWM\r\n");
	movlw	low(((STR_24)|8000h))
	movwf	(DB_printf@Format)
	movlw	high(((STR_24)|8000h))
	movwf	((DB_printf@Format))+1
	fcall	_DB_printf
	line	101
	
l468:	
	return
	opt callstack 0
GLOBAL	__end_of_InitPWM
	__end_of_InitPWM:
	signat	_InitPWM,89
	global	_ES_Run

;; *************** function _ES_Run *****************
;; Defined at:
;;		line 300 in file "FrameworkSource/ES_Framework.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  HighestPrior    1   29[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      enum E7707
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    8
;; This function calls:
;;		_ES_CheckUserEvents
;;		_ES_DeQueue
;;		_ES_GetMSBitSet
;;		_RunTestHarnessService0
;;		__HW_Process_Pending_Ints
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text4,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Framework.c"
	line	300
global __ptext4
__ptext4:	;psect for function _ES_Run
psect	text4
	file	"FrameworkSource/ES_Framework.c"
	line	300
	global	__size_of_ES_Run
	__size_of_ES_Run	equ	__end_of_ES_Run-_ES_Run
	
_ES_Run:	
;incstack = 0
	opt	callstack 7
; Regs used in _ES_Run: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+8-btemp+11+pclath+cstack]
	line	306
;FrameworkSource/ES_Framework.c: 303:   uint8_t HighestPrior;;FrameworkSource/ES_Framework.c: 304:   static ES_Event_t ThisEvent;;FrameworkSource/ES_Framework.c: 306:   while (1)
	
l217:	
	line	310
;FrameworkSource/ES_Framework.c: 307:   {;FrameworkSource/ES_Framework.c: 310:     while ((_HW_Process_Pending_Ints()) && (Ready != 0))
	goto	l3653
	line	312
	
l3641:	
;FrameworkSource/ES_Framework.c: 311:     {;FrameworkSource/ES_Framework.c: 312:       HighestPrior = ES_GetMSBitSet(Ready);
	movf	(_Ready+1),w
	movwf	(ES_GetMSBitSet@Val2Check+1)
	movf	(_Ready),w
	movwf	(ES_GetMSBitSet@Val2Check)
	fcall	_ES_GetMSBitSet
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(ES_Run@HighestPrior)
	line	313
	
l3643:	
;FrameworkSource/ES_Framework.c: 313:       if (ES_DeQueue(EventQueues[HighestPrior].pMem, &ThisEvent) == 0)
	movf	(ES_Run@HighestPrior),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_EventQueues)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_EventQueues)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
		moviw	[0]fsr0
	movwf	(ES_DeQueue@pBlock)
	movlw	0x1
	movwf	(ES_DeQueue@pBlock+1)

	movlw	(low(ES_Run@ThisEvent|((0x0)<<8)))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_DeQueue@pReturnEvent)
	fcall	_ES_DeQueue
	xorlw	low(0)&0ffh
	skipz
	goto	u2961
	goto	u2960
u2961:
	goto	l3647
u2960:
	line	315
	
l3645:	
;FrameworkSource/ES_Framework.c: 314:       {;FrameworkSource/ES_Framework.c: 315:         Ready &= ~BitNum2SetMask[HighestPrior];
	movlb 0	; select bank0
	movf	(ES_Run@HighestPrior),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	comf	btemp+10,f
	comf	btemp+11,f
	movf	0+wtemp5,w
	andwf	(_Ready),f
	movf	1+wtemp5,w
	andwf	(_Ready+1),f
	line	320
	
l3647:	
;FrameworkSource/ES_Framework.c: 320:       if (ServDescList[HighestPrior].RunFunc(ThisEvent).EventType !=
	movlb 0	; select bank0
	movf	(ES_Run@ThisEvent),w
	movwf	(RunTestHarnessService0@ThisEvent)
	movf	(ES_Run@ThisEvent+1),w
	movwf	(RunTestHarnessService0@ThisEvent+1)
	movf	(ES_Run@ThisEvent+2),w
	movwf	(RunTestHarnessService0@ThisEvent+2)
	movf	(ES_Run@HighestPrior),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_ServDescList+02h)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_ServDescList+02h)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[1]fsr0
	movwf	pclath
	moviw	[0]fsr0
	callw
	pagesel	$
	movlb 0	; select bank0
	movf	((0+(?_RunTestHarnessService0))),w
	btfsc	status,2
	goto	u2971
	goto	u2970
u2971:
	goto	l3653
u2970:
	line	323
	
l3649:	
;FrameworkSource/ES_Framework.c: 322:       {;FrameworkSource/ES_Framework.c: 323:         return FailedRun;
	movlw	low(02h)
	goto	l222
	line	310
	
l3653:	
;FrameworkSource/ES_Framework.c: 310:     while ((_HW_Process_Pending_Ints()) && (Ready != 0))
	fcall	__HW_Process_Pending_Ints
	xorlw	low(0)&0ffh
	skipnz
	goto	u2981
	goto	u2980
u2981:
	goto	l3657
u2980:
	
l3655:	
	movlb 0	; select bank0
	movf	((_Ready)),w
iorwf	((_Ready+1)),w
	btfss	status,2
	goto	u2991
	goto	u2990
u2991:
	goto	l3641
u2990:
	line	334
	
l3657:	
;FrameworkSource/ES_Framework.c: 334:     ES_CheckUserEvents();
	fcall	_ES_CheckUserEvents
	goto	l217
	line	339
	
l222:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_Run
	__end_of_ES_Run:
	signat	_ES_Run,89
	global	__HW_Process_Pending_Ints

;; *************** function __HW_Process_Pending_Ints *****************
;; Defined at:
;;		line 236 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    7
;; This function calls:
;;		_ES_Timer_Tick_Resp
;; This function is called by:
;;		_ES_Run
;; This function uses a non-reentrant model
;;
psect	text5,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Port.c"
	line	236
global __ptext5
__ptext5:	;psect for function __HW_Process_Pending_Ints
psect	text5
	file	"FrameworkSource/ES_Port.c"
	line	236
	global	__size_of__HW_Process_Pending_Ints
	__size_of__HW_Process_Pending_Ints	equ	__end_of__HW_Process_Pending_Ints-__HW_Process_Pending_Ints
	
__HW_Process_Pending_Ints:	
;incstack = 0
	opt	callstack 7
; Regs used in __HW_Process_Pending_Ints: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	238
	
l3493:	
;FrameworkSource/ES_Port.c: 238:   while (TickCount > 0)
	goto	l3499
	line	241
	
l3495:	
;FrameworkSource/ES_Port.c: 239:   {;FrameworkSource/ES_Port.c: 241:     ES_Timer_Tick_Resp();
	fcall	_ES_Timer_Tick_Resp
	line	242
	
l3497:	
;FrameworkSource/ES_Port.c: 242:     TickCount--;
	movlw	01h
	movlb 0	; select bank0
	subwf	(_TickCount),f	;volatile
	line	238
	
l3499:	
;FrameworkSource/ES_Port.c: 238:   while (TickCount > 0)
	movlb 0	; select bank0
	movf	((_TickCount)),w	;volatile
	btfss	status,2
	goto	u2821
	goto	u2820
u2821:
	goto	l3495
u2820:
	line	244
	
l3501:	
;FrameworkSource/ES_Port.c: 244:   return 1;
	movlw	low(01h)
	line	245
	
l64:	
	return
	opt callstack 0
GLOBAL	__end_of__HW_Process_Pending_Ints
	__end_of__HW_Process_Pending_Ints:
	signat	__HW_Process_Pending_Ints,89
	global	_ES_Timer_Tick_Resp

;; *************** function _ES_Timer_Tick_Resp *****************
;; Defined at:
;;		line 292 in file "FrameworkSource/ES_Timers.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		Absolute function
;;		_ES_GetMSBitSet
;;		_PostTestHarnessService0
;; This function is called by:
;;		__HW_Process_Pending_Ints
;; This function uses a non-reentrant model
;;
psect	text6,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Timers.c"
	line	292
global __ptext6
__ptext6:	;psect for function _ES_Timer_Tick_Resp
psect	text6
	file	"FrameworkSource/ES_Timers.c"
	line	292
	global	__size_of_ES_Timer_Tick_Resp
	__size_of_ES_Timer_Tick_Resp	equ	__end_of_ES_Timer_Tick_Resp-_ES_Timer_Tick_Resp
	
_ES_Timer_Tick_Resp:	
;incstack = 0
	opt	callstack 7
; Regs used in _ES_Timer_Tick_Resp: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	298
	
l3289:	
;FrameworkSource/ES_Timers.c: 294:   static Tflag_t NeedsProcessing;;FrameworkSource/ES_Timers.c: 295:   static uint8_t NextTimer2Process;;FrameworkSource/ES_Timers.c: 296:   static ES_Event_t NewEvent;;FrameworkSource/ES_Timers.c: 298:   if (TMR_ActiveFlags != 0)
	movlb 0	; select bank0
	movf	((_TMR_ActiveFlags)),w
iorwf	((_TMR_ActiveFlags+1)),w
	btfsc	status,2
	goto	u2641
	goto	u2640
u2641:
	goto	l141
u2640:
	line	301
	
l3291:	
;FrameworkSource/ES_Timers.c: 299:   {;FrameworkSource/ES_Timers.c: 301:     NeedsProcessing = TMR_ActiveFlags;
	movf	(_TMR_ActiveFlags+1),w
	movwf	(ES_Timer_Tick_Resp@NeedsProcessing+1)
	movf	(_TMR_ActiveFlags),w
	movwf	(ES_Timer_Tick_Resp@NeedsProcessing)
	line	305
	
l3293:	
;FrameworkSource/ES_Timers.c: 303:     {;FrameworkSource/ES_Timers.c: 305:       NextTimer2Process = ES_GetMSBitSet(NeedsProcessing);
	movf	(ES_Timer_Tick_Resp@NeedsProcessing+1),w
	movwf	(ES_GetMSBitSet@Val2Check+1)
	movf	(ES_Timer_Tick_Resp@NeedsProcessing),w
	movwf	(ES_GetMSBitSet@Val2Check)
	fcall	_ES_GetMSBitSet
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_Timer_Tick_Resp@NextTimer2Process)
	line	307
	
l3295:	
;FrameworkSource/ES_Timers.c: 307:       if (--TMR_TimerArray[NextTimer2Process] == 0)
	lslf	(ES_Timer_Tick_Resp@NextTimer2Process),w
	addlw	low(_TMR_TimerArray|((0x1)<<8))&0ffh
	movwf	fsr0l
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movlw	01h
	subwf	indf0,f
	addfsr	fsr0,1
	movlw	0
	subwfb	indf0,f
	addfsr	fsr0,-1
		moviw	fsr0++
	iorwf	indf0,w
	btfss	status,2
	goto	u2651
	goto	u2650

u2651:
	goto	l3305
u2650:
	line	309
	
l3297:	
;FrameworkSource/ES_Timers.c: 308:       {;FrameworkSource/ES_Timers.c: 309:         NewEvent.EventType = ES_TIMEOUT;
	movlw	low(03h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(ES_Timer_Tick_Resp@NewEvent)
	line	310
	
l3299:	
;FrameworkSource/ES_Timers.c: 310:         NewEvent.EventParam = NextTimer2Process;
	movf	(ES_Timer_Tick_Resp@NextTimer2Process),w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	0+(ES_Timer_Tick_Resp@NewEvent)+01h
	movf	1+wtemp5,w
	movwf	1+(ES_Timer_Tick_Resp@NewEvent)+01h
	line	312
	
l3301:	
;FrameworkSource/ES_Timers.c: 312:         Timer2PostFunc[NextTimer2Process](NewEvent);
	movf	(ES_Timer_Tick_Resp@NewEvent),w
	pushw
	movf	(ES_Timer_Tick_Resp@NewEvent+1),w
	pushw
	movf	(ES_Timer_Tick_Resp@NewEvent+2),w
	pushw
	movf	(ES_Timer_Tick_Resp@NextTimer2Process),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_Timer2PostFunc)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_Timer2PostFunc)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	movf	btemp+11,w
	movwf	pclath
	movf	btemp+10,w
	callw
	pagesel	$
	line	314
	
l3303:	
;FrameworkSource/ES_Timers.c: 314:         TMR_ActiveFlags &= ~BitNum2SetMask[NextTimer2Process];
	movf	(ES_Timer_Tick_Resp@NextTimer2Process),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	comf	btemp+10,f
	comf	btemp+11,f
	movf	0+wtemp5,w
	movlb 0	; select bank0
	andwf	(_TMR_ActiveFlags),f
	movf	1+wtemp5,w
	andwf	(_TMR_ActiveFlags+1),f
	line	317
	
l3305:	
;FrameworkSource/ES_Timers.c: 317:       NeedsProcessing &= ~BitNum2SetMask[NextTimer2Process];
	movf	(ES_Timer_Tick_Resp@NextTimer2Process),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	comf	btemp+10,f
	comf	btemp+11,f
	movf	0+wtemp5,w
	movlb 0	; select bank0
	andwf	(ES_Timer_Tick_Resp@NeedsProcessing),f
	movf	1+wtemp5,w
	andwf	(ES_Timer_Tick_Resp@NeedsProcessing+1),f
	line	318
	
l3307:	
;FrameworkSource/ES_Timers.c: 318:     } while (NeedsProcessing != 0);
	movf	((ES_Timer_Tick_Resp@NeedsProcessing)),w
iorwf	((ES_Timer_Tick_Resp@NeedsProcessing+1)),w
	btfss	status,2
	goto	u2661
	goto	u2660
u2661:
	goto	l3293
u2660:
	line	320
	
l141:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_Timer_Tick_Resp
	__end_of_ES_Timer_Tick_Resp:
	signat	_ES_Timer_Tick_Resp,89
	global	_PostTestHarnessService0

;; *************** function _PostTestHarnessService0 *****************
;; Defined at:
;;		line 135 in file "ProjectSource/TestHarnessService0.c"
;; Parameters:    Size  Location     Type
;;  ThisEvent       3  [STACK] struct ES_Event
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 3F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_ES_PostToService
;; This function is called by:
;;		_ES_Timer_Tick_Resp
;; This function uses a reentrant model
;;
psect	text7,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/TestHarnessService0.c"
	line	135
global __ptext7
__ptext7:	;psect for function _PostTestHarnessService0
psect	text7
	file	"ProjectSource/TestHarnessService0.c"
	line	135
	global	__size_of_PostTestHarnessService0
	__size_of_PostTestHarnessService0	equ	__end_of_PostTestHarnessService0-_PostTestHarnessService0
	
_PostTestHarnessService0:	
; autosize = 0, savesize = 0, parsavesize = 0, tempsize = 0 parsize = 3, argsize = 3 vargsize = 0, retsize = 0 argfudge = 0
;incstack = 0
	opt	callstack 7
; Regs used in _PostTestHarnessService0: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	137
	
l3225:	
;ProjectSource/TestHarnessService0.c: 135: __attribute__((reentrant)) _Bool PostTestHarnessService0(ES_Event_t ThisEvent);ProjectSource/TestHarnessService0.c: 136: {;ProjectSource/TestHarnessService0.c: 137:   return ES_PostToService(MyPriority, ThisEvent);;	Return value of _PostTestHarnessService0 is never used
	;stkvar	_ThisEvent @ sp[(0)+-3]
	moviw	[0+((0)+-3)]fsr1
	pushw
	moviw	[1+((-1)+-3)]fsr1
	pushw
	moviw	[2+((-2)+-3)]fsr1
	pushw
	movlb 0	; select bank0
	movf	(_MyPriority),w
	fcall	_ES_PostToService
	line	138
	
l346:	
; _PostTestHarnessService0: autosize = 0, savesize = 0, parsavesize = 0, tempsize = 0 parsize = 3, argsize = 3, vargsize = 0
	addfsr fsr1,-3
	return
	opt callstack 0
GLOBAL	__end_of_PostTestHarnessService0
	__end_of_PostTestHarnessService0:
	signat	_PostTestHarnessService0,4217
	global	_ES_GetMSBitSet

;; *************** function _ES_GetMSBitSet *****************
;; Defined at:
;;		line 79 in file "FrameworkSource/ES_LookupTables.c"
;; Parameters:    Size  Location     Type
;;  Val2Check       2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  LoopCntr        1    4[BANK0 ] char 
;;  Nybble2Test     1    3[BANK0 ] unsigned char 
;;  ReturnVal       1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+10, btemp+11, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       5       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ES_Timer_Tick_Resp
;;		_ES_Run
;; This function uses a non-reentrant model
;;
psect	text8,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_LookupTables.c"
	line	79
global __ptext8
__ptext8:	;psect for function _ES_GetMSBitSet
psect	text8
	file	"FrameworkSource/ES_LookupTables.c"
	line	79
	global	__size_of_ES_GetMSBitSet
	__size_of_ES_GetMSBitSet	equ	__end_of_ES_GetMSBitSet-_ES_GetMSBitSet
	
_ES_GetMSBitSet:	
;incstack = 0
	opt	callstack 10
; Regs used in _ES_GetMSBitSet: [wreg-fsr0h+status,2+status,0+btemp+10+btemp+11+pclath]
	line	83
	
l3143:	
;FrameworkSource/ES_LookupTables.c: 79: uint8_t ES_GetMSBitSet(uint16_t Val2Check);FrameworkSource/ES_LookupTables.c: 80: {;FrameworkSource/ES_LookupTables.c: 81:   int8_t LoopCntr;;FrameworkSource/ES_LookupTables.c: 82:   uint8_t Nybble2Test;;FrameworkSource/ES_LookupTables.c: 83:   uint8_t ReturnVal = 128;
	movlw	low(080h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(ES_GetMSBitSet@ReturnVal)
	line	86
;FrameworkSource/ES_LookupTables.c: 86:   for (LoopCntr = sizeof(Val2Check) * (8 / 4) - 1;
	movlw	low(03h)
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_GetMSBitSet@LoopCntr)
	line	90
	
l3149:	
;FrameworkSource/ES_LookupTables.c: 88:   {;FrameworkSource/ES_LookupTables.c: 90:     Nybble2Test = (uint8_t)
	movf	(ES_GetMSBitSet@Val2Check+1),w
	movwf	btemp+11
	movf	(ES_GetMSBitSet@Val2Check),w
	movwf	btemp+10
	movf	(ES_GetMSBitSet@LoopCntr),w
	movwf	(??_ES_GetMSBitSet+0)+0
	movlw	(02h)-1
u2475:
	lslf	(??_ES_GetMSBitSet+0)+0,f
	addlw	-1
	skipz
	goto	u2475
	lslf	(??_ES_GetMSBitSet+0)+0,w
	incf	wreg,f
	goto	u2484
u2485:
	lsrf	btemp+11,f
	rrf	btemp+10,f
u2484:
	decfsz	wreg,f
	goto	u2485
	movf	0+wtemp5,w
	andlw	0Fh
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_GetMSBitSet@Nybble2Test)
	line	93
	
l3151:	
;FrameworkSource/ES_LookupTables.c: 93:     if (Nybble2Test != 0)
	movf	((ES_GetMSBitSet@Nybble2Test)),w
	btfsc	status,2
	goto	u2491
	goto	u2490
u2491:
	goto	l3155
u2490:
	line	96
	
l3153:	
;FrameworkSource/ES_LookupTables.c: 94:     {;FrameworkSource/ES_LookupTables.c: 96:       ReturnVal = Nybble2MSBitNum[Nybble2Test - 1] +
	movf	(ES_GetMSBitSet@LoopCntr),w
	movwf	(??_ES_GetMSBitSet+0)+0
	movlw	02h
u2505:
	lslf	(??_ES_GetMSBitSet+0)+0,f
	decfsz	wreg,f
	goto	u2505
	movf	(ES_GetMSBitSet@Nybble2Test),w
	addlw	low(((_Nybble2MSBitNum+0FFFFh)|8000h))
	movwf	fsr0l
	movlw	high(((_Nybble2MSBitNum+0FFFFh)|8000h))
	skipnc
	addlw	1
	movwf	fsr0h
	movf	indf0,w ;code access
	addwf	0+(??_ES_GetMSBitSet+0)+0,w
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_GetMSBitSet@ReturnVal)
	line	98
;FrameworkSource/ES_LookupTables.c: 98:       break;
	goto	l3159
	line	100
	
l3155:	
;FrameworkSource/ES_LookupTables.c: 100:   }
	movlw	low(-1)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(ES_GetMSBitSet@LoopCntr),f
	
l3157:	
	btfss	(ES_GetMSBitSet@LoopCntr),7
	goto	u2511
	goto	u2510
u2511:
	goto	l3149
u2510:
	line	101
	
l3159:	
;FrameworkSource/ES_LookupTables.c: 101:   return ReturnVal;
	movf	(ES_GetMSBitSet@ReturnVal),w
	line	102
	
l151:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_GetMSBitSet
	__end_of_ES_GetMSBitSet:
	signat	_ES_GetMSBitSet,4217
	global	_RunTestHarnessService0

;; *************** function _RunTestHarnessService0 *****************
;; Defined at:
;;		line 157 in file "ProjectSource/TestHarnessService0.c"
;; Parameters:    Size  Location     Type
;;  ThisEvent       3   25[BANK0 ] struct ES_Event
;; Auto vars:     Size  Location     Type
;;  ReturnEvent     3   69[BANK1 ] struct ES_Event
;; Return value:  Size  Location     Type
;;                  3   25[BANK0 ] struct ES_Event
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       3       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_BlinkLED
;;		_DB_printf
;;		_ES_EnQueueLIFO
;;		_ES_PostToService
;;		_ES_RecallEvents
;;		_ES_Timer_InitTimer
;;		_Start_PushToTalk
;;		_Stop_PushToTalk
;;		_puts
;; This function is called by:
;;		_ES_Run
;; This function uses a non-reentrant model
;;
psect	text9,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/TestHarnessService0.c"
	line	157
global __ptext9
__ptext9:	;psect for function _RunTestHarnessService0
psect	text9
	file	"ProjectSource/TestHarnessService0.c"
	line	157
	global	__size_of_RunTestHarnessService0
	__size_of_RunTestHarnessService0	equ	__end_of_RunTestHarnessService0-_RunTestHarnessService0
	
_RunTestHarnessService0:	
;incstack = 0
	opt	callstack 8
; Regs used in _RunTestHarnessService0: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+8-btemp+11+pclath+cstack]
	line	160
	
l3547:	
;ProjectSource/TestHarnessService0.c: 157: ES_Event_t RunTestHarnessService0(ES_Event_t ThisEvent);ProjectSource/TestHarnessService0.c: 158: {;ProjectSource/TestHarnessService0.c: 159:   ES_Event_t ReturnEvent;;ProjectSource/TestHarnessService0.c: 160:   ReturnEvent.EventType = ES_NO_EVENT;
	movlb 1	; select bank1
	clrf	(RunTestHarnessService0@ReturnEvent)^080h
	line	166
;ProjectSource/TestHarnessService0.c: 161:   static char DeferredChar = '1';;ProjectSource/TestHarnessService0.c: 166:   switch (ThisEvent.EventType)
	goto	l3597
	line	170
	
l3549:	
;ProjectSource/TestHarnessService0.c: 169:     {;ProjectSource/TestHarnessService0.c: 170:       ES_Timer_InitTimer(15, (100 / 2));
	movlw	032h
	movwf	(ES_Timer_InitTimer@NewTime)
	movlw	0
	movwf	((ES_Timer_InitTimer@NewTime))+1
	movlw	low(0Fh)
	fcall	_ES_Timer_InitTimer
	line	171
	
l3551:	
;ProjectSource/TestHarnessService0.c: 171:       puts("Service 00:");
	movlw	low(((STR_15)|8000h))
	movlb 0	; select bank0
	movwf	(puts@s)
	movlw	high(((STR_15)|8000h))
	movwf	((puts@s))+1
	fcall	_puts
	line	172
	
l3553:	
;ProjectSource/TestHarnessService0.c: 172:       DB_printf("\rES_INIT received in Service %d\r\n", MyPriority);
	movlw	low(((STR_16)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_16)|8000h))
	movwf	((DB_printf@Format))+1
	movf	(_MyPriority),w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	0+(?_DB_printf)+02h
	movf	1+wtemp5,w
	movwf	1+(?_DB_printf)+02h
	fcall	_DB_printf
	line	174
;ProjectSource/TestHarnessService0.c: 173:     };ProjectSource/TestHarnessService0.c: 174:     break;
	goto	l3599
	line	177
	
l3555:	
;ProjectSource/TestHarnessService0.c: 176:     {;ProjectSource/TestHarnessService0.c: 177:       ES_Timer_InitTimer(15, (100 * 5));
	movlw	0F4h
	movwf	(ES_Timer_InitTimer@NewTime)
	movlw	01h
	movwf	((ES_Timer_InitTimer@NewTime))+1
	movlw	low(0Fh)
	fcall	_ES_Timer_InitTimer
	line	178
;ProjectSource/TestHarnessService0.c: 178:       DB_printf("ES_TIMEOUT received from Timer %d in Service %d\r\n",
	movlw	low(((STR_17)|8000h))
	movlb 0	; select bank0
	movwf	(DB_printf@Format)
	movlw	high(((STR_17)|8000h))
	movwf	((DB_printf@Format))+1
	movf	1+(RunTestHarnessService0@ThisEvent)+01h,w
	movwf	1+(?_DB_printf)+02h
	movf	0+(RunTestHarnessService0@ThisEvent)+01h,w
	movwf	0+(?_DB_printf)+02h
	movf	(_MyPriority),w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	0+(?_DB_printf)+04h
	movf	1+wtemp5,w
	movwf	1+(?_DB_printf)+04h
	fcall	_DB_printf
	line	180
	
l3557:	
;ProjectSource/TestHarnessService0.c: 180:       BlinkLED();
	fcall	_BlinkLED
	line	182
;ProjectSource/TestHarnessService0.c: 181:     };ProjectSource/TestHarnessService0.c: 182:     break;
	goto	l3599
	line	185
	
l3559:	
;ProjectSource/TestHarnessService0.c: 184:     {;ProjectSource/TestHarnessService0.c: 185:       puts("ES_SHORT_TIMEOUT received");
	movlw	low(((STR_18)|8000h))
	movwf	(puts@s)
	movlw	high(((STR_18)|8000h))
	movwf	((puts@s))+1
	fcall	_puts
	line	187
;ProjectSource/TestHarnessService0.c: 186:     };ProjectSource/TestHarnessService0.c: 187:     break;
	goto	l3599
	line	190
	
l3561:	
;ProjectSource/TestHarnessService0.c: 189:     {;ProjectSource/TestHarnessService0.c: 190:       DB_printf("ES_NEW_KEY received with -> %c <- in Service 0\r\n",
	movlw	low(((STR_19)|8000h))
	movwf	(DB_printf@Format)
	movlw	high(((STR_19)|8000h))
	movwf	((DB_printf@Format))+1
	movf	0+(RunTestHarnessService0@ThisEvent)+01h,w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	0+(?_DB_printf)+02h
	movf	1+wtemp5,w
	movwf	1+(?_DB_printf)+02h
	fcall	_DB_printf
	line	192
	
l3563:	
;ProjectSource/TestHarnessService0.c: 192:       if ('d' == ThisEvent.EventParam)
		movlw	100
	movlb 0	; select bank0
	xorwf	(0+(RunTestHarnessService0@ThisEvent)+01h),w
iorwf	(1+(RunTestHarnessService0@ThisEvent)+01h),w
	btfss	status,2
	goto	u2861
	goto	u2860
u2861:
	goto	l3573
u2860:
	line	194
	
l3565:	
;ProjectSource/TestHarnessService0.c: 193:       {;ProjectSource/TestHarnessService0.c: 194:         ThisEvent.EventParam = DeferredChar++;
	movf	(RunTestHarnessService0@DeferredChar),w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	0+(RunTestHarnessService0@ThisEvent)+01h
	movf	1+wtemp5,w
	movwf	1+(RunTestHarnessService0@ThisEvent)+01h
	
l3567:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(RunTestHarnessService0@DeferredChar),f
	line	195
	
l3569:	
;ProjectSource/TestHarnessService0.c: 195:         if (ES_EnQueueLIFO(DeferralQueue, ThisEvent))
	movlw	low(_DeferralQueue)
	movwf	(ES_EnQueueLIFO@pBlock)
	movlw	high(_DeferralQueue)
	movwf	(ES_EnQueueLIFO@pBlock+1)
	movf	(RunTestHarnessService0@ThisEvent),w
	movwf	(ES_EnQueueLIFO@Event2Add)
	movf	(RunTestHarnessService0@ThisEvent+1),w
	movwf	(ES_EnQueueLIFO@Event2Add+1)
	movf	(RunTestHarnessService0@ThisEvent+2),w
	movwf	(ES_EnQueueLIFO@Event2Add+2)
	fcall	_ES_EnQueueLIFO
	xorlw	low(0)&0ffh
	skipnz
	goto	u2871
	goto	u2870
u2871:
	goto	l3573
u2870:
	line	197
	
l3571:	
;ProjectSource/TestHarnessService0.c: 196:         {;ProjectSource/TestHarnessService0.c: 197:           puts("ES_NEW_KEY deferred in Service 0\r");
	movlw	low(((STR_20)|8000h))
	movlb 0	; select bank0
	movwf	(puts@s)
	movlw	high(((STR_20)|8000h))
	movwf	((puts@s))+1
	fcall	_puts
	line	200
	
l3573:	
;ProjectSource/TestHarnessService0.c: 200:       if ('r' == ThisEvent.EventParam)
		movlw	114
	movlb 0	; select bank0
	xorwf	(0+(RunTestHarnessService0@ThisEvent)+01h),w
iorwf	(1+(RunTestHarnessService0@ThisEvent)+01h),w
	btfss	status,2
	goto	u2881
	goto	u2880
u2881:
	goto	l3587
u2880:
	line	202
	
l3575:	
;ProjectSource/TestHarnessService0.c: 201:       {;ProjectSource/TestHarnessService0.c: 202:         ThisEvent.EventParam = 'Q';
	movlw	051h
	movwf	0+(RunTestHarnessService0@ThisEvent)+01h
	movlw	0
	movwf	(0+(RunTestHarnessService0@ThisEvent)+01h)+1
	line	203
	
l3577:	
;ProjectSource/TestHarnessService0.c: 203:         ES_PostToService(MyPriority, ThisEvent);
	movf	(RunTestHarnessService0@ThisEvent),w
	pushw
	movf	(RunTestHarnessService0@ThisEvent+1),w
	pushw
	movf	(RunTestHarnessService0@ThisEvent+2),w
	pushw
	movf	(_MyPriority),w
	fcall	_ES_PostToService
	line	205
	
l3579:	
;ProjectSource/TestHarnessService0.c: 205:         if (1 == ES_RecallEvents(MyPriority, DeferralQueue))
	movlw	(low(_DeferralQueue|((0x0)<<8)))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(ES_RecallEvents@pBlock)
	movf	(_MyPriority),w
	fcall	_ES_RecallEvents
	xorlw	01h&0ffh
	skipz
	goto	u2891
	goto	u2890
u2891:
	goto	l3585
u2890:
	line	207
	
l3581:	
;ProjectSource/TestHarnessService0.c: 206:         {;ProjectSource/TestHarnessService0.c: 207:           puts("ES_NEW_KEY(s) recalled in Service 0\r");
	movlw	low(((STR_21)|8000h))
	movlb 0	; select bank0
	movwf	(puts@s)
	movlw	high(((STR_21)|8000h))
	movwf	((puts@s))+1
	fcall	_puts
	line	208
	
l3583:	
;ProjectSource/TestHarnessService0.c: 208:           DeferredChar = '1';
	movlw	low(031h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(RunTestHarnessService0@DeferredChar)
	goto	l3587
	line	211
	
l3585:	
	line	216
	
l3587:	
;ProjectSource/TestHarnessService0.c: 216:       if ('s' == ThisEvent.EventParam)
		movlw	115
	movlb 0	; select bank0
	xorwf	(0+(RunTestHarnessService0@ThisEvent)+01h),w
iorwf	(1+(RunTestHarnessService0@ThisEvent)+01h),w
	btfss	status,2
	goto	u2901
	goto	u2900
u2901:
	goto	l3591
u2900:
	line	218
	
l3589:	
;ProjectSource/TestHarnessService0.c: 217:       {;ProjectSource/TestHarnessService0.c: 218:           Start_PushToTalk();
	fcall	_Start_PushToTalk
	line	220
	
l3591:	
;ProjectSource/TestHarnessService0.c: 220:       if ('o' == ThisEvent.EventParam)
		movlw	111
	movlb 0	; select bank0
	xorwf	(0+(RunTestHarnessService0@ThisEvent)+01h),w
iorwf	(1+(RunTestHarnessService0@ThisEvent)+01h),w
	btfss	status,2
	goto	u2911
	goto	u2910
u2911:
	goto	l3599
u2910:
	line	222
	
l3593:	
;ProjectSource/TestHarnessService0.c: 221:       {;ProjectSource/TestHarnessService0.c: 222:           Stop_PushToTalk();
	fcall	_Stop_PushToTalk
	goto	l3599
	line	229
	
l3597:	
	movlb 0	; select bank0
	movf	(RunTestHarnessService0@ThisEvent),w
	movwf	btemp+10
	clrf	btemp+11
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "simple"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte            8     6 (fixed)
; jumptable            260     6 (fixed)
;	Chosen strategy is simple_byte

	movf 1+wtemp5,w
	opt asmopt_push
	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	l3819
	goto	l3599
	opt asmopt_pop
	
l3819:	
; Switch size 1, requested type "simple"
; Number of cases is 4, Range of values is 2 to 5
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           13     7 (average)
; direct_byte           17     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf 0+wtemp5,w
	opt asmopt_push
	opt asmopt_off
	xorlw	2^0	; case 2
	skipnz
	goto	l3549
	xorlw	3^2	; case 3
	skipnz
	goto	l3555
	xorlw	4^3	; case 4
	skipnz
	goto	l3559
	xorlw	5^4	; case 5
	skipnz
	goto	l3561
	goto	l3599
	opt asmopt_pop

	line	231
	
l3599:	
;ProjectSource/TestHarnessService0.c: 231:   return ReturnEvent;
	movlb 1	; select bank1
	movf	(RunTestHarnessService0@ReturnEvent)^080h,w
	movlb 0	; select bank0
	movwf	(?_RunTestHarnessService0)
	movlb 1	; select bank1
	movf	(RunTestHarnessService0@ReturnEvent+1)^080h,w
	movlb 0	; select bank0
	movwf	(?_RunTestHarnessService0+1)
	movlb 1	; select bank1
	movf	(RunTestHarnessService0@ReturnEvent+2)^080h,w
	movlb 0	; select bank0
	movwf	(?_RunTestHarnessService0+2)
	line	232
	
l365:	
	return
	opt callstack 0
GLOBAL	__end_of_RunTestHarnessService0
	__end_of_RunTestHarnessService0:
	signat	_RunTestHarnessService0,4219
	global	_puts

;; *************** function _puts *****************
;; Defined at:
;;		line 3 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\puts.c"
;; Parameters:    Size  Location     Type
;;  s               2   11[BANK0 ] PTR const unsigned char 
;;		 -> STR_21(37), STR_20(34), STR_18(26), STR_15(12), 
;;		 -> STR_2(29), 
;; Auto vars:     Size  Location     Type
;;  r               2    0        int 
;; Return value:  Size  Location     Type
;;                  2   11[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       5       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_fputc
;;		_fputs
;; This function is called by:
;;		_main
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text10,local,class=CODE,delta=2,merge=1,group=2
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\puts.c"
	line	3
global __ptext10
__ptext10:	;psect for function _puts
psect	text10
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\puts.c"
	line	3
	global	__size_of_puts
	__size_of_puts	equ	__end_of_puts-_puts
	
_puts:	
;incstack = 0
	opt	callstack 10
; Regs used in _puts: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
	line	7
	
l3447:	
	movlb 0	; select bank0
	clrf	(_puts$1226)
	incf	(_puts$1226),f
	
l3449:	
	movf	(puts@s+1),w
	movwf	(fputs@s+1)
	movf	(puts@s),w
	movwf	(fputs@s)
	clrf	(fputs@fp)
	fcall	_fputs
	movlb 0	; select bank0
	btfsc	(1+(?_fputs)),7
	goto	u2791
	goto	u2790
u2791:
	goto	l3455
u2790:
	
l3451:	
	movlw	0Ah
	movwf	(fputc@c)
	movlw	0
	movwf	((fputc@c))+1
	clrf	(fputc@fp)
	fcall	_fputc
	movlb 0	; select bank0
	btfsc	(1+(?_fputc)),7
	goto	u2801
	goto	u2800
u2801:
	goto	l3455
u2800:
	
l3453:	
	clrf	(_puts$1226)
	
l3455:	
	line	10
	
l1059:	
	return
	opt callstack 0
GLOBAL	__end_of_puts
	__end_of_puts:
	signat	_puts,4218
	global	_fputs

;; *************** function _fputs *****************
;; Defined at:
;;		line 8 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\nf_fputs.c"
;; Parameters:    Size  Location     Type
;;  s               2    5[BANK0 ] PTR const unsigned char 
;;		 -> STR_21(37), STR_20(34), STR_18(26), STR_15(12), 
;;		 -> STR_2(29), 
;;  fp              1    7[BANK0 ] PTR struct _IO_FILE
;;		 -> NULL(0), 
;; Auto vars:     Size  Location     Type
;;  i               2    9[BANK0 ] int 
;;  c               1    8[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    5[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       6       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_fputc
;; This function is called by:
;;		_puts
;; This function uses a non-reentrant model
;;
psect	text11,local,class=CODE,delta=2,merge=1,group=2
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\nf_fputs.c"
	line	8
global __ptext11
__ptext11:	;psect for function _fputs
psect	text11
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\nf_fputs.c"
	line	8
	global	__size_of_fputs
	__size_of_fputs	equ	__end_of_fputs-_fputs
	
_fputs:	
;incstack = 0
	opt	callstack 10
; Regs used in _fputs: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
	line	13
	
l3263:	
	movlb 0	; select bank0
	clrf	(fputs@i)
	clrf	(fputs@i+1)
	line	14
	goto	l3269
	line	15
	
l3265:	
	movf	(fputs@c),w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	(fputc@c)
	movf	1+wtemp5,w
	movwf	(fputc@c+1)
	movf	(fputs@fp),w
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(fputc@fp)
	fcall	_fputc
	line	16
	
l3267:	
	movlw	01h
	movlb 0	; select bank0
	addwf	(fputs@i),f
	movlw	0
	addwfc	(fputs@i+1),f
	line	14
	
l3269:	
	movf	(fputs@s),w
	addwf	(fputs@i),w
	movwf	fsr0l
	movf	(fputs@s+1),w
	addwfc	(fputs@i+1),w
	movwf	fsr0h
	movf	indf0,w ;code access
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(fputs@c)
	movf	(((fputs@c))),w
	btfss	status,2
	goto	u2631
	goto	u2630
u2631:
	goto	l3265
u2630:
	line	18
	
l3271:	
	movf	(fputs@i+1),w
	movwf	(?_fputs+1)
	movf	(fputs@i),w
	movwf	(?_fputs)
	line	19
	
l1085:	
	return
	opt callstack 0
GLOBAL	__end_of_fputs
	__end_of_fputs:
	signat	_fputs,8314
	global	_fputc

;; *************** function _fputc *****************
;; Defined at:
;;		line 8 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\nf_fputc.c"
;; Parameters:    Size  Location     Type
;;  c               2    0[BANK0 ] int 
;;  fp              1    2[BANK0 ] PTR struct _IO_FILE
;;		 -> NULL(0), 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] int 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       5       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		_putch
;; This function is called by:
;;		_puts
;;		_fputs
;; This function uses a non-reentrant model
;;
psect	text12,local,class=CODE,delta=2,merge=1,group=2
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\nf_fputc.c"
	line	8
global __ptext12
__ptext12:	;psect for function _fputc
psect	text12
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\nf_fputc.c"
	line	8
	global	__size_of_fputc
	__size_of_fputc	equ	__end_of_fputc-_fputc
	
_fputc:	
;incstack = 0
	opt	callstack 10
; Regs used in _fputc: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
	line	12
	
l3121:	
	movlb 0	; select bank0
	movf	((fputc@fp)),w
	btfsc	status,2
	goto	u2421
	goto	u2420
u2421:
	goto	l3125
u2420:
	
l3123:	
	movf	((fputc@fp)),w
	btfss	status,2
	goto	u2431
	goto	u2430
u2431:
	goto	l3127
u2430:
	line	13
	
l3125:	
	movf	(fputc@c),w
	fcall	_putch
	line	14
	goto	l3133
	line	15
	
l3127:	
	movf	(fputc@fp),w
	addlw	03h
	movwf	fsr0l
	clrf fsr0h	
	
		moviw	fsr0++
	iorwf	indf0,w
	btfsc	status,2
	goto	u2441
	goto	u2440

u2441:
	goto	l3131
u2440:
	
l3129:	
	movf	(fputc@fp),w
	addlw	03h
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	incf	(fputc@fp),w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	btemp+8
	moviw	[1]fsr0
	movwf	btemp+9
	movf	1+wtemp4,w
	xorlw	80h
	movwf	(??_fputc+0)+0
	movf	1+wtemp5,w
	xorlw	80h
	subwf	(??_fputc+0)+0,w
	skipz
	goto	u2455
	movf	0+wtemp5,w
	subwf	0+wtemp4,w
u2455:

	skipnc
	goto	u2451
	goto	u2450
u2451:
	goto	l3133
u2450:
	line	17
	
l3131:	
	movlb 0	; select bank0
	incf	(fputc@fp),w
	movwf	fsr0l
	clrf fsr0h	
	
	movlw	01h
	addwf	indf0,f
	addfsr	fsr0,1
	skipnc
	incf	indf0,f
	line	20
	
l3133:	
	movlb 0	; select bank0
	movf	(fputc@c),w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	(?_fputc)
	movf	1+wtemp5,w
	movwf	(?_fputc+1)
	line	21
	
l1077:	
	return
	opt callstack 0
GLOBAL	__end_of_fputc
	__end_of_fputc:
	signat	_fputc,8314
	global	_Stop_PushToTalk

;; *************** function _Stop_PushToTalk *****************
;; Defined at:
;;		line 64 in file "ProjectSource/ServoControl.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text13,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/ServoControl.c"
	line	64
global __ptext13
__ptext13:	;psect for function _Stop_PushToTalk
psect	text13
	file	"ProjectSource/ServoControl.c"
	line	64
	global	__size_of_Stop_PushToTalk
	__size_of_Stop_PushToTalk	equ	__end_of_Stop_PushToTalk-_Stop_PushToTalk
	
_Stop_PushToTalk:	
;incstack = 0
	opt	callstack 11
; Regs used in _Stop_PushToTalk: [wreg]
	line	66
	
l3477:	
;ProjectSource/ServoControl.c: 66:     CCPR2H = 8;
	movlw	low(08h)
	movlb 6	; select bank6
	movwf	(785)^0300h	;volatile
	line	67
	
l465:	
	return
	opt callstack 0
GLOBAL	__end_of_Stop_PushToTalk
	__end_of_Stop_PushToTalk:
	signat	_Stop_PushToTalk,89
	global	_Start_PushToTalk

;; *************** function _Start_PushToTalk *****************
;; Defined at:
;;		line 59 in file "ProjectSource/ServoControl.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text14,local,class=CODE,delta=2,merge=1,group=0
	line	59
global __ptext14
__ptext14:	;psect for function _Start_PushToTalk
psect	text14
	file	"ProjectSource/ServoControl.c"
	line	59
	global	__size_of_Start_PushToTalk
	__size_of_Start_PushToTalk	equ	__end_of_Start_PushToTalk-_Start_PushToTalk
	
_Start_PushToTalk:	
;incstack = 0
	opt	callstack 11
; Regs used in _Start_PushToTalk: [wreg]
	line	61
	
l3475:	
;ProjectSource/ServoControl.c: 61:     CCPR2H = 16;
	movlw	low(010h)
	movlb 6	; select bank6
	movwf	(785)^0300h	;volatile
	line	62
	
l462:	
	return
	opt callstack 0
GLOBAL	__end_of_Start_PushToTalk
	__end_of_Start_PushToTalk:
	signat	_Start_PushToTalk,89
	global	_ES_Timer_InitTimer

;; *************** function _ES_Timer_InitTimer *****************
;; Defined at:
;;		line 236 in file "FrameworkSource/ES_Timers.c"
;; Parameters:    Size  Location     Type
;;  Num             1    wreg     unsigned char 
;;  NewTime         2    0[BANK0 ] unsigned short 
;; Auto vars:     Size  Location     Type
;;  Num             1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      enum E7687
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+10, btemp+11, pclath
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text15,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Timers.c"
	line	236
global __ptext15
__ptext15:	;psect for function _ES_Timer_InitTimer
psect	text15
	file	"FrameworkSource/ES_Timers.c"
	line	236
	global	__size_of_ES_Timer_InitTimer
	__size_of_ES_Timer_InitTimer	equ	__end_of_ES_Timer_InitTimer-_ES_Timer_InitTimer
	
_ES_Timer_InitTimer:	
;incstack = 0
	opt	callstack 11
; Regs used in _ES_Timer_InitTimer: [wreg-fsr0h+status,2+status,0+btemp+10+btemp+11+pclath]
;ES_Timer_InitTimer@Num stored from wreg
	movwf	(ES_Timer_InitTimer@Num)
	line	239
	
l3309:	
;FrameworkSource/ES_Timers.c: 236: ES_TimerReturn_t ES_Timer_InitTimer(uint8_t Num, uint16_t NewTime);FrameworkSource/ES_Timers.c: 237: {;FrameworkSource/ES_Timers.c: 239:   if ((Num >= (sizeof(TMR_TimerArray) / sizeof(TMR_TimerArray[0]))) ||
	movlw	low(010h)
	subwf	(ES_Timer_InitTimer@Num),w
	skipnc
	goto	u2671
	goto	u2670
u2671:
	goto	l125
u2670:
	
l3311:	
	movf	(ES_Timer_InitTimer@Num),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_Timer2PostFunc)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_Timer2PostFunc)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	fsr0++
	movlb 0	; select bank0
movwf	(??_ES_Timer_InitTimer+0)+0
moviw	fsr0++
iorwf	(??_ES_Timer_InitTimer+0)+0
	btfsc	status,2
	goto	u2681
	goto	u2680
u2681:
	goto	l125
u2680:
	
l3313:	
	movf	((ES_Timer_InitTimer@NewTime)),w
iorwf	((ES_Timer_InitTimer@NewTime+1)),w
	btfss	status,2
	goto	u2691
	goto	u2690
u2691:
	goto	l3315
u2690:
	goto	l125
	line	247
	
l3315:	
;FrameworkSource/ES_Timers.c: 247:   TMR_TimerArray[Num] = NewTime;
	lslf	(ES_Timer_InitTimer@Num),w
	addlw	low(_TMR_TimerArray|((0x1)<<8))&0ffh
	movwf	fsr0l
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movf	(ES_Timer_InitTimer@NewTime),w
	movwi	[0]fsr0
	movf	(ES_Timer_InitTimer@NewTime+1),w
	movwi	[1]fsr0
	line	248
	
l3317:	
;FrameworkSource/ES_Timers.c: 248:   TMR_ActiveFlags |= BitNum2SetMask[Num];
	movf	(ES_Timer_InitTimer@Num),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	movf	0+wtemp5,w
	iorwf	(_TMR_ActiveFlags),f
	movf	1+wtemp5,w
	iorwf	(_TMR_ActiveFlags+1),f
	line	250
	
l125:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_Timer_InitTimer
	__end_of_ES_Timer_InitTimer:
	signat	_ES_Timer_InitTimer,8313
	global	_ES_RecallEvents

;; *************** function _ES_RecallEvents *****************
;; Defined at:
;;		line 56 in file "FrameworkSource/ES_DeferRecall.c"
;; Parameters:    Size  Location     Type
;;  WhichService    1    wreg     unsigned char 
;;  pBlock          1   19[BANK0 ] PTR struct ES_Event
;;		 -> DeferralQueue(12), 
;; Auto vars:     Size  Location     Type
;;  WhichService    1   20[BANK0 ] unsigned char 
;;  RecalledEven    3   22[BANK0 ] struct ES_Event
;;  WereEventsPu    1   21[BANK0 ] _Bool 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       5       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       6       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_ES_DeQueue
;;		_ES_PostToServiceLIFO
;; This function is called by:
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text16,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_DeferRecall.c"
	line	56
global __ptext16
__ptext16:	;psect for function _ES_RecallEvents
psect	text16
	file	"FrameworkSource/ES_DeferRecall.c"
	line	56
	global	__size_of_ES_RecallEvents
	__size_of_ES_RecallEvents	equ	__end_of_ES_RecallEvents-_ES_RecallEvents
	
_ES_RecallEvents:	
;incstack = 0
	opt	callstack 8
; Regs used in _ES_RecallEvents: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
;ES_RecallEvents@WhichService stored from wreg
	movlb 0	; select bank0
	movwf	(ES_RecallEvents@WhichService)
	line	59
	
l3321:	
;FrameworkSource/ES_DeferRecall.c: 56: _Bool ES_RecallEvents(uint8_t WhichService, ES_Event_t *pBlock);FrameworkSource/ES_DeferRecall.c: 57: {;FrameworkSource/ES_DeferRecall.c: 58:   ES_Event_t RecalledEvent;;FrameworkSource/ES_DeferRecall.c: 59:   _Bool WereEventsPulled = 0;
	clrf	(ES_RecallEvents@WereEventsPulled)
	line	63
	
l3323:	
;FrameworkSource/ES_DeferRecall.c: 62:   {;FrameworkSource/ES_DeferRecall.c: 63:     ES_DeQueue(pBlock, &RecalledEvent);
	movf	(ES_RecallEvents@pBlock),w
	movwf	(ES_DeQueue@pBlock)
	movlw	0x0

	movwf	(ES_DeQueue@pBlock+1)
	movlw	(low(ES_RecallEvents@RecalledEvent|((0x0)<<8)))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_DeQueue@pReturnEvent)
	fcall	_ES_DeQueue
	line	64
	
l3325:	
;FrameworkSource/ES_DeferRecall.c: 64:     if (RecalledEvent.EventType != ES_NO_EVENT)
	movlb 0	; select bank0
	movf	((ES_RecallEvents@RecalledEvent)),w
	btfsc	status,2
	goto	u2701
	goto	u2700
u2701:
	goto	l3331
u2700:
	line	66
	
l3327:	
;FrameworkSource/ES_DeferRecall.c: 65:     {;FrameworkSource/ES_DeferRecall.c: 66:       ES_PostToServiceLIFO(WhichService, RecalledEvent);
	movf	(ES_RecallEvents@RecalledEvent),w
	movwf	(ES_PostToServiceLIFO@TheEvent)
	movf	(ES_RecallEvents@RecalledEvent+1),w
	movwf	(ES_PostToServiceLIFO@TheEvent+1)
	movf	(ES_RecallEvents@RecalledEvent+2),w
	movwf	(ES_PostToServiceLIFO@TheEvent+2)
	movf	(ES_RecallEvents@WhichService),w
	fcall	_ES_PostToServiceLIFO
	line	67
	
l3329:	
;FrameworkSource/ES_DeferRecall.c: 67:       WereEventsPulled = 1;
	movlb 0	; select bank0
	clrf	(ES_RecallEvents@WereEventsPulled)
	incf	(ES_RecallEvents@WereEventsPulled),f
	line	69
	
l3331:	
;FrameworkSource/ES_DeferRecall.c: 69:   } while (RecalledEvent.EventType != ES_NO_EVENT);
	movf	((ES_RecallEvents@RecalledEvent)),w
	btfss	status,2
	goto	u2711
	goto	u2710
u2711:
	goto	l3323
u2710:
	line	70
	
l3333:	
;FrameworkSource/ES_DeferRecall.c: 70:   return WereEventsPulled;
	movf	(ES_RecallEvents@WereEventsPulled),w
	line	71
	
l173:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_RecallEvents
	__end_of_ES_RecallEvents:
	signat	_ES_RecallEvents,8313
	global	_ES_PostToServiceLIFO

;; *************** function _ES_PostToServiceLIFO *****************
;; Defined at:
;;		line 425 in file "FrameworkSource/ES_Framework.c"
;; Parameters:    Size  Location     Type
;;  WhichService    1    wreg     unsigned char 
;;  TheEvent        3   15[BANK0 ] struct ES_Event
;; Auto vars:     Size  Location     Type
;;  WhichService    1   18[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_ES_EnQueueLIFO
;; This function is called by:
;;		_ES_RecallEvents
;; This function uses a non-reentrant model
;;
psect	text17,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Framework.c"
	line	425
global __ptext17
__ptext17:	;psect for function _ES_PostToServiceLIFO
psect	text17
	file	"FrameworkSource/ES_Framework.c"
	line	425
	global	__size_of_ES_PostToServiceLIFO
	__size_of_ES_PostToServiceLIFO	equ	__end_of_ES_PostToServiceLIFO-_ES_PostToServiceLIFO
	
_ES_PostToServiceLIFO:	
;incstack = 0
	opt	callstack 8
; Regs used in _ES_PostToServiceLIFO: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
;ES_PostToServiceLIFO@WhichService stored from wreg
	movlb 0	; select bank0
	movwf	(ES_PostToServiceLIFO@WhichService)
	line	427
	
l3191:	
;FrameworkSource/ES_Framework.c: 425: _Bool ES_PostToServiceLIFO(uint8_t WhichService, ES_Event_t TheEvent);FrameworkSource/ES_Framework.c: 426: {;FrameworkSource/ES_Framework.c: 427:   if ((WhichService < (sizeof(EventQueues) / sizeof(EventQueues[0]))) &&
	movf	((ES_PostToServiceLIFO@WhichService)),w
	btfss	status,2
	goto	u2541
	goto	u2540
u2541:
	goto	l245
u2540:
	
l3193:	
	movf	(ES_PostToServiceLIFO@WhichService),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_EventQueues)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_EventQueues)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
		moviw	[0]fsr0
	movwf	(ES_EnQueueLIFO@pBlock)
	movlw	0x1
	movwf	(ES_EnQueueLIFO@pBlock+1)

	movf	(ES_PostToServiceLIFO@TheEvent),w
	movwf	(ES_EnQueueLIFO@Event2Add)
	movf	(ES_PostToServiceLIFO@TheEvent+1),w
	movwf	(ES_EnQueueLIFO@Event2Add+1)
	movf	(ES_PostToServiceLIFO@TheEvent+2),w
	movwf	(ES_EnQueueLIFO@Event2Add+2)
	fcall	_ES_EnQueueLIFO
	xorlw	01h&0ffh
	skipz
	goto	u2551
	goto	u2550
u2551:
	goto	l245
u2550:
	line	431
	
l3195:	
;FrameworkSource/ES_Framework.c: 430:   {;FrameworkSource/ES_Framework.c: 431:     Ready |= BitNum2SetMask[WhichService];
	movlb 0	; select bank0
	movf	(ES_PostToServiceLIFO@WhichService),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	movf	0+wtemp5,w
	iorwf	(_Ready),f
	movf	1+wtemp5,w
	iorwf	(_Ready+1),f
	line	438
	
l245:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_PostToServiceLIFO
	__end_of_ES_PostToServiceLIFO:
	signat	_ES_PostToServiceLIFO,8313
	global	_ES_EnQueueLIFO

;; *************** function _ES_EnQueueLIFO *****************
;; Defined at:
;;		line 124 in file "FrameworkSource/ES_Queue.c"
;; Parameters:    Size  Location     Type
;;  pBlock          2    6[BANK0 ] PTR struct ES_Event
;;		 -> DeferralQueue(12), Queue0(18), 
;;  Event2Add       3    8[BANK0 ] struct ES_Event
;; Auto vars:     Size  Location     Type
;;  pThisQueue      2   13[BANK0 ] PTR struct .
;;		 -> DeferralQueue(12), Queue0(18), 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       5       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       9       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        9 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___wmul
;; This function is called by:
;;		_ES_PostToServiceLIFO
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text18,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Queue.c"
	line	124
global __ptext18
__ptext18:	;psect for function _ES_EnQueueLIFO
psect	text18
	file	"FrameworkSource/ES_Queue.c"
	line	124
	global	__size_of_ES_EnQueueLIFO
	__size_of_ES_EnQueueLIFO	equ	__end_of_ES_EnQueueLIFO-_ES_EnQueueLIFO
	
_ES_EnQueueLIFO:	
;incstack = 0
	opt	callstack 8
; Regs used in _ES_EnQueueLIFO: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
	line	127
	
l3027:	
;FrameworkSource/ES_Queue.c: 124: _Bool ES_EnQueueLIFO(ES_Event_t *pBlock, ES_Event_t Event2Add);FrameworkSource/ES_Queue.c: 125: {;FrameworkSource/ES_Queue.c: 126:   pQueue_t pThisQueue;;FrameworkSource/ES_Queue.c: 127:   pThisQueue = (pQueue_t)pBlock;
	movlb 0	; select bank0
	movf	(ES_EnQueueLIFO@pBlock+1),w
	movwf	(ES_EnQueueLIFO@pThisQueue+1)
	movf	(ES_EnQueueLIFO@pBlock),w
	movwf	(ES_EnQueueLIFO@pThisQueue)
	line	129
	
l3029:	
;FrameworkSource/ES_Queue.c: 129:   if (pThisQueue->NumEntries < pThisQueue->QueueSize)
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h

	movf	indf0,w
	movwf	(??_ES_EnQueueLIFO+0)+0
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,02h
	movf	indf0,w
	movwf	(??_ES_EnQueueLIFO+1)+0
	movf	(??_ES_EnQueueLIFO+0)+0,w
	subwf	(??_ES_EnQueueLIFO+1)+0,w
	skipnc
	goto	u2241
	goto	u2240
u2241:
	goto	l3053
u2240:
	line	131
	
l3031:	
;FrameworkSource/ES_Queue.c: 130:   {;FrameworkSource/ES_Queue.c: 131:     { _INTCON_temp = INTCON; GIE=0;};
	movf	(11),w	;volatile
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(__INTCON_temp)
	
l3033:	
	bcf	(95/8),(95)&7	;volatile
	line	133
	
l3035:	
;FrameworkSource/ES_Queue.c: 133:     pThisQueue->NumEntries++;
	movlw	low(01h)
	movwf	btemp+11
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,02h
	movf	btemp+11,w
	addwf	indf0,f
	line	135
	
l3037:	
;FrameworkSource/ES_Queue.c: 135:     if (pThisQueue->CurrentIndex == 0)
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movf	(indf0),w
	btfss	status,2
	goto	u2251
	goto	u2250
u2251:
	goto	l3041
u2250:
	line	137
	
l3039:	
;FrameworkSource/ES_Queue.c: 136:     {;FrameworkSource/ES_Queue.c: 137:       pThisQueue->CurrentIndex = pThisQueue->QueueSize - 1;
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h

	movf	indf0,w
	addlw	0FFh
	movwf	btemp+11
	movf	(ES_EnQueueLIFO@pThisQueue),w
	addlw	01h
	movwf	wtemp4
	movlw	0
	addwfc	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	1+wtemp4
	movf	0+wtemp4,w
	movwf	fsr0l
	movf	1+wtemp4,w
	movwf	fsr0h
	movf	btemp+11,w
	movwf	indf0
	line	138
;FrameworkSource/ES_Queue.c: 138:     }
	goto	l3043
	line	141
	
l3041:	
;FrameworkSource/ES_Queue.c: 140:     {;FrameworkSource/ES_Queue.c: 141:       pThisQueue->CurrentIndex--;
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movlw	01h
	subwf	indf0,f
	line	143
	
l3043:	
;FrameworkSource/ES_Queue.c: 143:     pBlock[1 + pThisQueue->CurrentIndex] = Event2Add;
	movf	(ES_EnQueueLIFO@pThisQueue),w
	movwf	fsr0l
	movf	(ES_EnQueueLIFO@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movf	indf0,w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	(___wmul@multiplier)
	movf	1+wtemp5,w
	movwf	(___wmul@multiplier+1)
	movlw	03h
	movwf	(___wmul@multiplicand)
	movlw	0
	movwf	((___wmul@multiplicand))+1
	fcall	___wmul
	movlb 0	; select bank0
	movf	(0+(?___wmul)),w
	addwf	(ES_EnQueueLIFO@pBlock),w
	movwf	wtemp5
	movf	(1+(?___wmul)),w
	addwfc	(ES_EnQueueLIFO@pBlock+1),w
	movwf	1+wtemp5
	movf	0+wtemp5,w
	movwf	fsr0l
	movf	1+wtemp5,w
	movwf	fsr0h
	addfsr	fsr0,03h
	movf	(ES_EnQueueLIFO@Event2Add),w
	movwi	[0]fsr0
	movf	(ES_EnQueueLIFO@Event2Add+1),w
	movwi	[1]fsr0
	movf	(ES_EnQueueLIFO@Event2Add+2),w
	movwi	[2]fsr0
	line	144
	
l3045:	
;FrameworkSource/ES_Queue.c: 144:     { INTCON = _INTCON_temp; };
	movf	(__INTCON_temp),w
	movwf	(11)	;volatile
	line	145
	
l3047:	
;FrameworkSource/ES_Queue.c: 145:     return 1;
	movlw	low(01h)
	goto	l266
	line	149
	
l3053:	
;FrameworkSource/ES_Queue.c: 148:   {;FrameworkSource/ES_Queue.c: 149:     return 0;
	movlw	low(0)
	line	151
	
l266:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_EnQueueLIFO
	__end_of_ES_EnQueueLIFO:
	signat	_ES_EnQueueLIFO,8313
	global	_ES_DeQueue

;; *************** function _ES_DeQueue *****************
;; Defined at:
;;		line 169 in file "FrameworkSource/ES_Queue.c"
;; Parameters:    Size  Location     Type
;;  pBlock          2    6[BANK0 ] PTR struct ES_Event
;;		 -> DeferralQueue(12), Queue0(18), 
;;  pReturnEvent    1    8[BANK0 ] PTR struct ES_Event
;;		 -> ES_Run@ThisEvent(3), ES_RecallEvents@RecalledEvent(3), 
;; Auto vars:     Size  Location     Type
;;  pThisQueue      2   13[BANK0 ] PTR struct .
;;		 -> DeferralQueue(12), Queue0(18), 
;;  NumLeft         1   12[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+8, btemp+9, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       9       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        9 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___lbmod
;;		___wmul
;; This function is called by:
;;		_ES_RecallEvents
;;		_ES_Run
;; This function uses a non-reentrant model
;;
psect	text19,local,class=CODE,delta=2,merge=1,group=0
	line	169
global __ptext19
__ptext19:	;psect for function _ES_DeQueue
psect	text19
	file	"FrameworkSource/ES_Queue.c"
	line	169
	global	__size_of_ES_DeQueue
	__size_of_ES_DeQueue	equ	__end_of_ES_DeQueue-_ES_DeQueue
	
_ES_DeQueue:	
;incstack = 0
	opt	callstack 9
; Regs used in _ES_DeQueue: [wreg-fsr0h+status,2+status,0+btemp+8-btemp+11+pclath+cstack]
	line	174
	
l3163:	
;FrameworkSource/ES_Queue.c: 169: uint8_t ES_DeQueue(ES_Event_t *pBlock, ES_Event_t *pReturnEvent);FrameworkSource/ES_Queue.c: 170: {;FrameworkSource/ES_Queue.c: 171:   pQueue_t pThisQueue;;FrameworkSource/ES_Queue.c: 172:   uint8_t NumLeft;;FrameworkSource/ES_Queue.c: 174:   pThisQueue = (pQueue_t)pBlock;
	movlb 0	; select bank0
	movf	(ES_DeQueue@pBlock+1),w
	movwf	(ES_DeQueue@pThisQueue+1)
	movf	(ES_DeQueue@pBlock),w
	movwf	(ES_DeQueue@pThisQueue)
	line	175
	
l3165:	
;FrameworkSource/ES_Queue.c: 175:   if (pThisQueue->NumEntries > 0)
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,02h
	movf	(indf0),w
	btfsc	status,2
	goto	u2521
	goto	u2520
u2521:
	goto	l3183
u2520:
	line	177
	
l3167:	
;FrameworkSource/ES_Queue.c: 176:   {;FrameworkSource/ES_Queue.c: 177:     { _INTCON_temp = INTCON; GIE=0;};
	movf	(11),w	;volatile
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(__INTCON_temp)
	
l3169:	
	bcf	(95/8),(95)&7	;volatile
	line	178
	
l3171:	
;FrameworkSource/ES_Queue.c: 178:     *pReturnEvent = pBlock[1 + pThisQueue->CurrentIndex];
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movf	indf0,w
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movwf	(___wmul@multiplier)
	movf	1+wtemp5,w
	movwf	(___wmul@multiplier+1)
	movlw	03h
	movwf	(___wmul@multiplicand)
	movlw	0
	movwf	((___wmul@multiplicand))+1
	fcall	___wmul
	movlb 0	; select bank0
	movf	(0+(?___wmul)),w
	addwf	(ES_DeQueue@pBlock),w
	movwf	wtemp5
	movf	(1+(?___wmul)),w
	addwfc	(ES_DeQueue@pBlock+1),w
	movwf	1+wtemp5
	movf	0+wtemp5,w
	movwf	fsr0l
	movf	1+wtemp5,w
	movwf	fsr0h
	addfsr	fsr0,03h
	moviw	[0]fsr0
	movwf	((??_ES_DeQueue+0)+0)
	moviw	[1]fsr0
	movwf	((??_ES_DeQueue+0)+0+1)
	moviw	[2]fsr0
	movwf	((??_ES_DeQueue+0)+0+2)
	movf	(ES_DeQueue@pReturnEvent),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	0+(??_ES_DeQueue+0)+0,w
	movwi	[0]fsr0
	movf	1+(??_ES_DeQueue+0)+0,w
	movwi	[1]fsr0
	movf	2+(??_ES_DeQueue+0)+0,w
	movwi	[2]fsr0
	line	180
	
l3173:	
;FrameworkSource/ES_Queue.c: 180:     pThisQueue->CurrentIndex++;
	movlw	low(01h)
	movwf	btemp+11
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movf	btemp+11,w
	addwf	indf0,f
	line	182
	
l3175:	
;FrameworkSource/ES_Queue.c: 182:     if (pThisQueue->CurrentIndex >= pThisQueue->QueueSize)
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h

	movf	indf0,w
	movwf	(??_ES_DeQueue+0)+0
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movf	indf0,w
	movwf	(??_ES_DeQueue+1)+0
	movf	(??_ES_DeQueue+0)+0,w
	subwf	(??_ES_DeQueue+1)+0,w
	skipc
	goto	u2531
	goto	u2530
u2531:
	goto	l3179
u2530:
	line	184
	
l3177:	
;FrameworkSource/ES_Queue.c: 183:     {;FrameworkSource/ES_Queue.c: 184:       pThisQueue->CurrentIndex = (uint8_t)(pThisQueue->CurrentIndex % pThisQueue->QueueSize);
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h

	movf	indf0,w
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(___lbmod@divisor)
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	movf	indf0,w
	fcall	___lbmod
	movwf	btemp+11
	movlb 0	; select bank0
	movf	(ES_DeQueue@pThisQueue),w
	addlw	01h
	movwf	wtemp4
	movlw	0
	addwfc	(ES_DeQueue@pThisQueue+1),w
	movwf	1+wtemp4
	movf	0+wtemp4,w
	movwf	fsr0l
	movf	1+wtemp4,w
	movwf	fsr0h
	movf	btemp+11,w
	movwf	indf0
	line	187
	
l3179:	
;FrameworkSource/ES_Queue.c: 187:     NumLeft = --pThisQueue->NumEntries;
	movf	(ES_DeQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_DeQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,02h
	moviw	[0]fsr0
	decf	wreg,w
	movwi	[0]fsr0
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_DeQueue@NumLeft)
	line	188
	
l3181:	
;FrameworkSource/ES_Queue.c: 188:     { INTCON = _INTCON_temp; };
	movf	(__INTCON_temp),w
	movwf	(11)	;volatile
	line	189
;FrameworkSource/ES_Queue.c: 189:   }
	goto	l3187
	line	192
	
l3183:	
;FrameworkSource/ES_Queue.c: 191:   {;FrameworkSource/ES_Queue.c: 192:     (*pReturnEvent).EventType = ES_NO_EVENT;
	movf	(ES_DeQueue@pReturnEvent),w
	movwf	fsr0l
	clrf fsr0h	
	
	clrf	indf0
	line	193
	
l3185:	
;FrameworkSource/ES_Queue.c: 193:     (*pReturnEvent).EventParam = 0;
	incf	(ES_DeQueue@pReturnEvent),w
	movwf	fsr0l
	clrf fsr0h	
	
	movlw	0
	movwi	[0]fsr0
	movwi	[1]fsr0
	line	194
;FrameworkSource/ES_Queue.c: 194:     NumLeft = 0;
	clrf	(ES_DeQueue@NumLeft)
	line	196
	
l3187:	
;FrameworkSource/ES_Queue.c: 196:   return NumLeft;
	movf	(ES_DeQueue@NumLeft),w
	line	197
	
l273:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_DeQueue
	__end_of_ES_DeQueue:
	signat	_ES_DeQueue,8313
	global	___wmul

;; *************** function ___wmul *****************
;; Defined at:
;;		line 15 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\Umul16.c"
;; Parameters:    Size  Location     Type
;;  multiplier      2    0[BANK0 ] unsigned int 
;;  multiplicand    2    2[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  product         2    4[BANK0 ] unsigned int 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       6       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ES_Initialize
;;		_ES_Run
;;		_ES_PostAll
;;		_ES_PostToService
;;		_ES_PostToServiceLIFO
;;		_ES_DeQueue
;;		_ES_EnQueueLIFO
;; This function uses a non-reentrant model
;;
psect	text20,local,class=CODE,delta=2,merge=1,group=1
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\Umul16.c"
	line	15
global __ptext20
__ptext20:	;psect for function ___wmul
psect	text20
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\Umul16.c"
	line	15
	global	__size_of___wmul
	__size_of___wmul	equ	__end_of___wmul-___wmul
	
___wmul:	
;incstack = 0
	opt	callstack 8
; Regs used in ___wmul: [wreg+status,2+status,0]
	line	43
	
l2997:	
	movlb 0	; select bank0
	clrf	(___wmul@product)
	clrf	(___wmul@product+1)
	line	45
	
l2999:	
	btfss	(___wmul@multiplier),(0)&7
	goto	u2181
	goto	u2180
u2181:
	goto	l600
u2180:
	line	46
	
l3001:	
	movf	(___wmul@multiplicand),w
	addwf	(___wmul@product),f
	movf	(___wmul@multiplicand+1),w
	addwfc	(___wmul@product+1),f
	
l600:	
	line	47
	movlw	01h
	
u2195:
	lslf	(___wmul@multiplicand),f
	rlf	(___wmul@multiplicand+1),f
	decfsz	wreg,f
	goto	u2195
	line	48
	
l3003:	
	movlw	01h
	
u2205:
	lsrf	(___wmul@multiplier+1),f
	rrf	(___wmul@multiplier),f
	decfsz	wreg,f
	goto	u2205
	line	49
	
l3005:	
	movf	((___wmul@multiplier)),w
iorwf	((___wmul@multiplier+1)),w
	btfss	status,2
	goto	u2211
	goto	u2210
u2211:
	goto	l2999
u2210:
	line	52
	
l3007:	
	movf	(___wmul@product+1),w
	movwf	(?___wmul+1)
	movf	(___wmul@product),w
	movwf	(?___wmul)
	line	53
	
l602:	
	return
	opt callstack 0
GLOBAL	__end_of___wmul
	__end_of___wmul:
	signat	___wmul,8314
	global	___lbmod

;; *************** function ___lbmod *****************
;; Defined at:
;;		line 4 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lbmod.c"
;; Parameters:    Size  Location     Type
;;  dividend        1    wreg     unsigned char 
;;  divisor         1    1[COMMON] unsigned char 
;; Auto vars:     Size  Location     Type
;;  dividend        1    1[BANK0 ] unsigned char 
;;  rem             1    3[BANK0 ] unsigned char 
;;  counter         1    2[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ES_DeQueue
;; This function uses a non-reentrant model
;;
psect	text21,local,class=CODE,delta=2,merge=1,group=1
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lbmod.c"
	line	4
global __ptext21
__ptext21:	;psect for function ___lbmod
psect	text21
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lbmod.c"
	line	4
	global	__size_of___lbmod
	__size_of___lbmod	equ	__end_of___lbmod-___lbmod
	
___lbmod:	
;incstack = 0
	opt	callstack 9
; Regs used in ___lbmod: [wreg+status,2+status,0+btemp+11]
;___lbmod@dividend stored from wreg
	movlb 0	; select bank0
	movwf	(___lbmod@dividend)
	line	9
	
l3057:	
	movlw	low(08h)
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(___lbmod@counter)
	line	10
	
l3059:	
	clrf	(___lbmod@rem)
	line	12
	
l3061:	
	movf	(___lbmod@dividend),w
	movwf	(??___lbmod+0)+0
	movlw	07h
u2265:
	lsrf	(??___lbmod+0)+0,f
	decfsz	wreg,f
	goto	u2265
	lslf	(___lbmod@rem),w
	iorwf	0+(??___lbmod+0)+0,w
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(___lbmod@rem)
	line	13
	
l3063:	
	clrc
	rlf	(___lbmod@dividend),f

	line	14
	
l3065:	
	movf	(___lbmod@divisor),w
	subwf	(___lbmod@rem),w
	skipc
	goto	u2271
	goto	u2270
u2271:
	goto	l818
u2270:
	line	15
	
l3067:	
	movf	(___lbmod@divisor),w
	subwf	(___lbmod@rem),f
	
l818:	
	line	16
	movlw	01h
	subwf	(___lbmod@counter),f
	btfss	status,2
	goto	u2281
	goto	u2280
u2281:
	goto	l3061
u2280:
	line	17
	
l3069:	
	movf	(___lbmod@rem),w
	line	18
	
l820:	
	return
	opt callstack 0
GLOBAL	__end_of___lbmod
	__end_of___lbmod:
	signat	___lbmod,8313
	global	_DB_printf

;; *************** function _DB_printf *****************
;; Defined at:
;;		line 76 in file "ProjectSource/dbprintf.c"
;; Parameters:    Size  Location     Type
;;  Format          2   15[BANK0 ] PTR const unsigned char 
;;		 -> STR_24(11), STR_19(49), STR_17(50), STR_16(34), 
;;		 -> STR_14(15), STR_13(23), STR_12(24), STR_11(27), 
;;		 -> STR_10(34), STR_9(36), STR_8(55), STR_7(4), 
;;		 -> STR_4(7), STR_3(54), STR_1(5), 
;; Auto vars:     Size  Location     Type
;;  LineBuffer     61    0[BANK1 ] unsigned char [61]
;;  pString         2   65[BANK1 ] PTR unsigned char 
;;		 -> STR_22(7), ?_DB_printf(1), STR_6(12), STR_5(9), 
;;  i               2   63[BANK1 ] int 
;;  u               2   61[BANK1 ] unsigned int 
;;  pBuffer         1   68[BANK1 ] PTR unsigned char 
;;		 -> DB_printf@LineBuffer(61), 
;;  Arguments       1   67[BANK1 ] PTR void [1]
;;		 -> ?_DB_printf(1), 
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       6       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0      69       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       7      69       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:       76 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_putch
;;		_uitoa
;; This function is called by:
;;		_main
;;		_RunTestHarnessService0
;;		_InitPWM
;; This function uses a non-reentrant model
;;
psect	text22,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/dbprintf.c"
	line	76
global __ptext22
__ptext22:	;psect for function _DB_printf
psect	text22
	file	"ProjectSource/dbprintf.c"
	line	76
	global	__size_of_DB_printf
	__size_of_DB_printf	equ	__end_of_DB_printf-_DB_printf
	
_DB_printf:	
;incstack = 0
	opt	callstack 10
; Regs used in _DB_printf: [wreg-fsr0h+status,2+status,0+btemp+10+btemp+11+pclath+cstack]
	line	85
	
l3347:	
;ProjectSource/dbprintf.c: 76: void DB_printf(const char *Format, ...);ProjectSource/dbprintf.c: 77: {;ProjectSource/dbprintf.c: 78:    va_list Arguments;;ProjectSource/dbprintf.c: 79:    char *pBuffer,;ProjectSource/dbprintf.c: 80:         *pString;;ProjectSource/dbprintf.c: 81:    int i;;ProjectSource/dbprintf.c: 82:  unsigned int u;;ProjectSource/dbprintf.c: 83:    char LineBuffer[60 +1];;ProjectSource/dbprintf.c: 85:    *Arguments = __va_start();
	movlw	(low(?_DB_printf|((0x0)<<8)+02h))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movlb 1	; select bank1
	movwf	(DB_printf@Arguments)^080h
	line	86
;ProjectSource/dbprintf.c: 86:    pBuffer = LineBuffer;
	movlw	(low(DB_printf@LineBuffer|((0x0)<<8)))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(DB_printf@pBuffer)^080h
	line	87
	
l3349:	
;ProjectSource/dbprintf.c: 87:    *pBuffer = 0;
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	clrf	indf0
	line	88
;ProjectSource/dbprintf.c: 88:    while (*Format)
	goto	l3427
	line	89
	
l3351:	
;ProjectSource/dbprintf.c: 89:       if (*Format != '%')
	movf	(DB_printf@Format),w
	movwf	fsr0l
	movf	((DB_printf@Format+1)),w
	movwf	fsr0h
	moviw	fsr0++
xorlw	37
	btfsc	status,2
	goto	u2721
	goto	u2720
u2721:
	goto	l3423
u2720:
	line	90
	
l3353:	
;ProjectSource/dbprintf.c: 90:             *pBuffer++ = *Format++;
	movf	(DB_printf@Format),w
	movwf	fsr0l
	movf	((DB_printf@Format+1)),w
	movwf	fsr0h
	movf	indf0,w ;code access
	movwf	btemp+11
	movlb 1	; select bank1
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	
l3355:	
	movlw	01h
	movlb 0	; select bank0
	addwf	(DB_printf@Format),f
	movlw	0
	addwfc	(DB_printf@Format+1),f
	
l3357:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 1	; select bank1
	addwf	(DB_printf@pBuffer)^080h,f
	goto	l3427
	line	96
	
l3359:	
;ProjectSource/dbprintf.c: 96:                i = (*(int *)__va_arg(*(int **)Arguments, (int)0));
	movlb 1	; select bank1
	movf	(DB_printf@Arguments)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(DB_printf@i)^080h
	moviw	[1]fsr0
	movwf	(DB_printf@i+1)^080h
	
l3361:	
	movlw	low(02h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@Arguments)^080h,f
	line	97
	
l3363:	
;ProjectSource/dbprintf.c: 97:                if (i < 0)
	btfss	(DB_printf@i+1)^080h,7
	goto	u2731
	goto	u2730
u2731:
	goto	l3371
u2730:
	line	99
	
l3365:	
;ProjectSource/dbprintf.c: 98:                {;ProjectSource/dbprintf.c: 99:                   *pBuffer++ = '-';
	movlw	low(02Dh)
	movwf	btemp+11
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	
l3367:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@pBuffer)^080h,f
	line	100
	
l3369:	
;ProjectSource/dbprintf.c: 100:                   i = -1*i;
	comf	(DB_printf@i)^080h,f
	comf	(DB_printf@i+1)^080h,f
	incf	(DB_printf@i)^080h,f
	skipnz
	incf	(DB_printf@i+1)^080h,f
	line	102
	
l3371:	
;ProjectSource/dbprintf.c: 102:                uitoa(&pBuffer, (unsigned int)i, 10);
	movf	(DB_printf@i+1)^080h,w
	movlb 0	; select bank0
	movwf	(uitoa@i+1)
	movlb 1	; select bank1
	movf	(DB_printf@i)^080h,w
	movlb 0	; select bank0
	movwf	(uitoa@i)
	movlw	0Ah
	movwf	(uitoa@base)
	movlw	0
	movwf	((uitoa@base))+1
	movlw	(low(DB_printf@pBuffer|((0x0)<<8)))&0ffh
	fcall	_uitoa
	line	103
;ProjectSource/dbprintf.c: 103:                break;
	goto	l3425
	line	105
	
l3373:	
;ProjectSource/dbprintf.c: 105:                u = (*(unsigned int *)__va_arg(*(unsigned int **)Arguments, (unsigned int)0));
	movlb 1	; select bank1
	movf	(DB_printf@Arguments)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(DB_printf@u)^080h
	moviw	[1]fsr0
	movwf	(DB_printf@u+1)^080h
	
l3375:	
	movlw	low(02h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@Arguments)^080h,f
	line	108
	
l3377:	
;ProjectSource/dbprintf.c: 108:                uitoa(&pBuffer, u, 16);
	movf	(DB_printf@u+1)^080h,w
	movlb 0	; select bank0
	movwf	(uitoa@i+1)
	movlb 1	; select bank1
	movf	(DB_printf@u)^080h,w
	movlb 0	; select bank0
	movwf	(uitoa@i)
	movlw	010h
	movwf	(uitoa@base)
	movlw	0
	movwf	((uitoa@base))+1
	movlw	(low(DB_printf@pBuffer|((0x0)<<8)))&0ffh
	fcall	_uitoa
	line	109
;ProjectSource/dbprintf.c: 109:                break;
	goto	l3425
	line	111
	
l3379:	
;ProjectSource/dbprintf.c: 111:                u = (*(unsigned int *)__va_arg(*(unsigned int **)Arguments, (unsigned int)0));
	movlb 1	; select bank1
	movf	(DB_printf@Arguments)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(DB_printf@u)^080h
	moviw	[1]fsr0
	movwf	(DB_printf@u+1)^080h
	
l3381:	
	movlw	low(02h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@Arguments)^080h,f
	line	112
	
l3383:	
;ProjectSource/dbprintf.c: 112:                uitoa(&pBuffer, u, 10);
	movf	(DB_printf@u+1)^080h,w
	movlb 0	; select bank0
	movwf	(uitoa@i+1)
	movlb 1	; select bank1
	movf	(DB_printf@u)^080h,w
	movlb 0	; select bank0
	movwf	(uitoa@i)
	movlw	0Ah
	movwf	(uitoa@base)
	movlw	0
	movwf	((uitoa@base))+1
	movlw	(low(DB_printf@pBuffer|((0x0)<<8)))&0ffh
	fcall	_uitoa
	line	113
;ProjectSource/dbprintf.c: 113:                break;
	goto	l3425
	line	115
	
l3385:	
;ProjectSource/dbprintf.c: 115:                *pBuffer++ = (char) (*(unsigned int *)__va_arg(*(unsigned int **)Arguments, (unsigned int)0));
	movlb 1	; select bank1
	movf	(DB_printf@Arguments)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	indf0,w
	movwf	btemp+11
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	
l3387:	
	movlw	low(02h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@Arguments)^080h,f
	
l3389:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@pBuffer)^080h,f
	line	116
;ProjectSource/dbprintf.c: 116:                break;
	goto	l3425
	line	118
	
l3391:	
;ProjectSource/dbprintf.c: 118:                pString = (*(char * *)__va_arg(*(char * **)Arguments, (char *)0));
	movlb 1	; select bank1
	movf	(DB_printf@Arguments)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	moviw	[0]fsr0
	movwf	(DB_printf@pString)^080h
	moviw	[1]fsr0
	movwf	(DB_printf@pString+1)^080h
	
l3393:	
	movlw	low(02h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@Arguments)^080h,f
	line	119
	
l3395:	
;ProjectSource/dbprintf.c: 119:                if (!pString)
	movf	((DB_printf@pString)^080h),w
iorwf	((DB_printf@pString+1)^080h),w
	btfss	status,2
	goto	u2741
	goto	u2740
u2741:
	goto	l3405
u2740:
	line	120
	
l3397:	
;ProjectSource/dbprintf.c: 120:                   pString = "(null)";
	movlw	low(((STR_22)|8000h))
	movwf	(DB_printf@pString)^080h
	movlw	high(((STR_22)|8000h))
	movwf	((DB_printf@pString)^080h)+1
	goto	l3405
	line	122
	
l3399:	
;ProjectSource/dbprintf.c: 122:                   *pBuffer++ = *pString++;
	movf	(DB_printf@pString)^080h,w
	movwf	fsr0l
	movf	((DB_printf@pString+1)^080h),w
	movwf	fsr0h
	movf	indf0,w ;code access
	movwf	btemp+11
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	
l3401:	
	movlw	01h
	addwf	(DB_printf@pString)^080h,f
	movlw	0
	addwfc	(DB_printf@pString+1)^080h,f
	
l3403:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@pBuffer)^080h,f
	line	121
	
l3405:	
;ProjectSource/dbprintf.c: 121:                while (*pString)
	movf	(DB_printf@pString)^080h,w
	movwf	fsr0l
	movf	((DB_printf@pString+1)^080h),w
	movwf	fsr0h
	moviw	fsr0++
	btfss	status,2
	goto	u2751
	goto	u2750
u2751:
	goto	l3399
u2750:
	goto	l3425
	line	125
	
l3407:	
;ProjectSource/dbprintf.c: 125:                *pBuffer++ = '%';
	movlw	low(025h)
	movwf	btemp+11
	movlb 1	; select bank1
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	goto	l3389
	line	128
	
l3411:	
;ProjectSource/dbprintf.c: 128:                 *pBuffer++ = 'B';
	movlw	low(042h)
	movwf	btemp+11
	movlb 1	; select bank1
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	
l3413:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@pBuffer)^080h,f
	line	129
	
l3415:	
;ProjectSource/dbprintf.c: 129:                 *pBuffer++ = 'A';
	movlw	low(041h)
	movwf	btemp+11
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(DB_printf@pBuffer)^080h,f
	line	130
	
l3417:	
;ProjectSource/dbprintf.c: 130:                 *pBuffer++ = 'D';
	movlw	low(044h)
	movwf	btemp+11
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	goto	l3389
	line	132
	
l3423:	
	movlw	01h
	addwf	(DB_printf@Format),f
	movlw	0
	addwfc	(DB_printf@Format+1),f
	movf	((DB_printf@Format)),w
	movwf	fsr0l
	movf	(((DB_printf@Format+1))),w
	movwf	fsr0h
	movf	indf0,w ;code access
	movwf	btemp+10
	clrf	btemp+11
	; Switch on 2 bytes has been partitioned into a top level switch of size 1, and 1 sub-switches
; Switch size 1, requested type "simple"
; Number of cases is 1, Range of values is 0 to 0
; switch strategies available:
; Name         Instructions Cycles
; simple_byte            4     3 (average)
; direct_byte            8     6 (fixed)
; jumptable            260     6 (fixed)
;	Chosen strategy is simple_byte

	movf 1+wtemp5,w
	opt asmopt_push
	opt asmopt_off
	xorlw	0^0	; case 0
	skipnz
	goto	l3821
	goto	l3411
	opt asmopt_pop
	
l3821:	
; Switch size 1, requested type "simple"
; Number of cases is 6, Range of values is 37 to 120
; switch strategies available:
; Name         Instructions Cycles
; simple_byte           19    10 (average)
; direct_byte          177     9 (fixed)
; jumptable            263     9 (fixed)
;	Chosen strategy is simple_byte

	movf 0+wtemp5,w
	opt asmopt_push
	opt asmopt_off
	xorlw	37^0	; case 37
	skipnz
	goto	l3407
	xorlw	99^37	; case 99
	skipnz
	goto	l3385
	xorlw	100^99	; case 100
	skipnz
	goto	l3359
	xorlw	115^100	; case 115
	skipnz
	goto	l3391
	xorlw	117^115	; case 117
	skipnz
	goto	l3379
	xorlw	120^117	; case 120
	skipnz
	goto	l3373
	goto	l3411
	opt asmopt_pop

	line	133
	
l3425:	
;ProjectSource/dbprintf.c: 133:          Format++;
	movlw	01h
	movlb 0	; select bank0
	addwf	(DB_printf@Format),f
	movlw	0
	addwfc	(DB_printf@Format+1),f
	line	88
	
l3427:	
;ProjectSource/dbprintf.c: 88:    while (*Format)
	movlb 0	; select bank0
	movf	(DB_printf@Format),w
	movwf	fsr0l
	movf	((DB_printf@Format+1)),w
	movwf	fsr0h
	moviw	fsr0++
	btfss	status,2
	goto	u2761
	goto	u2760
u2761:
	goto	l3351
u2760:
	line	135
	
l3429:	
;ProjectSource/dbprintf.c: 135:    *pBuffer = 0;
	movlb 1	; select bank1
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	clrf	indf0
	line	138
	
l3431:	
;ProjectSource/dbprintf.c: 138:    for (pBuffer = LineBuffer; *pBuffer != 0; pBuffer++)
	movlw	(low(DB_printf@LineBuffer|((0x0)<<8)))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(DB_printf@pBuffer)^080h
	goto	l3441
	line	140
	
l3433:	
;ProjectSource/dbprintf.c: 139:    {;ProjectSource/dbprintf.c: 140:       if (*pBuffer != '\n')
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
		movlw	10
	xorwf	(indf0),w
	btfsc	status,2
	goto	u2771
	goto	u2770
u2771:
	goto	l3437
u2770:
	line	141
	
l3435:	
;ProjectSource/dbprintf.c: 141:          putch(*pBuffer);
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	indf0,w
	fcall	_putch
	goto	l3439
	line	144
	
l3437:	
;ProjectSource/dbprintf.c: 143:       {;ProjectSource/dbprintf.c: 144:          putch(0x0d);
	movlw	low(0Dh)
	fcall	_putch
	line	145
;ProjectSource/dbprintf.c: 145:          putch(0x0a);
	movlw	low(0Ah)
	fcall	_putch
	line	147
	
l3439:	
;ProjectSource/dbprintf.c: 147:    }
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 1	; select bank1
	addwf	(DB_printf@pBuffer)^080h,f
	
l3441:	
	movf	(DB_printf@pBuffer)^080h,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	(indf0),w
	btfss	status,2
	goto	u2781
	goto	u2780
u2781:
	goto	l3433
u2780:
	line	149
	
l418:	
	return
	opt callstack 0
GLOBAL	__end_of_DB_printf
	__end_of_DB_printf:
	signat	_DB_printf,601
	global	_uitoa

;; *************** function _uitoa *****************
;; Defined at:
;;		line 151 in file "ProjectSource/dbprintf.c"
;; Parameters:    Size  Location     Type
;;  LineBuffer      1    wreg     PTR PTR unsigned char 
;;		 -> DB_printf@pBuffer(1), 
;;  i               2    7[BANK0 ] unsigned int 
;;  base            2    9[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  LineBuffer      1   14[BANK0 ] PTR PTR unsigned char 
;;		 -> DB_printf@pBuffer(1), 
;;  rem             2   11[BANK0 ] unsigned int 
;;  s               1   13[BANK0 ] PTR unsigned char 
;;		 -> FieldBuf(7), 
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       8       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___lwdiv
;;		___lwmod
;; This function is called by:
;;		_DB_printf
;; This function uses a non-reentrant model
;;
psect	text23,local,class=CODE,delta=2,merge=1,group=0
	line	151
global __ptext23
__ptext23:	;psect for function _uitoa
psect	text23
	file	"ProjectSource/dbprintf.c"
	line	151
	global	__size_of_uitoa
	__size_of_uitoa	equ	__end_of_uitoa-_uitoa
	
_uitoa:	
;incstack = 0
	opt	callstack 10
; Regs used in _uitoa: [wreg-fsr0h+status,2+status,0+btemp+11+pclath+cstack]
;uitoa@LineBuffer stored from wreg
	movlb 0	; select bank0
	movwf	(uitoa@LineBuffer)
	line	156
	
l3229:	
;ProjectSource/dbprintf.c: 151: static void uitoa(char **LineBuffer, unsigned int i, unsigned int base);ProjectSource/dbprintf.c: 152: {;ProjectSource/dbprintf.c: 153:    char *s;;ProjectSource/dbprintf.c: 154:    unsigned int rem;;ProjectSource/dbprintf.c: 156:    FieldBuf[6] = 0;
	clrf	0+(_FieldBuf)+06h
	line	157
	
l3231:	
;ProjectSource/dbprintf.c: 157:    if (i == 0)
	movf	((uitoa@i)),w
iorwf	((uitoa@i+1)),w
	btfss	status,2
	goto	u2581
	goto	u2580
u2581:
	goto	l3239
u2580:
	line	159
	
l3233:	
;ProjectSource/dbprintf.c: 158:    {;ProjectSource/dbprintf.c: 159:       (*LineBuffer)[0] = '0';
	movlw	low(030h)
	movwf	btemp+11
	movf	(uitoa@LineBuffer),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	indf0,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	line	160
	
l3235:	
;ProjectSource/dbprintf.c: 160:       ++(*LineBuffer);
	movlw	low(01h)
	movwf	btemp+11
	movf	(uitoa@LineBuffer),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	addwf	indf0,f
	goto	l422
	line	163
	
l3239:	
;ProjectSource/dbprintf.c: 163:    s = &FieldBuf[6];
	movlw	(low(_FieldBuf|((0x0)<<8)+06h))&0ffh
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(uitoa@s)
	line	164
;ProjectSource/dbprintf.c: 164:    while (i)
	goto	l3253
	line	166
	
l3241:	
;ProjectSource/dbprintf.c: 165:    {;ProjectSource/dbprintf.c: 166:       rem = i % base;
	movf	(uitoa@base+1),w
	movwf	(___lwmod@divisor+1)
	movf	(uitoa@base),w
	movwf	(___lwmod@divisor)
	movf	(uitoa@i+1),w
	movwf	(___lwmod@dividend+1)
	movf	(uitoa@i),w
	movwf	(___lwmod@dividend)
	fcall	___lwmod
	movlb 0	; select bank0
	movf	(1+(?___lwmod)),w
	movwf	(uitoa@rem+1)
	movf	(0+(?___lwmod)),w
	movwf	(uitoa@rem)
	line	167
	
l3243:	
;ProjectSource/dbprintf.c: 167:       if (rem < 10)
	movlw	0
	subwf	(uitoa@rem+1),w
	movlw	0Ah
	skipnz
	subwf	(uitoa@rem),w
	skipnc
	goto	u2591
	goto	u2590
u2591:
	goto	l3247
u2590:
	line	168
	
l3245:	
;ProjectSource/dbprintf.c: 168:          *--s = rem + '0';
	movf	(uitoa@rem),w
	addlw	030h
	movwf	btemp+11
	movlw	01h
	subwf	(uitoa@s),f
	movf	((uitoa@s)),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	goto	l3251
	line	170
	
l3247:	
;ProjectSource/dbprintf.c: 170:       if (base == 16)
		movlw	16
	xorwf	((uitoa@base)),w
iorwf	((uitoa@base+1)),w
	btfss	status,2
	goto	u2601
	goto	u2600
u2601:
	goto	l3251
u2600:
	line	171
	
l3249:	
;ProjectSource/dbprintf.c: 171:          *--s = "abcdef"[rem - 10];
	movf	(uitoa@rem+1),w
	movwf	fsr0h
	movf	(uitoa@rem),w
	movwf	fsr0l
	movlw	low(((STR_23+0FFF6h)|8000h))
	addwf	fsr0l,f
	movlw	high(((STR_23+0FFF6h)|8000h))
	addwfc	fsr0h,f
	movf	indf0,w ;code access
	movwf	btemp+11
	movlw	01h
	subwf	(uitoa@s),f
	movf	((uitoa@s)),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	line	172
	
l3251:	
;ProjectSource/dbprintf.c: 172:       i /= base;
	movf	(uitoa@base+1),w
	movwf	(___lwdiv@divisor+1)
	movf	(uitoa@base),w
	movwf	(___lwdiv@divisor)
	movf	(uitoa@i+1),w
	movwf	(___lwdiv@dividend+1)
	movf	(uitoa@i),w
	movwf	(___lwdiv@dividend)
	fcall	___lwdiv
	movlb 0	; select bank0
	movf	(1+(?___lwdiv)),w
	movwf	(uitoa@i+1)
	movf	(0+(?___lwdiv)),w
	movwf	(uitoa@i)
	line	164
	
l3253:	
;ProjectSource/dbprintf.c: 164:    while (i)
	movf	((uitoa@i)),w
iorwf	((uitoa@i+1)),w
	btfss	status,2
	goto	u2611
	goto	u2610
u2611:
	goto	l3241
u2610:
	goto	l3261
	line	176
	
l3255:	
;ProjectSource/dbprintf.c: 175:    {;ProjectSource/dbprintf.c: 176:       (*LineBuffer)[0] = *s++;
	movf	(uitoa@s),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	indf0,w
	movwf	btemp+11
	movf	(uitoa@LineBuffer),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	indf0,w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	movwf	indf0
	
l3257:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(uitoa@s),f
	line	177
	
l3259:	
;ProjectSource/dbprintf.c: 177:       ++(*LineBuffer);
	movlw	low(01h)
	movwf	btemp+11
	movf	(uitoa@LineBuffer),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	btemp+11,w
	addwf	indf0,f
	line	174
	
l3261:	
;ProjectSource/dbprintf.c: 174:    while (*s)
	movf	(uitoa@s),w
	movwf	fsr0l
	clrf fsr0h	
	
	movf	(indf0),w
	btfss	status,2
	goto	u2621
	goto	u2620
u2621:
	goto	l3255
u2620:
	line	179
	
l422:	
	return
	opt callstack 0
GLOBAL	__end_of_uitoa
	__end_of_uitoa:
	signat	_uitoa,12409
	global	___lwmod

;; *************** function ___lwmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lwmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] unsigned int 
;;  dividend        2    2[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  counter         1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_uitoa
;; This function uses a non-reentrant model
;;
psect	text24,local,class=CODE,delta=2,merge=1,group=1
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lwmod.c"
	line	5
global __ptext24
__ptext24:	;psect for function ___lwmod
psect	text24
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lwmod.c"
	line	5
	global	__size_of___lwmod
	__size_of___lwmod	equ	__end_of___lwmod-___lwmod
	
___lwmod:	
;incstack = 0
	opt	callstack 10
; Regs used in ___lwmod: [wreg+status,2+status,0+btemp+11]
	line	12
	
l3099:	
	movlb 0	; select bank0
	movf	((___lwmod@divisor)),w
iorwf	((___lwmod@divisor+1)),w
	btfsc	status,2
	goto	u2361
	goto	u2360
u2361:
	goto	l3117
u2360:
	line	13
	
l3101:	
	clrf	(___lwmod@counter)
	incf	(___lwmod@counter),f
	line	14
	goto	l3107
	line	15
	
l3103:	
	movlw	01h
	
u2375:
	lslf	(___lwmod@divisor),f
	rlf	(___lwmod@divisor+1),f
	decfsz	wreg,f
	goto	u2375
	line	16
	
l3105:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(___lwmod@counter),f
	line	14
	
l3107:	
	btfss	(___lwmod@divisor+1),(15)&7
	goto	u2381
	goto	u2380
u2381:
	goto	l3103
u2380:
	line	19
	
l3109:	
	movf	(___lwmod@divisor+1),w
	subwf	(___lwmod@dividend+1),w
	skipz
	goto	u2395
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),w
u2395:
	skipc
	goto	u2391
	goto	u2390
u2391:
	goto	l3113
u2390:
	line	20
	
l3111:	
	movf	(___lwmod@divisor),w
	subwf	(___lwmod@dividend),f
	movf	(___lwmod@divisor+1),w
	subwfb	(___lwmod@dividend+1),f
	line	21
	
l3113:	
	movlw	01h
	
u2405:
	lsrf	(___lwmod@divisor+1),f
	rrf	(___lwmod@divisor),f
	decfsz	wreg,f
	goto	u2405
	line	22
	
l3115:	
	movlw	01h
	subwf	(___lwmod@counter),f
	btfss	status,2
	goto	u2411
	goto	u2410
u2411:
	goto	l3109
u2410:
	line	24
	
l3117:	
	movf	(___lwmod@dividend+1),w
	movwf	(?___lwmod+1)
	movf	(___lwmod@dividend),w
	movwf	(?___lwmod)
	line	25
	
l890:	
	return
	opt callstack 0
GLOBAL	__end_of___lwmod
	__end_of___lwmod:
	signat	___lwmod,8314
	global	___lwdiv

;; *************** function ___lwdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lwdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] unsigned int 
;;  dividend        2    2[BANK0 ] unsigned int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    5[BANK0 ] unsigned int 
;;  counter         1    4[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] unsigned int 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       7       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        7 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_uitoa
;; This function uses a non-reentrant model
;;
psect	text25,local,class=CODE,delta=2,merge=1,group=1
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lwdiv.c"
	line	5
global __ptext25
__ptext25:	;psect for function ___lwdiv
psect	text25
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\lwdiv.c"
	line	5
	global	__size_of___lwdiv
	__size_of___lwdiv	equ	__end_of___lwdiv-___lwdiv
	
___lwdiv:	
;incstack = 0
	opt	callstack 10
; Regs used in ___lwdiv: [wreg+status,2+status,0+btemp+11]
	line	13
	
l3073:	
	movlb 0	; select bank0
	clrf	(___lwdiv@quotient)
	clrf	(___lwdiv@quotient+1)
	line	14
	
l3075:	
	movf	((___lwdiv@divisor)),w
iorwf	((___lwdiv@divisor+1)),w
	btfsc	status,2
	goto	u2291
	goto	u2290
u2291:
	goto	l3095
u2290:
	line	15
	
l3077:	
	clrf	(___lwdiv@counter)
	incf	(___lwdiv@counter),f
	line	16
	goto	l3083
	line	17
	
l3079:	
	movlw	01h
	
u2305:
	lslf	(___lwdiv@divisor),f
	rlf	(___lwdiv@divisor+1),f
	decfsz	wreg,f
	goto	u2305
	line	18
	
l3081:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(___lwdiv@counter),f
	line	16
	
l3083:	
	btfss	(___lwdiv@divisor+1),(15)&7
	goto	u2311
	goto	u2310
u2311:
	goto	l3079
u2310:
	line	21
	
l3085:	
	movlw	01h
	
u2325:
	lslf	(___lwdiv@quotient),f
	rlf	(___lwdiv@quotient+1),f
	decfsz	wreg,f
	goto	u2325
	line	22
	movf	(___lwdiv@divisor+1),w
	subwf	(___lwdiv@dividend+1),w
	skipz
	goto	u2335
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),w
u2335:
	skipc
	goto	u2331
	goto	u2330
u2331:
	goto	l3091
u2330:
	line	23
	
l3087:	
	movf	(___lwdiv@divisor),w
	subwf	(___lwdiv@dividend),f
	movf	(___lwdiv@divisor+1),w
	subwfb	(___lwdiv@dividend+1),f
	line	24
	
l3089:	
	bsf	(___lwdiv@quotient)+(0/8),(0)&7
	line	26
	
l3091:	
	movlw	01h
	
u2345:
	lsrf	(___lwdiv@divisor+1),f
	rrf	(___lwdiv@divisor),f
	decfsz	wreg,f
	goto	u2345
	line	27
	
l3093:	
	movlw	01h
	subwf	(___lwdiv@counter),f
	btfss	status,2
	goto	u2351
	goto	u2350
u2351:
	goto	l3085
u2350:
	line	29
	
l3095:	
	movf	(___lwdiv@quotient+1),w
	movwf	(?___lwdiv+1)
	movf	(___lwdiv@quotient),w
	movwf	(?___lwdiv)
	line	30
	
l880:	
	return
	opt callstack 0
GLOBAL	__end_of___lwdiv
	__end_of___lwdiv:
	signat	___lwdiv,8314
	global	_putch

;; *************** function _putch *****************
;; Defined at:
;;		line 305 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;  data            1    wreg     unsigned char 
;; Auto vars:     Size  Location     Type
;;  data            1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_DB_printf
;;		_fputc
;; This function uses a non-reentrant model
;;
psect	text26,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Port.c"
	line	305
global __ptext26
__ptext26:	;psect for function _putch
psect	text26
	file	"FrameworkSource/ES_Port.c"
	line	305
	global	__size_of_putch
	__size_of_putch	equ	__end_of_putch-_putch
	
_putch:	
;incstack = 0
	opt	callstack 10
; Regs used in _putch: [wreg]
;putch@data stored from wreg
	movwf	(putch@data)
	line	306
	
l2969:	
	line	307
;FrameworkSource/ES_Port.c: 307:   {}
	
l70:	
	line	306
;FrameworkSource/ES_Port.c: 306:   while( ! TX1IF)
	movlb 14	; select bank14
	btfss	(14460/8)^0700h,(14460)&7	;volatile
	goto	u2161
	goto	u2160
u2161:
	goto	l70
u2160:
	line	308
	
l2971:	
;FrameworkSource/ES_Port.c: 308:   TX1REG = data;
	movf	(putch@data),w
	movlb 2	; select bank2
	movwf	(282)^0100h	;volatile
	line	309
	
l73:	
	return
	opt callstack 0
GLOBAL	__end_of_putch
	__end_of_putch:
	signat	_putch,4217
	global	_BlinkLED

;; *************** function _BlinkLED *****************
;; Defined at:
;;		line 242 in file "ProjectSource/TestHarnessService0.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_RunTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text27,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/TestHarnessService0.c"
	line	242
global __ptext27
__ptext27:	;psect for function _BlinkLED
psect	text27
	file	"ProjectSource/TestHarnessService0.c"
	line	242
	global	__size_of_BlinkLED
	__size_of_BlinkLED	equ	__end_of_BlinkLED-_BlinkLED
	
_BlinkLED:	
;incstack = 0
	opt	callstack 11
; Regs used in _BlinkLED: []
	line	246
	
l371:	
	return
	opt callstack 0
GLOBAL	__end_of_BlinkLED
	__end_of_BlinkLED:
	signat	_BlinkLED,89
	global	_ES_CheckUserEvents

;; *************** function _ES_CheckUserEvents *****************
;; Defined at:
;;		line 47 in file "FrameworkSource/ES_CheckEvents.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  i               1   12[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_Check4Keystroke
;; This function is called by:
;;		_ES_Run
;; This function uses a non-reentrant model
;;
psect	text28,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_CheckEvents.c"
	line	47
global __ptext28
__ptext28:	;psect for function _ES_CheckUserEvents
psect	text28
	file	"FrameworkSource/ES_CheckEvents.c"
	line	47
	global	__size_of_ES_CheckUserEvents
	__size_of_ES_CheckUserEvents	equ	__end_of_ES_CheckUserEvents-_ES_CheckUserEvents
	
_ES_CheckUserEvents:	
;incstack = 0
	opt	callstack 8
; Regs used in _ES_CheckUserEvents: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	51
	
l3507:	
;FrameworkSource/ES_CheckEvents.c: 49:   uint8_t i;;FrameworkSource/ES_CheckEvents.c: 51:   for (i = 0; i < (sizeof(ES_EventList) / sizeof(ES_EventList[0])); i++)
	movlb 0	; select bank0
	clrf	(ES_CheckUserEvents@i)
	line	53
	
l3513:	
;FrameworkSource/ES_CheckEvents.c: 52:   {;FrameworkSource/ES_CheckEvents.c: 53:     if (ES_EventList[i]() == 1)
	movf	(ES_CheckUserEvents@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_ES_EventList)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_ES_EventList)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[1]fsr0
	movwf	pclath
	moviw	[0]fsr0
	callw
	pagesel	$
	xorlw	01h&0ffh
	skipz
	goto	u2831
	goto	u2830
u2831:
	goto	l3517
u2830:
	goto	l162
	line	57
	
l3517:	
;FrameworkSource/ES_CheckEvents.c: 57:   }
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	addwf	(ES_CheckUserEvents@i),f
	
l3519:	
	movf	((ES_CheckUserEvents@i)),w
	btfsc	status,2
	goto	u2841
	goto	u2840
u2841:
	goto	l3513
u2840:
	line	66
	
l162:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_CheckUserEvents
	__end_of_ES_CheckUserEvents:
	signat	_ES_CheckUserEvents,89
	global	_Check4Keystroke

;; *************** function _Check4Keystroke *****************
;; Defined at:
;;		line 109 in file "ProjectSource/EventCheckers.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;  ThisEvent       3    9[BANK0 ] struct ES_Event
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        3 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_ES_PostAll
;;		_getchar
;; This function is called by:
;;		_ES_CheckUserEvents
;; This function uses a non-reentrant model
;;
psect	text29,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/EventCheckers.c"
	line	109
global __ptext29
__ptext29:	;psect for function _Check4Keystroke
psect	text29
	file	"ProjectSource/EventCheckers.c"
	line	109
	global	__size_of_Check4Keystroke
	__size_of_Check4Keystroke	equ	__end_of_Check4Keystroke-_Check4Keystroke
	
_Check4Keystroke:	
;incstack = 0
	opt	callstack 8
; Regs used in _Check4Keystroke: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	111
	
l3459:	
;ProjectSource/EventCheckers.c: 111:   if ((RC1IF))
	movlb 14	; select bank14
	btfss	(14461/8)^0700h,(14461)&7	;volatile
	goto	u2811
	goto	u2810
u2811:
	goto	l3471
u2810:
	line	114
	
l3461:	
;ProjectSource/EventCheckers.c: 112:   {;ProjectSource/EventCheckers.c: 113:     ES_Event_t ThisEvent;;ProjectSource/EventCheckers.c: 114:     ThisEvent.EventType = ES_NEW_KEY;
	movlw	low(05h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(Check4Keystroke@ThisEvent)
	line	115
	
l3463:	
;ProjectSource/EventCheckers.c: 115:     ThisEvent.EventParam = getchar();
	fcall	_getchar
	movlb 0	; select bank0
	movf	(1+(?_getchar)),w
	movwf	1+(Check4Keystroke@ThisEvent)+01h
	movf	(0+(?_getchar)),w
	movwf	0+(Check4Keystroke@ThisEvent)+01h
	line	116
	
l3465:	
;ProjectSource/EventCheckers.c: 116:     ES_PostAll(ThisEvent);
	movf	(Check4Keystroke@ThisEvent),w
	movwf	(ES_PostAll@ThisEvent)
	movf	(Check4Keystroke@ThisEvent+1),w
	movwf	(ES_PostAll@ThisEvent+1)
	movf	(Check4Keystroke@ThisEvent+2),w
	movwf	(ES_PostAll@ThisEvent+2)
	fcall	_ES_PostAll
	line	117
	
l3467:	
;ProjectSource/EventCheckers.c: 117:     return 1;
	movlw	low(01h)
	goto	l312
	line	119
	
l3471:	
;ProjectSource/EventCheckers.c: 119:   return 0;
	movlw	low(0)
	line	120
	
l312:	
	return
	opt callstack 0
GLOBAL	__end_of_Check4Keystroke
	__end_of_Check4Keystroke:
	signat	_Check4Keystroke,89
	global	_getchar

;; *************** function _getchar *****************
;; Defined at:
;;		line 325 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] int 
;; Registers used:
;;		wreg, btemp+10, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_Check4Keystroke
;; This function uses a non-reentrant model
;;
psect	text30,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Port.c"
	line	325
global __ptext30
__ptext30:	;psect for function _getchar
psect	text30
	file	"FrameworkSource/ES_Port.c"
	line	325
	global	__size_of_getchar
	__size_of_getchar	equ	__end_of_getchar-_getchar
	
_getchar:	
;incstack = 0
	opt	callstack 10
; Regs used in _getchar: [wreg+btemp+10+btemp+11]
	line	326
	
l3137:	
	line	327
;FrameworkSource/ES_Port.c: 327:   {}
	
l76:	
	line	326
;FrameworkSource/ES_Port.c: 326:   while( ! RC1IF)
	movlb 14	; select bank14
	btfss	(14461/8)^0700h,(14461)&7	;volatile
	goto	u2461
	goto	u2460
u2461:
	goto	l76
u2460:
	line	328
	
l3139:	
;FrameworkSource/ES_Port.c: 328:   return RC1REG;
	movlb 2	; select bank2
	movf	(281)^0100h,w	;volatile
	movwf	btemp+10
	clrf	btemp+11
	movf	0+wtemp5,w
	movlb 0	; select bank0
	movwf	(?_getchar)
	movf	1+wtemp5,w
	movwf	(?_getchar+1)
	line	329
	
l79:	
	return
	opt callstack 0
GLOBAL	__end_of_getchar
	__end_of_getchar:
	signat	_getchar,90
	global	_ES_PostAll

;; *************** function _ES_PostAll *****************
;; Defined at:
;;		line 355 in file "FrameworkSource/ES_Framework.c"
;; Parameters:    Size  Location     Type
;;  ThisEvent       3    6[BANK0 ] struct ES_Event
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_ES_EnQueueFIFO
;; This function is called by:
;;		_Check4Keystroke
;; This function uses a non-reentrant model
;;
psect	text31,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Framework.c"
	line	355
global __ptext31
__ptext31:	;psect for function _ES_PostAll
psect	text31
	file	"FrameworkSource/ES_Framework.c"
	line	355
	global	__size_of_ES_PostAll
	__size_of_ES_PostAll	equ	__end_of_ES_PostAll-_ES_PostAll
	
_ES_PostAll:	
;incstack = 0
	opt	callstack 8
; Regs used in _ES_PostAll: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	359
	
l3201:	
;FrameworkSource/ES_Framework.c: 355: _Bool ES_PostAll(ES_Event_t ThisEvent);FrameworkSource/ES_Framework.c: 356: {;FrameworkSource/ES_Framework.c: 357:   uint8_t i;;FrameworkSource/ES_Framework.c: 359:   for (i = 0; i < (sizeof(EventQueues) / sizeof(EventQueues[0])); i++)
	clrf	(ES_PostAll@i)
	line	361
	
l3207:	
;FrameworkSource/ES_Framework.c: 360:   {;FrameworkSource/ES_Framework.c: 361:     if (ES_EnQueueFIFO(EventQueues[i].pMem, ThisEvent) != 1)
	movlb 0	; select bank0
	movf	(ES_PostAll@ThisEvent),w
	pushw
	movf	(ES_PostAll@ThisEvent+1),w
	pushw
	movf	(ES_PostAll@ThisEvent+2),w
	pushw
	movf	(ES_PostAll@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_EventQueues)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_EventQueues)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	movf	indf0,w ;code access
	fcall	_ES_EnQueueFIFO
	xorlw	01h&0ffh
	skipnz
	goto	u2561
	goto	u2560
u2561:
	goto	l3213
u2560:
	goto	l235
	line	367
	
l3213:	
;FrameworkSource/ES_Framework.c: 366:     {;FrameworkSource/ES_Framework.c: 367:       Ready |= BitNum2SetMask[i];
	movf	(ES_PostAll@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	movf	0+wtemp5,w
	movlb 0	; select bank0
	iorwf	(_Ready),f
	movf	1+wtemp5,w
	iorwf	(_Ready+1),f
	line	369
	
l3215:	
;FrameworkSource/ES_Framework.c: 369:   }
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(ES_PostAll@i),f
	
l3217:	
	movf	((ES_PostAll@i)),w
	btfsc	status,2
	goto	u2571
	goto	u2570
u2571:
	goto	l3207
u2570:
	line	378
	
l235:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_PostAll
	__end_of_ES_PostAll:
	signat	_ES_PostAll,4217
	global	_ES_Initialize

;; *************** function _ES_Initialize *****************
;; Defined at:
;;		line 256 in file "FrameworkSource/ES_Framework.c"
;; Parameters:    Size  Location     Type
;;  NewRate         2   10[BANK0 ] enum E7645
;; Auto vars:     Size  Location     Type
;;  i               1    1[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      enum E7707
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    6
;; This function calls:
;;		_ES_InitQueue
;;		_ES_Timer_Init
;;		_InitTestHarnessService0
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text32,local,class=CODE,delta=2,merge=1,group=0
	line	256
global __ptext32
__ptext32:	;psect for function _ES_Initialize
psect	text32
	file	"FrameworkSource/ES_Framework.c"
	line	256
	global	__size_of_ES_Initialize
	__size_of_ES_Initialize	equ	__end_of_ES_Initialize-_ES_Initialize
	
_ES_Initialize:	
;incstack = 0
	opt	callstack 9
; Regs used in _ES_Initialize: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	259
	
l3609:	
;FrameworkSource/ES_Framework.c: 256: ES_Return_t ES_Initialize(TimerRate_t NewRate);FrameworkSource/ES_Framework.c: 257: {;FrameworkSource/ES_Framework.c: 258:   uint8_t i;;FrameworkSource/ES_Framework.c: 259:   ES_Timer_Init(NewRate);
	movlb 0	; select bank0
	movf	(ES_Initialize@NewRate+1),w
	movwf	(ES_Timer_Init@Rate+1)
	movf	(ES_Initialize@NewRate),w
	movwf	(ES_Timer_Init@Rate)
	fcall	_ES_Timer_Init
	line	261
	
l3611:	
;FrameworkSource/ES_Framework.c: 261:   for (i = 0; i < (sizeof(ServDescList) / sizeof(ServDescList[0])); i++)
	clrf	(ES_Initialize@i)
	line	263
	
l3617:	
;FrameworkSource/ES_Framework.c: 262:   {;FrameworkSource/ES_Framework.c: 263:     if ((ServDescList[i].InitFunc == (pInitFunc)0) ||
	movf	(ES_Initialize@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_ServDescList)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_ServDescList)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	fsr0++
	movlb 0	; select bank0
movwf	(??_ES_Initialize+0)+0
moviw	fsr0++
iorwf	(??_ES_Initialize+0)+0
	btfsc	status,2
	goto	u2921
	goto	u2920
u2921:
	goto	l3621
u2920:
	
l3619:	
	movf	(ES_Initialize@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_ServDescList+02h)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_ServDescList+02h)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	fsr0++
movwf	(??_ES_Initialize+0)+0
moviw	fsr0++
iorwf	(??_ES_Initialize+0)+0
	btfss	status,2
	goto	u2931
	goto	u2930
u2931:
	goto	l3625
u2930:
	line	266
	
l3621:	
;FrameworkSource/ES_Framework.c: 265:     {;FrameworkSource/ES_Framework.c: 266:       return FailedPointer;
	movlw	low(03h)
	goto	l211
	line	269
	
l3625:	
;FrameworkSource/ES_Framework.c: 269:     ES_InitQueue(EventQueues[i].pMem, EventQueues[i].Size);
	movf	(ES_Initialize@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_EventQueues)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_EventQueues)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
		moviw	[0]fsr0
	movwf	(ES_InitQueue@pBlock)
	movlw	0x1
	movwf	(ES_InitQueue@pBlock+1)

	movf	(ES_Initialize@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_EventQueues+01h)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_EventQueues+01h)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	movf	indf0,w ;code access
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_InitQueue@BlockSize)
	fcall	_ES_InitQueue
	line	271
	
l3627:	
;FrameworkSource/ES_Framework.c: 271:     if (ServDescList[i].InitFunc(i) != 1)
	movf	(ES_Initialize@i),w
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(InitTestHarnessService0@Priority)
	movf	(ES_Initialize@i),w
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_ServDescList)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_ServDescList)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[1]fsr0
	movwf	pclath
	moviw	[0]fsr0
	callw
	pagesel	$
	xorlw	01h&0ffh
	skipnz
	goto	u2941
	goto	u2940
u2941:
	goto	l3633
u2940:
	line	273
	
l3629:	
;FrameworkSource/ES_Framework.c: 272:     {;FrameworkSource/ES_Framework.c: 273:       return FailedInit;
	movlw	low(05h)
	goto	l211
	line	275
	
l3633:	
;FrameworkSource/ES_Framework.c: 275:   }
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(ES_Initialize@i),f
	
l3635:	
	movf	((ES_Initialize@i)),w
	btfsc	status,2
	goto	u2951
	goto	u2950
u2951:
	goto	l3617
u2950:
	line	279
	
l3637:	
;FrameworkSource/ES_Framework.c: 279:   return Success;
	movlw	low(0)
	line	280
	
l211:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_Initialize
	__end_of_ES_Initialize:
	signat	_ES_Initialize,4217
	global	_InitTestHarnessService0

;; *************** function _InitTestHarnessService0 *****************
;; Defined at:
;;		line 91 in file "ProjectSource/TestHarnessService0.c"
;; Parameters:    Size  Location     Type
;;  Priority        1    6[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  ThisEvent       3    7[BANK0 ] struct ES_Event
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        4 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    5
;; This function calls:
;;		_ES_InitQueue
;;		_ES_PostToService
;;		_InitLED
;; This function is called by:
;;		_ES_Initialize
;; This function uses a non-reentrant model
;;
psect	text33,local,class=CODE,delta=2,merge=1,group=0
	file	"ProjectSource/TestHarnessService0.c"
	line	91
global __ptext33
__ptext33:	;psect for function _InitTestHarnessService0
psect	text33
	file	"ProjectSource/TestHarnessService0.c"
	line	91
	global	__size_of_InitTestHarnessService0
	__size_of_InitTestHarnessService0	equ	__end_of_InitTestHarnessService0-_InitTestHarnessService0
	
_InitTestHarnessService0:	
;incstack = 0
	opt	callstack 9
; Regs used in _InitTestHarnessService0: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
	line	95
	
l3527:	
;ProjectSource/TestHarnessService0.c: 91: _Bool InitTestHarnessService0(uint8_t Priority);ProjectSource/TestHarnessService0.c: 92: {;ProjectSource/TestHarnessService0.c: 93:   ES_Event_t ThisEvent;;ProjectSource/TestHarnessService0.c: 95:   MyPriority = Priority;
	movlb 0	; select bank0
	movf	(InitTestHarnessService0@Priority),w
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(_MyPriority)
	line	100
	
l3529:	
;ProjectSource/TestHarnessService0.c: 100:   ES_InitQueue(DeferralQueue, (sizeof(DeferralQueue) / sizeof(DeferralQueue[0])));
	movlw	low(_DeferralQueue)
	movwf	(ES_InitQueue@pBlock)
	movlw	high(_DeferralQueue)
	movwf	(ES_InitQueue@pBlock+1)
	movlw	low(04h)
	movwf	btemp+11
	movf	btemp+11,w
	movwf	(ES_InitQueue@BlockSize)
	fcall	_ES_InitQueue
	line	102
	
l3531:	
;ProjectSource/TestHarnessService0.c: 102:   InitLED();
	fcall	_InitLED
	line	107
	
l3533:	
;ProjectSource/TestHarnessService0.c: 107:   ThisEvent.EventType = ES_INIT;
	movlw	low(02h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	movwf	(InitTestHarnessService0@ThisEvent)
	line	108
	
l3535:	
;ProjectSource/TestHarnessService0.c: 108:   if (ES_PostToService(MyPriority, ThisEvent) == 1)
	movf	(InitTestHarnessService0@ThisEvent),w
	pushw
	movf	(InitTestHarnessService0@ThisEvent+1),w
	pushw
	movf	(InitTestHarnessService0@ThisEvent+2),w
	pushw
	movf	(_MyPriority),w
	fcall	_ES_PostToService
	xorlw	01h&0ffh
	skipz
	goto	u2851
	goto	u2850
u2851:
	goto	l3543
u2850:
	line	110
	
l3537:	
;ProjectSource/TestHarnessService0.c: 109:   {;ProjectSource/TestHarnessService0.c: 110:     return 1;
	movlw	low(01h)
	goto	l342
	line	114
	
l3543:	
;ProjectSource/TestHarnessService0.c: 113:   {;ProjectSource/TestHarnessService0.c: 114:     return 0;
	movlw	low(0)
	line	116
	
l342:	
	return
	opt callstack 0
GLOBAL	__end_of_InitTestHarnessService0
	__end_of_InitTestHarnessService0:
	signat	_InitTestHarnessService0,4217
	global	_InitLED

;; *************** function _InitLED *****************
;; Defined at:
;;		line 238 in file "ProjectSource/TestHarnessService0.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		None
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_InitTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text34,local,class=CODE,delta=2,merge=1,group=0
	line	238
global __ptext34
__ptext34:	;psect for function _InitLED
psect	text34
	file	"ProjectSource/TestHarnessService0.c"
	line	238
	global	__size_of_InitLED
	__size_of_InitLED	equ	__end_of_InitLED-_InitLED
	
_InitLED:	
;incstack = 0
	opt	callstack 11
; Regs used in _InitLED: []
	line	240
	
l368:	
	return
	opt callstack 0
GLOBAL	__end_of_InitLED
	__end_of_InitLED:
	signat	_InitLED,89
	global	_ES_PostToService

;; *************** function _ES_PostToService *****************
;; Defined at:
;;		line 395 in file "FrameworkSource/ES_Framework.c"
;; Parameters:    Size  Location     Type
;;  WhichService    1    wreg     unsigned char 
;;  TheEvent        3  [STACK] struct ES_Event
;; Auto vars:     Size  Location     Type
;;  WhichService    1  [STACK] unsigned char 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 3F/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    4
;; This function calls:
;;		_ES_EnQueueFIFO
;; This function is called by:
;;		_InitTestHarnessService0
;;		_PostTestHarnessService0
;;		_RunTestHarnessService0
;; This function uses a reentrant model
;;
psect	text35,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Framework.c"
	line	395
global __ptext35
__ptext35:	;psect for function _ES_PostToService
psect	text35
	file	"FrameworkSource/ES_Framework.c"
	line	395
	global	__size_of_ES_PostToService
	__size_of_ES_PostToService	equ	__end_of_ES_PostToService-_ES_PostToService
	
_ES_PostToService:	
; autosize = 1, savesize = 0, parsavesize = 0, tempsize = 0 parsize = 3, argsize = 3 vargsize = 0, retsize = 0 argfudge = 0
	addfsr fsr1,1
;incstack = 0
	opt	callstack 7
; Regs used in _ES_PostToService: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
;_WhichService stored from wreg
	;stkvar	_WhichService @ sp[(-1)+0]
	movwi	[((-1)+0)+0]fsr1
	line	397
	
l3011:	
;FrameworkSource/ES_Framework.c: 395: __attribute__((reentrant)) _Bool ES_PostToService(uint8_t WhichService, ES_Event_t TheEvent);FrameworkSource/ES_Framework.c: 396: {;FrameworkSource/ES_Framework.c: 397:   if ((WhichService < (sizeof(EventQueues) / sizeof(EventQueues[0]))) &&
	;stkvar	_WhichService @ sp[(-1)+0]
	moviw	[((-1)+0)+0]fsr1
	btfss	status,2
	goto	u2221
	goto	u2220
u2221:
	goto	l3023
u2220:
	
l3013:	
	;stkvar	_TheEvent @ sp[(-1)+-3]
	moviw	[0+((-1)+-3)]fsr1
	pushw
	moviw	[1+((-2)+-3)]fsr1
	pushw
	moviw	[2+((-3)+-3)]fsr1
	pushw
	;stkvar	_WhichService @ sp[(-4)+0]
	moviw	[((-4)+0)]fsr1
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_EventQueues)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_EventQueues)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	movf	indf0,w ;code access
	fcall	_ES_EnQueueFIFO
	xorlw	01h&0ffh
	skipz
	goto	u2231
	goto	u2230
u2231:
	goto	l3023
u2230:
	line	401
	
l3015:	
;FrameworkSource/ES_Framework.c: 400:   {;FrameworkSource/ES_Framework.c: 401:     Ready |= BitNum2SetMask[WhichService];
	;stkvar	_WhichService @ sp[(-1)+0]
	moviw	[((-1)+0)]fsr1
	movwf	btemp+10
	clrf	btemp+11
	lslf	btemp+10,f
	rlf	btemp+11,f
	movlw	low(((_BitNum2SetMask)|8000h))
	addwf	0+wtemp5,w
	movwf	fsr0l
	movlw	high(((_BitNum2SetMask)|8000h))
	addwfc	1+wtemp5,w
	movwf	fsr0h
	moviw	[0]fsr0
	movwf	btemp+10
	moviw	[1]fsr0
	movwf	btemp+11
	movf	0+wtemp5,w
	movlb 0	; select bank0
	iorwf	(_Ready),f
	movf	1+wtemp5,w
	iorwf	(_Ready+1),f
	line	402
	
l3017:	
;FrameworkSource/ES_Framework.c: 402:     return 1;
	movlw	low(01h)
	goto	l240
	line	406
	
l3023:	
;FrameworkSource/ES_Framework.c: 405:   {;FrameworkSource/ES_Framework.c: 406:     return 0;
	movlw	low(0)
	line	408
	
l240:	
; _ES_PostToService: autosize = 1, savesize = 0, parsavesize = 0, tempsize = 0 parsize = 3, argsize = 3, vargsize = 0
	addfsr fsr1,-4
	return
	opt callstack 0
GLOBAL	__end_of_ES_PostToService
	__end_of_ES_PostToService:
	signat	_ES_PostToService,8313
	global	_ES_EnQueueFIFO

;; *************** function _ES_EnQueueFIFO *****************
;; Defined at:
;;		line 85 in file "FrameworkSource/ES_Queue.c"
;; Parameters:    Size  Location     Type
;;  pBlock          1    wreg     PTR struct ES_Event
;;		 -> Queue0(18), 
;;  Event2Add       3  [STACK] struct ES_Event
;; Auto vars:     Size  Location     Type
;;  pBlock          1  [STACK] PTR struct ES_Event
;;		 -> Queue0(18), 
;;  pThisQueue      1  [STACK] PTR struct .
;;		 -> Queue0(18), 
;; Return value:  Size  Location     Type
;;                  1    wreg      _Bool 
;; Registers used:
;;		wreg, fsr0l, fsr0h, fsr1l, fsr1h, status,2, status,0, btemp+0, btemp+1, btemp+10, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		___awmod
;; This function is called by:
;;		_ES_PostAll
;;		_ES_PostToService
;; This function uses a reentrant model
;;
psect	text36,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Queue.c"
	line	85
global __ptext36
__ptext36:	;psect for function _ES_EnQueueFIFO
psect	text36
	file	"FrameworkSource/ES_Queue.c"
	line	85
	global	__size_of_ES_EnQueueFIFO
	__size_of_ES_EnQueueFIFO	equ	__end_of_ES_EnQueueFIFO-_ES_EnQueueFIFO
	
_ES_EnQueueFIFO:	
; autosize = 2, savesize = 0, parsavesize = 0, tempsize = 0 parsize = 3, argsize = 3 vargsize = 0, retsize = 0 argfudge = 0
	addfsr fsr1,2
;incstack = 0
	opt	callstack 7
; Regs used in _ES_EnQueueFIFO: [wreg-fsr0h+fsr1l+fsr1h+status,2-btemp+1+btemp+10+btemp+11+pclath+cstack]
;_pBlock stored from wreg
	;stkvar	_pBlock @ sp[(-2)+0]
	movwi	[((-2)+0)+0]fsr1
	line	88
	
l2973:	
;FrameworkSource/ES_Queue.c: 85: __attribute__((reentrant)) _Bool ES_EnQueueFIFO(ES_Event_t *pBlock, ES_Event_t Event2Add);FrameworkSource/ES_Queue.c: 86: {;FrameworkSource/ES_Queue.c: 87:   pQueue_t pThisQueue;;FrameworkSource/ES_Queue.c: 88:   pThisQueue = (pQueue_t)pBlock;
	;stkvar	_pBlock @ sp[(-2)+0]
	moviw	[((-2)+0)]fsr1
	;stkvar	_pThisQueue @ sp[(-2)+1]
	movwi	[((-2)+1)+0]fsr1
	line	90
	
l2975:	
;FrameworkSource/ES_Queue.c: 90:   if (pThisQueue->NumEntries < pThisQueue->QueueSize)
	;stkvar	_pThisQueue @ sp[(-2)+1]
	moviw	[((-2)+1)]fsr1
	movwf	fsr0l
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movf	indf0,w
	movwf	btemp+10
	;stkvar	_pThisQueue @ sp[(-2)+1]
	moviw	[((-2)+1)]fsr1
	movwf	btemp+11
	movf	btemp+11,w
	addlw	02h
	movwf	fsr0l
	movlw 1	; select bank2/3
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movf	indf0,w
	movwf	btemp+0
	movf	btemp+10,w
	subwf	btemp+0,w
	skipnc
	goto	u2171
	goto	u2170
u2171:
	goto	l2993
u2170:
	line	93
	
l2977:	
;FrameworkSource/ES_Queue.c: 91:   {;FrameworkSource/ES_Queue.c: 93:     { _INTCON_temp = INTCON; GIE=0;};
	movf	(11),w	;volatile
	movlb 0	; select bank0
	movwf	(__INTCON_temp)
	
l2979:	
	bcf	(95/8),(95)&7	;volatile
	line	94
	
l2981:	
;FrameworkSource/ES_Queue.c: 94:     pBlock[1 + ((pThisQueue->CurrentIndex + pThisQueue->NumEntries)
	;stkvar	_Event2Add @ sp[(-2)+-3]
	;stkvar	_pThisQueue @ sp[(-2)+1]
	moviw	[((-2)+1)]fsr1
	movwf	fsr0l
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movf	indf0,w
	movwf	(___awmod@divisor)
	clrf	(___awmod@divisor+1)
	;stkvar	_pThisQueue @ sp[(-2)+1]
	moviw	[((-2)+1)]fsr1
	movwf	btemp+11
	movf	btemp+11,w
	addlw	02h
	movwf	fsr0l
	movlw 1	; select bank2/3
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movf	indf0,w
	movwf	btemp+10
	;stkvar	_pThisQueue @ sp[(-2)+1]
	moviw	[((-2)+1)]fsr1
	movwf	btemp+11
	incf	btemp+11,w
	movwf	fsr0l
	movlw 1	; select bank2/3
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	movf	indf0,w
	addwf	btemp+10,w
	movwf	(___awmod@dividend)
	clrf	(___awmod@dividend)+1
	rlf	1+(___awmod@dividend),f
	
	fcall	___awmod
	movlb 0	; select bank0
	movf	(?___awmod+0),w
	movwf	btemp+0
	movf	(?___awmod+1),w
	movwf	btemp+1
	movf	0+btemp+0,w
	addwf	0+btemp+0,w
	addwf	0+btemp+0,w
	movwf	btemp+11
	;stkvar	_pBlock @ sp[(-2)+0]
	moviw	[((-2)+0)+0]fsr1
	movwf	btemp+10
	movf	btemp+11,w
	addwf	btemp+10,f
	movf	0+btemp+10,w
	addlw	03h
	movwf	fsr0l
	movlw 1	; select bank2/3
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	moviw	[0+((-2)+-3)]fsr1
	movwi	[0]fsr0
	moviw	[1+((-2)+-3)]fsr1
	movwi	[1]fsr0
	moviw	[2+((-2)+-3)]fsr1
	movwi	[2]fsr0
	line	96
	
l2983:	
;FrameworkSource/ES_Queue.c: 96:     pThisQueue->NumEntries++;
	;stkvar	_pThisQueue @ sp[(-2)+1]
	moviw	[((-2)+1)]fsr1
	movwf	btemp+11
	movf	btemp+11,w
	addlw	02h
	movwf	fsr0l
	movlw 1	; select bank2/3
	movlw 1	; select bank2/3
	movwf fsr0h	
	
	incf	indf0,f
	line	97
	
l2985:	
;FrameworkSource/ES_Queue.c: 97:     { INTCON = _INTCON_temp; };
	movf	(__INTCON_temp),w
	movwf	(11)	;volatile
	line	99
	
l2987:	
;FrameworkSource/ES_Queue.c: 99:     return 1;
	movlw	low(01h)
	goto	l259
	line	103
	
l2993:	
;FrameworkSource/ES_Queue.c: 102:   {;FrameworkSource/ES_Queue.c: 103:     return 0;
	movlw	low(0)
	line	105
	
l259:	
; _ES_EnQueueFIFO: autosize = 2, savesize = 0, parsavesize = 0, tempsize = 0 parsize = 3, argsize = 3, vargsize = 0
	addfsr fsr1,-5
	return
	opt callstack 0
GLOBAL	__end_of_ES_EnQueueFIFO
	__end_of_ES_EnQueueFIFO:
	signat	_ES_EnQueueFIFO,8313
	global	___awmod

;; *************** function ___awmod *****************
;; Defined at:
;;		line 5 in file "C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\awmod.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[BANK0 ] int 
;;  dividend        2    2[BANK0 ] int 
;; Auto vars:     Size  Location     Type
;;  sign            1    5[BANK0 ] unsigned char 
;;  counter         1    4[BANK0 ] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[BANK0 ] int 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       4       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       6       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        6 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ES_EnQueueFIFO
;; This function uses a non-reentrant model
;;
psect	text37,local,class=CODE,delta=2,merge=1,group=1
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\awmod.c"
	line	5
global __ptext37
__ptext37:	;psect for function ___awmod
psect	text37
	file	"C:\Program Files (x86)\Microchip\xc8\v2.10\pic\sources\c99\common\awmod.c"
	line	5
	global	__size_of___awmod
	__size_of___awmod	equ	__end_of___awmod-___awmod
	
___awmod:	
;incstack = 0
	opt	callstack 7
; Regs used in ___awmod: [wreg+status,2+status,0+btemp+11]
	line	12
	
l2931:	
	movlb 0	; select bank0
	clrf	(___awmod@sign)
	line	13
	
l2933:	
	btfss	(___awmod@dividend+1),7
	goto	u2071
	goto	u2070
u2071:
	goto	l2939
u2070:
	line	14
	
l2935:	
	comf	(___awmod@dividend),f
	comf	(___awmod@dividend+1),f
	incf	(___awmod@dividend),f
	skipnz
	incf	(___awmod@dividend+1),f
	line	15
	
l2937:	
	clrf	(___awmod@sign)
	incf	(___awmod@sign),f
	line	17
	
l2939:	
	btfss	(___awmod@divisor+1),7
	goto	u2081
	goto	u2080
u2081:
	goto	l2943
u2080:
	line	18
	
l2941:	
	comf	(___awmod@divisor),f
	comf	(___awmod@divisor+1),f
	incf	(___awmod@divisor),f
	skipnz
	incf	(___awmod@divisor+1),f
	line	19
	
l2943:	
	movf	((___awmod@divisor)),w
iorwf	((___awmod@divisor+1)),w
	btfsc	status,2
	goto	u2091
	goto	u2090
u2091:
	goto	l2961
u2090:
	line	20
	
l2945:	
	clrf	(___awmod@counter)
	incf	(___awmod@counter),f
	line	21
	goto	l2951
	line	22
	
l2947:	
	movlw	01h
	
u2105:
	lslf	(___awmod@divisor),f
	rlf	(___awmod@divisor+1),f
	decfsz	wreg,f
	goto	u2105
	line	23
	
l2949:	
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	addwf	(___awmod@counter),f
	line	21
	
l2951:	
	btfss	(___awmod@divisor+1),(15)&7
	goto	u2111
	goto	u2110
u2111:
	goto	l2947
u2110:
	line	26
	
l2953:	
	movf	(___awmod@divisor+1),w
	subwf	(___awmod@dividend+1),w
	skipz
	goto	u2125
	movf	(___awmod@divisor),w
	subwf	(___awmod@dividend),w
u2125:
	skipc
	goto	u2121
	goto	u2120
u2121:
	goto	l2957
u2120:
	line	27
	
l2955:	
	movf	(___awmod@divisor),w
	subwf	(___awmod@dividend),f
	movf	(___awmod@divisor+1),w
	subwfb	(___awmod@dividend+1),f
	line	28
	
l2957:	
	movlw	01h
	
u2135:
	lsrf	(___awmod@divisor+1),f
	rrf	(___awmod@divisor),f
	decfsz	wreg,f
	goto	u2135
	line	29
	
l2959:	
	movlw	01h
	subwf	(___awmod@counter),f
	btfss	status,2
	goto	u2141
	goto	u2140
u2141:
	goto	l2953
u2140:
	line	31
	
l2961:	
	movf	((___awmod@sign)),w
	btfsc	status,2
	goto	u2151
	goto	u2150
u2151:
	goto	l2965
u2150:
	line	32
	
l2963:	
	comf	(___awmod@dividend),f
	comf	(___awmod@dividend+1),f
	incf	(___awmod@dividend),f
	skipnz
	incf	(___awmod@dividend+1),f
	line	33
	
l2965:	
	movf	(___awmod@dividend+1),w
	movwf	(?___awmod+1)
	movf	(___awmod@dividend),w
	movwf	(?___awmod)
	line	34
	
l766:	
	return
	opt callstack 0
GLOBAL	__end_of___awmod
	__end_of___awmod:
	signat	___awmod,8314
	global	_ES_InitQueue

;; *************** function _ES_InitQueue *****************
;; Defined at:
;;		line 58 in file "FrameworkSource/ES_Queue.c"
;; Parameters:    Size  Location     Type
;;  pBlock          2    0[BANK0 ] PTR struct ES_Event
;;		 -> DeferralQueue(12), Queue0(18), 
;;  BlockSize       1    2[BANK0 ] unsigned char 
;; Auto vars:     Size  Location     Type
;;  pThisQueue      2    3[BANK0 ] PTR struct .
;;		 -> DeferralQueue(12), Queue0(18), 
;; Return value:  Size  Location     Type
;;                  1    wreg      unsigned char 
;; Registers used:
;;		wreg, fsr0l, fsr0h, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       3       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       5       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        5 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ES_Initialize
;;		_InitTestHarnessService0
;; This function uses a non-reentrant model
;;
psect	text38,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Queue.c"
	line	58
global __ptext38
__ptext38:	;psect for function _ES_InitQueue
psect	text38
	file	"FrameworkSource/ES_Queue.c"
	line	58
	global	__size_of_ES_InitQueue
	__size_of_ES_InitQueue	equ	__end_of_ES_InitQueue-_ES_InitQueue
	
_ES_InitQueue:	
;incstack = 0
	opt	callstack 11
; Regs used in _ES_InitQueue: [wreg-fsr0h+status,2+status,0+btemp+11]
	line	62
	
l3337:	
;FrameworkSource/ES_Queue.c: 58: uint8_t ES_InitQueue(ES_Event_t *pBlock, uint8_t BlockSize);FrameworkSource/ES_Queue.c: 59: {;FrameworkSource/ES_Queue.c: 60:   pQueue_t pThisQueue;;FrameworkSource/ES_Queue.c: 62:   pThisQueue = (pQueue_t)pBlock;
	movlb 0	; select bank0
	movf	(ES_InitQueue@pBlock+1),w
	movwf	(ES_InitQueue@pThisQueue+1)
	movf	(ES_InitQueue@pBlock),w
	movwf	(ES_InitQueue@pThisQueue)
	line	64
	
l3339:	
;FrameworkSource/ES_Queue.c: 64:   pThisQueue->QueueSize = BlockSize - 1;
	movf	(ES_InitQueue@BlockSize),w
	addlw	0FFh
	movwf	btemp+11
	movf	(ES_InitQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_InitQueue@pThisQueue+1),w
	movwf	fsr0h
	movf	btemp+11,w
	movwf	indf0
	line	65
	
l3341:	
;FrameworkSource/ES_Queue.c: 65:   pThisQueue->CurrentIndex = 0;
	movf	(ES_InitQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_InitQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,01h
	clrf	indf0
	line	66
	
l3343:	
;FrameworkSource/ES_Queue.c: 66:   pThisQueue->NumEntries = 0;
	movf	(ES_InitQueue@pThisQueue),w
	movwf	fsr0l
	movf	(ES_InitQueue@pThisQueue+1),w
	movwf	fsr0h
	addfsr	fsr0,02h
	clrf	indf0
	line	68
	
l255:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_InitQueue
	__end_of_ES_InitQueue:
	signat	_ES_InitQueue,8313
	global	_ES_Timer_Init

;; *************** function _ES_Timer_Init *****************
;; Defined at:
;;		line 131 in file "FrameworkSource/ES_Timers.c"
;; Parameters:    Size  Location     Type
;;  Rate            2    2[BANK0 ] enum E7645
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, status,0, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    3
;; This function calls:
;;		__HW_Timer_Init
;; This function is called by:
;;		_ES_Initialize
;; This function uses a non-reentrant model
;;
psect	text39,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Timers.c"
	line	131
global __ptext39
__ptext39:	;psect for function _ES_Timer_Init
psect	text39
	file	"FrameworkSource/ES_Timers.c"
	line	131
	global	__size_of_ES_Timer_Init
	__size_of_ES_Timer_Init	equ	__end_of_ES_Timer_Init-_ES_Timer_Init
	
_ES_Timer_Init:	
;incstack = 0
	opt	callstack 11
; Regs used in _ES_Timer_Init: [wreg+status,2+status,0+pclath+cstack]
	line	134
	
l3505:	
;FrameworkSource/ES_Timers.c: 131: void ES_Timer_Init(TimerRate_t Rate);FrameworkSource/ES_Timers.c: 132: {;FrameworkSource/ES_Timers.c: 134:   _HW_Timer_Init(Rate);
	movlb 0	; select bank0
	movf	(ES_Timer_Init@Rate+1),w
	movwf	(__HW_Timer_Init@Rate+1)
	movf	(ES_Timer_Init@Rate),w
	movwf	(__HW_Timer_Init@Rate)
	fcall	__HW_Timer_Init
	line	135
	
l103:	
	return
	opt callstack 0
GLOBAL	__end_of_ES_Timer_Init
	__end_of_ES_Timer_Init:
	signat	_ES_Timer_Init,4217
	global	__HW_Timer_Init

;; *************** function __HW_Timer_Init *****************
;; Defined at:
;;		line 148 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;  Rate            2    0[BANK0 ] const enum E7635
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       2       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    2
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_ES_Timer_Init
;; This function uses a non-reentrant model
;;
psect	text40,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Port.c"
	line	148
global __ptext40
__ptext40:	;psect for function __HW_Timer_Init
psect	text40
	file	"FrameworkSource/ES_Port.c"
	line	148
	global	__size_of__HW_Timer_Init
	__size_of__HW_Timer_Init	equ	__end_of__HW_Timer_Init-__HW_Timer_Init
	
__HW_Timer_Init:	
;incstack = 0
	opt	callstack 11
; Regs used in __HW_Timer_Init: [wreg+status,2]
	line	151
	
l3275:	
;FrameworkSource/ES_Port.c: 148: void _HW_Timer_Init(const TimerRate_t Rate);FrameworkSource/ES_Port.c: 149: {;FrameworkSource/ES_Port.c: 151:   NCO1CLK = ((uint8_t)((0x0100UL & 0x0000000FUL) ? 1 : 0) + ((0x0100UL & 0x000000F0UL) ? 2 : 0) + ((0x0100UL & 0x00000F00UL) ? 4 : 0) + ((0x0100UL & 0x0000F000UL) ? 8 : 0) + ((0x0100UL & 0x000F0000UL) ? 16 : 0) + ((0x0100UL & 0x00F00000UL) ? 32 : 0) + ((0x0100UL & 0x0F000000UL) ? 64 : 0) + ((0x0100UL & 0xF0000000UL) ? 128 : 0));
	movlw	low(04h)
	movlb 11	; select bank11
	movwf	(1427)^0580h	;volatile
	line	154
	
l3277:	
;FrameworkSource/ES_Port.c: 154:   NCO1INCU = 0;
	clrf	(1425)^0580h	;volatile
	line	155
;FrameworkSource/ES_Port.c: 155:   NCO1INCH = Rate>>8;
	movlb 0	; select bank0
	movf	(__HW_Timer_Init@Rate+1),w
	movlb 11	; select bank11
	movwf	(1424)^0580h	;volatile
	line	156
	
l3279:	
;FrameworkSource/ES_Port.c: 156:   NCO1INCL = Rate;
	movlb 0	; select bank0
	movf	(__HW_Timer_Init@Rate),w
	movlb 11	; select bank11
	movwf	(1423)^0580h	;volatile
	line	159
	
l3281:	
;FrameworkSource/ES_Port.c: 159:   N1EN = 1;
	bsf	(11415/8)^0580h,(11415)&7	;volatile
	line	162
	
l3283:	
;FrameworkSource/ES_Port.c: 162:   NCO1IE = 1;
	movlb 14	; select bank14
	bsf	(14572/8)^0700h,(14572)&7	;volatile
	line	163
	
l3285:	
;FrameworkSource/ES_Port.c: 163:   PEIE = 1;
	bsf	(94/8),(94)&7	;volatile
	line	164
	
l3287:	
;FrameworkSource/ES_Port.c: 164:   GIE = 1;
	bsf	(95/8),(95)&7	;volatile
	line	166
	
l52:	
	return
	opt callstack 0
GLOBAL	__end_of__HW_Timer_Init
	__end_of__HW_Timer_Init:
	signat	__HW_Timer_Init,4217
	global	_myIsr

;; *************** function _myIsr *****************
;; Defined at:
;;		line 19 in file "ProjectSource/ISR.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11, pclath, cstack
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         1       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        1 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		__HW_SysTickIntHandler
;; This function is called by:
;;		Interrupt level 1
;; This function uses a non-reentrant model
;;
psect	intentry,class=CODE,delta=2
global __pintentry
__pintentry:
psect	intentry
	file	"ProjectSource/ISR.c"
	line	19
	global	__size_of_myIsr
	__size_of_myIsr	equ	__end_of_myIsr-_myIsr
	
_myIsr:	
;incstack = 0
	opt	callstack 7
; Regs used in _myIsr: [wreg+status,2+status,0+btemp+11+pclath+cstack]
psect	intentry
	pagesel	$
	movf	btemp+11,w
	movwf	(??_myIsr+0)
	global ___int_sp
	movlw low(___int_sp)
	movwf fsr1l
	movlw high(___int_sp)
	movwf fsr1h
	line	21
	
i1l2851:	
;ProjectSource/ISR.c: 21:    if (NCO1IF == 1){
	movlb 14	; select bank14
	btfss	(14492/8)^0700h,(14492)&7	;volatile
	goto	u204_21
	goto	u204_20
u204_21:
	goto	i1l381
u204_20:
	line	22
	
i1l2853:	
;ProjectSource/ISR.c: 22:      _HW_SysTickIntHandler();
	fcall	__HW_SysTickIntHandler
	line	24
	
i1l381:	
	movf	(??_myIsr+0),w
	movwf	btemp+11
	retfie
	opt callstack 0
GLOBAL	__end_of_myIsr
	__end_of_myIsr:
	signat	_myIsr,89
	global	__HW_SysTickIntHandler

;; *************** function __HW_SysTickIntHandler *****************
;; Defined at:
;;		line 185 in file "FrameworkSource/ES_Port.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;                  1    wreg      void 
;; Registers used:
;;		wreg, status,2, status,0, btemp+11
;; Tracked objects:
;;		On entry : 0/0
;;		On exit  : 0/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1   BANK2   BANK3   BANK4   BANK5   BANK6   BANK7   BANK8   BANK9  BANK10  BANK11  BANK12  BANK13  BANK14  BANK15  BANK16  BANK17  BANK18  BANK19  BANK20  BANK21  BANK22  BANK23  BANK24  BANK25
;;      Params:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Locals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Temps:          0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;      Totals:         0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_myIsr
;; This function uses a non-reentrant model
;;
psect	text42,local,class=CODE,delta=2,merge=1,group=0
	file	"FrameworkSource/ES_Port.c"
	line	185
global __ptext42
__ptext42:	;psect for function __HW_SysTickIntHandler
psect	text42
	file	"FrameworkSource/ES_Port.c"
	line	185
	global	__size_of__HW_SysTickIntHandler
	__size_of__HW_SysTickIntHandler	equ	__end_of__HW_SysTickIntHandler-__HW_SysTickIntHandler
	
__HW_SysTickIntHandler:	
;incstack = 0
	opt	callstack 7
; Regs used in __HW_SysTickIntHandler: [wreg+status,2+status,0+btemp+11]
	line	187
	
i1l2691:	
;FrameworkSource/ES_Port.c: 187:   NCOIF = 0;
	movlb 14	; select bank14
	bcf	(14492/8)^0700h,(14492)&7	;volatile
	line	188
	
i1l2693:	
;FrameworkSource/ES_Port.c: 188:   ++TickCount;
	movlw	low(01h)
	movwf	btemp+11
	movf	btemp+11,w
	movlb 0	; select bank0
	addwf	(_TickCount),f	;volatile
	line	189
	
i1l2695:	
;FrameworkSource/ES_Port.c: 189:   ++SysTickCounter;
	movlw	01h
	addwf	(_SysTickCounter),f	;volatile
	movlw	0
	addwfc	(_SysTickCounter+1),f	;volatile
	line	193
	
i1l55:	
	return
	opt callstack 0
GLOBAL	__end_of__HW_SysTickIntHandler
	__end_of__HW_SysTickIntHandler:
	signat	__HW_SysTickIntHandler,89
global	___latbits
___latbits	equ	3
	global	btemp
	btemp set 074h

	DABS	1,116,12	;btemp
	global	wtemp0
	wtemp0 set btemp+0
	global	wtemp1
	wtemp1 set btemp+2
	global	wtemp2
	wtemp2 set btemp+4
	global	wtemp3
	wtemp3 set btemp+6
	global	wtemp4
	wtemp4 set btemp+8
	global	wtemp5
	wtemp5 set btemp+10
	global	ttemp0
	ttemp0 set btemp+0
	global	ttemp1
	ttemp1 set btemp+3
	global	ttemp2
	ttemp2 set btemp+6
	global	ttemp3
	ttemp3 set btemp+9
	global	ltemp0
	ltemp0 set btemp+0
	global	ltemp1
	ltemp1 set btemp+4
	global	ltemp2
	ltemp2 set btemp+8
	global	ltemp3
	ltemp3 set btemp+2
	end
