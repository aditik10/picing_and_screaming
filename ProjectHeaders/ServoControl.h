/* 
 * File:   ServoControl.h
 * Author: kjepp
 *
 * Created on May 10, 2020, 10:06 PM
 */

#ifndef ServoControl_H
#define	ServoControl_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"
#include "ES_Port.h" 

// Public Function Prototypes
void Start_PushToTalk (void);
void Stop_PushToTalk (void);
void InitPWM(void);

#endif	/* ServoControl_H */

